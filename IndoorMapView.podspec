#
# Be sure to run `pod lib lint IndoorMapView.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'IndoorMapView'
  s.version          = '0.11.1'
  s.summary          = 'IndoorMapView provides a basic toolkit to present an interactive indoor map to a user'
  s.description      = <<-DESC
  IndoorMapView helps you to build an interactive indoor map expierence.
  Supports scrolling, zoom and rotation of the map. Focusing on a specific point.
  Provides default implementation for various map objects like user position, navigation path, custom shapes, images, floor transitions, etc as well as an easy way to add custom ones.
                       DESC
  s.homepage         = 'https://gitlab.com/oriient-public/indoormapview'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Michael Krutoyarskiy' => 'michaelk@oriient.me' }
  s.source           = { :git => 'https://gitlab.com/oriient-public/indoormapview.git', :tag => s.version.to_s }
  s.ios.deployment_target = '13.0'
  s.source_files = 'IndoorMapView/**/*.{swift}'
  s.frameworks = 'UIKit'
  s.dependency 'InteractiveContainerView', '0.7.1'
  s.resources = ['IndoorMapView/Resources/**/*']
  s.swift_version = '5.0'
end
