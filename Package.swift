// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "IndoorMapView",
    platforms: [.iOS(.v13)],
    products: [
        .library(
            name: "IndoorMapView",
            targets: [
                "IndoorMapView"
            ]
        ),
    ],
    dependencies: [
        .package(
            name: "InteractiveContainerView",
            url: "https://gitlab.com/oriient-public/interactivecontainerview",
            from: .init(stringLiteral: "0.7.1")
        )
    ],
    targets: [
        .target(
            name: "IndoorMapView",
            dependencies: [
                .product(
                    name: "InteractiveContainerView",
                    package: "InteractiveContainerView",
                    condition: nil
                )
            ],
            path: "IndoorMapView",
            resources: [
                .copy("Resources/IndoorMapView.xcassets")
            ]
        )
    ]
)
