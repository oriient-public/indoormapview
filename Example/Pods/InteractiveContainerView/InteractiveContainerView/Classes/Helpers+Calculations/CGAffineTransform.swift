//
//  CGAffineTransform.swift
//  IPSMapView
//
//  Created by Michael Krutoyarskiy on 27/10/2020.
//  Copyright © 2020 Michael Krutoyarskiy. All rights reserved.
//

import Foundation
import UIKit

public extension CGAffineTransform {
    /// Current scale value
    var scale: CGFloat { sqrt(a*a + c*c) }

    /// Current rotation value
    var rotation: CGFloat { atan2(b, a) }

    /// Current rotation value in degrees
    var rotationDegrees: CGFloat { rotation * (180 / CGFloat(Float.pi)) }

    /// Current translation value
    var translation: CGPoint { CGPoint(x: tx, y: ty) }

    /// Returns an affine transformation matrix constructed by applying transform closure
    /// with additional translation to respect the anchor point
    /// - Parameters:
    ///   - anchor: The anchor (pivot) point (For example: Point between user's finger for pinch/rotate gesture)
    ///   - center: Center of the transforming view
    ///   - transform: Transform to be applied
    /// - Returns: Transform relative to the anchor
    func transformed(
        around anchor: CGPoint,
        center: CGPoint,
        _ transform: (CGAffineTransform) -> CGAffineTransform
    ) -> CGAffineTransform {
        let translation = CGPoint(x: center.x - anchor.x, y: center.y - anchor.y)

        // to apply the anchor point dependent transform we translate - transform - translate back
        var t = self.translatedBy(x: -translation.x, y: -translation.y)
        t = transform(t)
        return t.translatedBy(x: translation.x, y: translation.y)
    }

    /// Returns an affine transformation matrix constructed by scaling
    /// with additional translation to respect the anchor point
    /// - Parameters:
    ///   - anchor: The anchor (pivot) point (For example: Point between user's finger for pinch/rotate gesture)
    ///   - scale: Scale to apply
    ///   - center: Center of the transforming view
    ///   - minScale: Minimum allowed scale
    ///   - maxScale: Maximum allowed scale
    /// - Returns: Transform scaled relative to the anchor
    func scaledAroundAnchor(
        _ anchor: CGPoint,
        scale: CGFloat,
        center: CGPoint,
        minScale: CGFloat = -CGFloat.greatestFiniteMagnitude,
        maxScale: CGFloat = CGFloat.greatestFiniteMagnitude
    ) -> CGAffineTransform {
        var scale = scale
        let current = self.scale
        let target = current * scale

        if target < minScale {
            scale = minScale/current
        }

        if target > maxScale {
            scale = maxScale/current
        }

        return transformed(around: anchor, center: center, { $0.scaledBy(x: scale, y: scale) })
    }

    /// Returns an affine transformation matrix constructed by rotating
    /// with additional translation to respect the anchor point
    /// - Parameters:
    ///   - anchor: The anchor (pivot) point (For example: Point between user's finger for pinch/rotate gesture)
    ///   - rotation: Rotation (radians) to apply
    ///   - center: Center of the transforming view
    /// - Returns: Transform rotated around the anchor
    func rotatedAroundAnchor(
        _ anchor: CGPoint,
        rotation: CGFloat,
        center: CGPoint,
        minScale: CGFloat = -CGFloat.greatestFiniteMagnitude,
        maxScale: CGFloat = CGFloat.greatestFiniteMagnitude
    ) -> CGAffineTransform {
        transformed(around: anchor, center: center, { $0.rotated(by: rotation) })
    }

    /// Calculates transform which applies translation so that
    /// view's content will not leave visibleArea
    /// - Parameters:
    ///   - transform: Transform to apply
    ///   - visibleArea: The area visible to the user
    ///   - contentOffset: The offset of the content inside the view frame
    ///   - view: The view to which the transfrom is applied
    static func applyLimitedTransform(
        _ transform: CGAffineTransform,
        visibleArea: CGRect,
        contentOffset: CGPoint,
        view: UIView
    ) {
        view.transform = transform

        // as view.frame is invalid due to transformation
        // we use original center to find the transformed view's corners
        let originalCenter = view.center.applying(transform.inverted())

        let topLeft = CGPoint.transformedViewPoint(
            transform: transform,
            viewBounds: view.bounds,
            originalCenter: originalCenter,
            originalPointOffset: contentOffset,
            widthMultiplier: -1,
            heightMultiplier: -1
        )

        let topRight = CGPoint.transformedViewPoint(
            transform: transform,
            viewBounds: view.bounds,
            originalCenter: originalCenter,
            originalPointOffset: contentOffset,
            widthMultiplier: 1,
            heightMultiplier: -1
        )

        let bottomLeft = CGPoint.transformedViewPoint(
            transform: transform,
            viewBounds: view.bounds,
            originalCenter: originalCenter,
            originalPointOffset: contentOffset,
            widthMultiplier: -1,
            heightMultiplier: 1
        )

        let bottomRight = CGPoint.transformedViewPoint(
            transform: transform,
            viewBounds: view.bounds,
            originalCenter: originalCenter,
            originalPointOffset: contentOffset,
            widthMultiplier: 1,
            heightMultiplier: 1
        )

        // whole visible area is inside the content bounds
        if visibleArea.isInside(polygon: [topLeft, topRight, bottomRight, bottomLeft]) {
            return
        }

        // minimal correction to be inside the visible area
        guard let correction = visibleArea.correction(
            to: [
                SegmentedLine( // left
                    a: topLeft,
                    b: bottomLeft
                ),
                SegmentedLine( // bot
                    a: bottomLeft,
                    b: bottomRight
                ),
                SegmentedLine( // right
                    a: bottomRight,
                    b: topRight
                ),
                SegmentedLine( // top
                    a: topLeft,
                    b: topRight
                )
            ]
        ) else {
            return
        }

        view.transform = transform.concatenating(CGAffineTransform(translationX: correction.x, y: correction.y))
    }

    /// Calculates intermediate transform for provided start, end, fractionComplete, anchor
    static func partialTransform(
        from start: CGAffineTransform,
        to end: CGAffineTransform,
        fractionComplete: CGFloat,
        anchor: CGPoint,
        center: CGPoint,
        size: CGSize
    ) -> CGAffineTransform {
        let anchorTranslation = CGAffineTransform(translationX: anchor.x - center.x, y: anchor.y - center.y)

        let transform = CGAffineTransform.partialTransform(
            from: start.concatenating(anchorTranslation.inverted()),
            to: end.concatenating(anchorTranslation.inverted()),
            size: size,
            at: fractionComplete
        )
        .concatenating(anchorTranslation)

        return transform
    }

    /// Calculates intermediate transform for provided start, end and fractionComplete
    static func partialTransform(
        from: CGAffineTransform,
        to: CGAffineTransform,
        size: CGSize,
        at fractionComplete: CGFloat
    ) -> CGAffineTransform {
        let interpolateValue: (CGFloat, CGFloat) -> CGFloat = { value1, value2 in
            return (1 - fractionComplete) * value1 + fractionComplete * value2
        }

        let fromComponents = from.components(viewSize: size)
        let toComponents = to.components(viewSize: size)

        return .init(
            scale: interpolateValue(fromComponents.scale, toComponents.scale),
            rotation: .interpolateAngle(from: fromComponents.rotation, to: toComponents.rotation, at: fractionComplete),
            viewSize: size,
            center: CGPoint(
                x: interpolateValue(fromComponents.center.x, toComponents.center.x),
                y: interpolateValue(fromComponents.center.y, toComponents.center.y)
            )
        )
    }

    /// Create affine transform of a view from the components
    init(
        scale: CGFloat,
        rotation: CGFloat,
        viewSize: CGSize,
        center: CGPoint
    ) {
        self = CGAffineTransform(
            translationX: viewSize.width/2 - center.x,
            y: viewSize.height/2 - center.y
        )
        .concatenating(CGAffineTransform(scaleX: scale, y: scale))
        .concatenating(CGAffineTransform(rotationAngle: rotation))
    }

    /// Decompose the transform of a view to the components
    func components(viewSize: CGSize) -> (
        scale: CGFloat,
        rotation: CGFloat,
        translation: CGPoint,
        center: CGPoint
    ) {
        let scale = self.scale
        let rotation = self.rotation
        let translation = self.translation

        let invertedTranslation = translation.applying(
            CGAffineTransform(scaleX: scale, y: scale)
                .concatenating(CGAffineTransform(rotationAngle: rotation))
                .inverted()
        )

        let center = CGPoint(
            x: viewSize.width/2 - invertedTranslation.x,
            y: viewSize.height/2 - invertedTranslation.y
        )

        return (
            scale: scale,
            rotation: rotation,
            translation: translation,
            center: center
        )
    }

}




