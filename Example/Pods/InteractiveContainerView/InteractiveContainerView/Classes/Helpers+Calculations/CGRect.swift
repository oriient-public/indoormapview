//
//  CGRect.swift
//  IPSMapView
//
//  Created by Michael Krutoyarskiy on 27/10/2020.
//  Copyright © 2020 Michael Krutoyarskiy. All rights reserved.
//

import Foundation
import UIKit

public extension CGRect {
    /// Corner points of the rectangle: [top-left, top-right, bot-right, bot-left]
    var corners: [CGPoint] {
        [
            //top-left
            CGPoint(
                x: self.minX,
                y: self.minY
            ),
            //top-right
            CGPoint(
                x: self.maxX,
                y: self.minY
            ),
            //bot-right
            CGPoint(
                x: self.maxX,
                y: self.maxY
            ),
            //bot-left
            CGPoint(
                x: self.minX,
                y: self.maxY
            ),
        ]
    }

    /// Edges of the rectangle: [left, bot, right, top]
    var edges: [SegmentedLine] {
        [
            SegmentedLine( // left
                a: CGPoint(
                    x: self.minX,
                    y: self.minY
                ),
                b: CGPoint(
                    x: self.minX,
                    y: self.maxY
                )
            ),
            SegmentedLine( // bot
                a: CGPoint(
                    x: self.minX,
                    y: self.maxY
                ),
                b: CGPoint(
                    x: self.maxX,
                    y: self.maxY
                )
            ),
            SegmentedLine( // right
                a: CGPoint(
                    x: self.maxX,
                    y: self.maxY
                ),
                b: CGPoint(
                    x: self.maxX,
                    y: self.minY
                )
            ),
            SegmentedLine( // top
                a: CGPoint(
                    x: self.minX,
                    y: self.minY
                ),
                b: CGPoint(
                    x: self.maxX,
                    y: self.minY
                )
            )

        ]
    }

    /// Checks if this rectangle is completely inside the area enclosed by the polygon
    func isInside(polygon: [CGPoint]) -> Bool {
        guard let first = polygon.first else {
            return false
        }

        let path = UIBezierPath()

        path.move(to: first)

        for i in 1..<polygon.count {
            path.addLine(to: polygon[i])
        }

        path.close()

        return self.corners.allSatisfy({ path.contains($0) })
    }

    /// Calculate correction needed the line to have interseption with the rect
    /// - Parameter lines: Line to evaluate
    /// - Returns: Minimal correction, or nil if it is not necessary
    static func correction(to line: SegmentedLine, edges: [SegmentedLine]) -> CGPoint? {
        var offsets: [CGPoint] = []
        for edge in edges {
            // if there is intersection - no correction is necessary
            if SegmentedLine.intersectionOfLines(line1: edge, line2: line) != nil {
                return nil
            }

            offsets.append(
                SegmentedLine.offsetBetweenLines(line1: line, line2: edge)
            )
        }

        return offsets.sorted(by: { $0.length < $1.length })[0]
    }

    /// Calculate correction needed so the closest of the lines to have interseption with the rect
    /// - Parameter lines: Lines to evaluate (edges of some polygon)
    /// - Returns: Minimal correction, or nil if it is not necessary
    func correction(to lines: [SegmentedLine]) -> CGPoint? {
        var corrections: [CGPoint] = []
        let edges = self.edges

        for line in lines {
            // if one of the line does not need correction - we stop
            guard let correction = Self.correction(to: line, edges: edges) else {
                return nil
            }
            corrections.append(correction)
        }

        return corrections.sorted(by: { $0.length < $1.length })[0]
    }
}
