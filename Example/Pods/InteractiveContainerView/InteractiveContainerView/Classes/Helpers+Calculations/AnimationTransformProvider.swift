//
//  AnimationTransformProvider.swift
//  InteractiveContainerView
//
//  Created by Michael Krutoyarskiy on 15/05/2023.
//

import Foundation
import UIKit

/// An abstraction for the animation intermediate transformations calculation
protocol AnimationTransformProvider {
    func transform(for fractionComplete: CGFloat) -> CGAffineTransform
}

/// Default implementation calculate a partial transformation around an anchor point
struct DefaultAnimationTransformProvider: AnimationTransformProvider {
    private let initialTransform: CGAffineTransform
    private let targetTransform: CGAffineTransform
    private let anchorPoint: CGPoint
    private let center: CGPoint
    private let size: CGSize

    init(initialTransform: CGAffineTransform, targetTransform: CGAffineTransform, anchorPoint: CGPoint, center: CGPoint, size: CGSize) {
        self.initialTransform = initialTransform
        self.targetTransform = targetTransform
        self.anchorPoint = anchorPoint
        self.center = center
        self.size = size
    }

    func transform(for fractionComplete: CGFloat) -> CGAffineTransform {
        CGAffineTransform.partialTransform(
            from: initialTransform,
            to: targetTransform,
            fractionComplete: fractionComplete,
            anchor: anchorPoint,
            center: center,
            size: size
        )
    }
}

/// Used to implement a deceleration animation when user lifts their finger after a pan motion
struct PanDecelerationAnimationTransformProvider: AnimationTransformProvider {
    private let velocity: CGPoint
    private let decelerationRate: CGFloat
    private let duration: Double
    private let initialTransform: CGAffineTransform

    init(velocity: CGPoint, decelerationRate: CGFloat, duration: Double, initialTransform: CGAffineTransform) {
        self.velocity = velocity
        self.decelerationRate = decelerationRate
        self.duration = duration
        self.initialTransform = initialTransform
    }

    func transform(for fractionComplete: CGFloat) -> CGAffineTransform {
        let value = UIPanGestureRecognizer.decelerationValue(
            at: fractionComplete * duration,
            velocity: velocity,
            rate: decelerationRate
        )

        return self.initialTransform.translatedBy(x: value.x, y: value.y)
    }
}
