//
//  CGPoint.swift
//  IPSMapView
//
//  Created by Michael Krutoyarskiy on 27/10/2020.
//  Copyright © 2020 Michael Krutoyarskiy. All rights reserved.
//

import Foundation
import UIKit

public extension CGPoint {
    /// Pixels devided by `UIScreen.main.scale`
    var screenPoints: CGPoint { CGPoint(x: x/UIScreen.main.scale, y: y/UIScreen.main.scale) }

    /// Distance to the specified point
    func distance(to point: CGPoint) -> CGFloat {
        sqrt(pow(point.x - self.x, 2) + pow(point.y - self.y, 2))
    }

    /// Length of the vector represented by this point
    var length: CGFloat {
        distance(to: .zero)
    }

    /// Point with inverted (multiplied by -1) coordinates
    var inverted: CGPoint {
        CGPoint(x: -x, y: -y)
    }

    /// Distance to the specified point
    func angle(to point: CGPoint) -> CGFloat {
        CGFloat(atan2(Float(point.y - y), Float(point.x - x)))
    }
    
    /// Finds a point on a transformed view
    /// from the point on the original view.
    /// Used to find corners of the transformed view
    /// TopLeft (widthMultiplier: -1, heightMultiplier: -1), BottomRight (widthMultiplier: 1, heightMultiplier: 1), etc
    /// - Parameters:
    ///   - transform: current view transform
    ///   - viewBounds: current view bounds
    ///   - originalCenter: original view center (before transformation)
    ///   - originalPointOffset: offset of the point from center before transfromation
    ///   - widthMultiplier: direction of the offset from center
    ///   - heightMultiplier: direction of the offset from center
    /// - Returns: Point after transformation
    static func transformedViewPoint(
        transform: CGAffineTransform,
        viewBounds: CGRect,
        originalCenter: CGPoint,
        originalPointOffset: CGPoint,
        widthMultiplier: CGFloat,
        heightMultiplier: CGFloat
    ) -> CGPoint {
        var x = originalCenter.x
        x += (viewBounds.width - 2*originalPointOffset.x)  / 2 * widthMultiplier

        var y = originalCenter.y
        y += (viewBounds.height - 2*originalPointOffset.y) / 2 * heightMultiplier

        var result = CGPoint(x: x, y: y).applying(transform)
        result.x += transform.tx
        result.y += transform.ty

        return result
    }

    /// Distance to the specified line segment
    func distance(to line: SegmentedLine) -> CGFloat {
        let pv_dx = self.x - line.a.x
        let pv_dy = self.y - line.a.y
        let wv_dx = line.b.x - line.a.x
        let wv_dy = line.b.y - line.a.y

        let dot = pv_dx * wv_dx + pv_dy * wv_dy
        let len_sq = wv_dx * wv_dx + wv_dy * wv_dy
        let param = dot / len_sq

        var int_x, int_y: CGFloat /* intersection of normal to ab that goes through p */

        if param < 0 || (line.a.x == line.b.x && line.a.y == line.b.y) {
            int_x = line.a.x
            int_y = line.a.y
        } else if param > 1 {
            int_x = line.b.x
            int_y = line.b.y
        } else {
            int_x = line.a.x + param * wv_dx
            int_y = line.a.y + param * wv_dy
        }

        /* Components of normal */
        let dx = self.x - int_x
        let dy = self.y - int_y

        return sqrt(dx * dx + dy * dy)
    }

    /// point + offset = closest point on the line
    func offset(from line: SegmentedLine) -> CGPoint {
        let pv_dx = self.x - line.a.x
        let pv_dy = self.y - line.a.y
        let wv_dx = line.b.x - line.a.x
        let wv_dy = line.b.y - line.a.y

        let dot = pv_dx * wv_dx + pv_dy * wv_dy
        let len_sq = wv_dx * wv_dx + wv_dy * wv_dy
        let param = dot / len_sq

        var int_x, int_y: CGFloat /* intersection of normal to ab that goes through p */

        if param < 0 || (line.a.x == line.b.x && line.a.y == line.b.y) {
            int_x = line.a.x
            int_y = line.a.y
        } else if param > 1 {
            int_x = line.b.x
            int_y = line.b.y
        } else {
            int_x = line.a.x + param * wv_dx
            int_y = line.a.y + param * wv_dy
        }

        /* Components of normal */
        return CGPoint(
            x: int_x - self.x,
            y: int_y - self.y
        )
    }
}
