//
//  CGFloat.swift
//  IPSMapView
//
//  Created by Michael Krutoyarskiy on 27/10/2020.
//  Copyright © 2020 Michael Krutoyarskiy. All rights reserved.
//

import Foundation
import UIKit

public extension CGFloat {
    // Radians as degrees
    var asDegrees: CGFloat { self * (180/CGFloat.pi) }

    // Degrees as radians
    var asRadians: CGFloat { self * (CGFloat.pi/180) }

    // Interpolation of 2 angles that considers the angles overhead
    static func interpolateAngle(
        from sourceRotation: CGFloat,
        to destinationRotation: CGFloat,
        at fractionComplete: CGFloat
    ) -> CGFloat {
        let originalDestinationRotation = destinationRotation
        var destinationRotation = originalDestinationRotation
        var delta = abs(sourceRotation - originalDestinationRotation)

        if (delta > abs(sourceRotation - (originalDestinationRotation - 2*CGFloat.pi))) {
            delta = abs(sourceRotation - (originalDestinationRotation - 2*CGFloat.pi))
            destinationRotation = (originalDestinationRotation - 2*CGFloat.pi)
        }

        if (delta > abs(sourceRotation - (originalDestinationRotation + 2*CGFloat.pi))) {
            destinationRotation = (originalDestinationRotation + 2*CGFloat.pi)
        }

        let interpolateValue: (CGFloat, CGFloat) -> CGFloat = { value1, value2 in
            return (1 - fractionComplete) * value1 + fractionComplete * value2
        }

        return interpolateValue(sourceRotation, destinationRotation)
    }
}
