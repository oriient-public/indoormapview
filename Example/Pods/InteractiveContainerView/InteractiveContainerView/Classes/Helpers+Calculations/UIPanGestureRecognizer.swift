//
//  UIPanGestureRecognizer.swift
//  IPSMapView
//
//  Created by Michael Krutoyarskiy on 27/10/2020.
//  Copyright © 2020 Michael Krutoyarskiy. All rights reserved.
//

import Foundation
import UIKit

extension UIPanGestureRecognizer {
    static func decelerationValue(
        at time: TimeInterval,
        velocity: CGPoint,
        rate: CGFloat
    ) -> CGPoint {
        let dCoeff = 1000 * log(rate)
        return CGPoint(
            x: (pow(rate, CGFloat(1000 * time)) - 1) / dCoeff * velocity.x,
            y: (pow(rate, CGFloat(1000 * time)) - 1) / dCoeff * velocity.y
        )
    }

    static func decelerationDestination(velocity: CGPoint, rate: CGFloat) -> CGPoint {
        let dCoeff = 1000 * log(rate)
        return CGPoint(
            x: -velocity.x/dCoeff,
            y: -velocity.y/dCoeff
        )
    }

    static func decelerationDuration(velocity: CGPoint, threshold: CGFloat, rate: CGFloat) -> Double {
        guard velocity.length > 0 else { return 0 }
        let dCoeff = 1000 * log(rate)
        return TimeInterval(log(-dCoeff * threshold / velocity.length) / dCoeff)
    }
}
