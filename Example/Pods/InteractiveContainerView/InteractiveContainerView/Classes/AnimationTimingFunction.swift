//
//  AnimationTimingFunction.swift
//  InteractiveContainerView
//
//  Created by Michael Krutoyarskiy on 15/05/2023.
//

import Foundation


/// This enum allows you to select the timing function for an animation
/// It is an analogue of CAMediaTimingFunction, but unlike CAMediaTimingFunction it exposes the interpolation function
public enum AnimationTimingFunction {
    case linear
    case easeInEaseOut
    case easeOut
    case easeIn

    public func interpolated(progress: CGFloat) -> CGFloat {
        let progress = min(max(progress, 0), 1)
        switch self {
        // See: https://developer.apple.com/documentation/quartzcore/camediatimingfunctionname/1522173-easeineaseout
        case .easeInEaseOut:
            return calculateBezierProgress(
                controlPoint1: CGPoint(x: 0.42, y: 0.0),
                controlPoint2: CGPoint(x: 0.58, y: 1.0),
                progress: progress
            )
        // See: https://developer.apple.com/documentation/quartzcore/camediatimingfunctionname/1521943-linear
        case .linear: return progress
        // See: https://developer.apple.com/documentation/quartzcore/camediatimingfunctionname/1521971-easein
        case .easeIn:
            return calculateBezierProgress(
                controlPoint1: CGPoint(x: 0.42, y: 0.0),
                controlPoint2: CGPoint(x: 1.0, y: 1.0),
                progress: progress
            )
        // See: https://developer.apple.com/documentation/quartzcore/camediatimingfunctionname/1522178-easeout
        case .easeOut:
            return calculateBezierProgress(
                controlPoint1: CGPoint(x: 0.0, y: 0.0),
                controlPoint2: CGPoint(x: 0.58, y: 1.0),
                progress: progress
            )
        }
    }

    // Progress interpolated using a Bézier timing function with the defined control points
    private func calculateBezierProgress(
        controlPoint1: CGPoint,
        controlPoint2: CGPoint,
        progress: CGFloat
    ) -> CGFloat {
        let t = progress
        let u = 1.0 - t
        let tt = t * t
        let uu = u * u
        let uuu = uu * u
        let ttt = tt * t

        let p = uuu * 0
            + 3 * uu * t * controlPoint1.y
            + 3 * u * tt * controlPoint2.y
            + ttt * 1

        return p
    }
}


