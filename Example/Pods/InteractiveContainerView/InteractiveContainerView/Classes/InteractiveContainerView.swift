//
//  InteractiveMapView.swift
//  IPSMapView
//
//  Created by Michael Krutoyarskiy on 29/03/2020.
//  Copyright © 2020 Michael Krutoyarskiy. All rights reserved.
//

import Foundation
import UIKit

/// InteractiveContainerView manages user gestures to apply correct transformation to the content layers.
/// Set `contentSize` so the container will correctly scale and offset the content to fit its bounds.
/// Use `addContentLayer()` to add content layers, all the transformation will be applied to them.
/// Use `delegate` to react to transform updates and user interactions.
/// Use `focus(on:zoomScale:rotation:duration:animated:)` to focus on some area of the content.
public class InteractiveContainerView: UIView {
    // MARK: - Public
    /// If set to false, the container will not apply user gestures to transform the content,
    /// but will still report the gestures to the delegate. Used to customize the handling of the gestures (To drag some objects for example).
    public var isContentTransformationEnabled = true
    
    /// Enable/Disable zoom in action on double tap. Enabled by default
    public var shouldZoomOnDoubleTap = true

    /// The offset of the content inside the container original bounds
    /// Automatically recalculated if content size or container bounds changed
    public private(set) var contentAspectOffset: CGPoint = .zero

    /// The scale factor applied to fit the content in the container
    /// Automatically recalculated if content size or container bounds changed
    public private(set) var contentAspectScale: CGFloat = 1

    /// Current transform applied to the content
    public var currentContentTransform: CGAffineTransform { self.container.transform  }

    /// The content point which is currently in the center of the container
    public var currentFocus: CGPoint { container.convert(CGPoint(x: self.bounds.size.width/2, y: self.bounds.size.height/2), from: self) }

    /// The size of the main content, required to correctly calculate zoom scale and scrolling limits
    public var contentSize: CGSize = .zero {
        didSet {
            recalculateContentParameters()
        }
    }

    /// Enables rotation gesture to transform the content. Applies the default focus when disabled.
    public var isRotationEnabled: Bool = true {
        didSet {
            if !isRotationEnabled {
                recalculateContentParameters()
                applyDefaultFocus()
            }
        }
    }

    /// Default duration of the focus animation
    public static let defaultAnimationDuration: Double = 0.3

    /// Default timing function of the focus animation
    public static let defaultAnimationTimingFunction: AnimationTimingFunction = .linear

    /// Minimum allowed zoom scale. When scale == 1 the content fits the container bounds
    /// *The limitation is only applied to gestures, ignored when scaling caused by focus function
    public var minZoomScale: CGFloat = 0.5

    /// Maximum allowed zoom scale. When scale == 1 the content fits the container bounds
    /// *The limitation is only applied to gestures, ignored when scaling caused by focus function
    public var maxZoomScale: CGFloat = 10

    /// Content rotation used for default focus. May be useful to fit longer content into a narrow screen.
    public var defaultContentRotation: CGFloat = 0

    /// Content scale used for default focus.
    public var defaultContentScale: CGFloat = 1

    /// Delegate to handle content modifications and gestures
    public weak var delegate: InteractiveContainerViewDelegate?

    /// Adds a content layer to the container, the layer bounds will be set to fit the container.
    /// All the transformations applied to container automatically applied to the contnet layers.
    /// Use standard `layer.removeFromSuperlayer()` to remove the layer
    public func addContentLayer(_ layer: CALayer) {
        layer.frame = container.bounds
        container.layer.addSublayer(layer)
    }

    /// Transforms the container to put the required point into focus (center of the container)
    /// - Parameters:
    ///   - point: Point on the content
    ///   - zoomScale: Required content scale
    ///   - rotation:  Required content rotation
    ///   - duration: Duration of the animation (ignored if `animated == false`). Default is: InteractiveContainerView.defaultAnimationDuration.
    ///   - animated: true to animate the content transformation, false to transform immediately;
    ///   - offset: offset from the center of the view, affects the animation anchor point.
    public func focus(
        on point: CGPoint,
        zoomScale: CGFloat,
        rotation: CGFloat,
        duration: Double? = nil,
        animated: Bool = false,
        timingFunction: AnimationTimingFunction = InteractiveContainerView.defaultAnimationTimingFunction,
        offset: CGFloat? = nil
    ) {
        let transform = CGAffineTransform(
            scale: zoomScale,
            rotation: rotation,
            viewSize: container.bounds.size,
            center: point
        )

        if animated {
            animateTransform(
                transform,
                duration: duration ?? Self.defaultAnimationDuration,
                timingFunction: timingFunction,
                animationAnchorPoint: CGPoint(x: container.center.x, y: container.center.y - (offset ?? 0))
            )
        } else {
            applyTransform(transform)
        }
    }

    /// Focus on the center of the content with `defaultContentRotation` and `defaultContentScale`
    /// - Parameter animated: true to animate the content transformation, false to transform immediately.
    public func applyDefaultFocus(animated: Bool = false) {
        focus(
            on: self.convert(container.center, from: self),
            zoomScale: defaultContentScale,
            rotation: defaultContentRotation,
            animated: animated
        )
    }

    /// Gets the snapshot of the content of the required size. See `UIView.resizableSnapshotView` for more details.
    public func getContentSnapshot(center: CGPoint, size: CGSize, afterScreenUpdates: Bool) -> UIView? {
        container.resizableSnapshotView(
            from: CGRect(
                x: center.x - size.width/2,
                y: center.y - size.height/2,
                width: size.width,
                height: size.height
            ),
            afterScreenUpdates: afterScreenUpdates,
            withCapInsets: .zero
        )
    }

    /// Converts point from content coordinate system to the provided view coordinate system
    public func convertContent(_ point: CGPoint, to view: UIView?) -> CGPoint {
        self.container.convert(point, to: view)
    }

    /// Converts rectangle from content coordinate system to the provided view coordinate system
    public func convertContent(_ rect: CGRect, to view: UIView?) -> CGRect {
        self.container.convert(rect, to: view)
    }

    /// Converts point from the provided view coordinate system to content coordinate system
    public func convertContent(_ point: CGPoint, from view: UIView?) -> CGPoint {
        self.container.convert(point, from: view)
    }

    /// Converts rectangle from the provided view coordinate system to content coordinate system
    public func convertContent(_ rect: CGRect, from view: UIView?) -> CGRect {
        self.container.convert(rect, from: view)
    }

    /// Delay triggering tap gesture until double tap gesture is failed
    /// Useful if tap and double tap used for the same object/area
    public var tapShouldRequireDoubleTapToFail = false

    // MARK: - Constants
    private static let doubleTapScale: CGFloat = 2
    private static let visibleAreaBoundsAdditionalInset: CGPoint = CGPoint(x: 100, y: 250)

    // MARK: - Private Properties
    // the container for map and content, all the transforms are applied to it
    private let container = UIView()

    // helper for transformations
    private let shadowContainer = UIView()

    public override var frame: CGRect {
        didSet {
            onFrameChanged()
        }
    }

    public override var bounds: CGRect {
        didSet {
            onFrameChanged()
        }
    }

    private var tapGesture: UITapGestureRecognizer!
    private var doubleTapGesture: UITapGestureRecognizer!
    private var visibleAreaBounds: CGRect = .zero

    // MARK: - Initial setup
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    private func commonInit() {
        setupDisplayLink()
        setupContainerLayer()
        setupGestureRecognizers()
    }

    private func setupContainerLayer() {
        self.clipsToBounds = true
        self.addSubview(container)

        container.layer.disableImplicitTransfromAnimation()
        container.layer.disableImplicitPositionAnimation()
        
        container.frame = self.bounds
        shadowContainer.frame = container.frame
        container.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        container.translatesAutoresizingMaskIntoConstraints = true
    }

    private func setupGestureRecognizers() {
        let pan = UIPanGestureRecognizer(target: self, action: #selector(onPanGesture))
        pan.delegate = self
        pan.maximumNumberOfTouches = 2
        self.addGestureRecognizer(pan)

        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(onLongPressGesture))
        longPress.delegate = self
        longPress.delaysTouchesEnded = true
        self.addGestureRecognizer(longPress)

        let touch = UILongPressGestureRecognizer(target: self, action: #selector(onTouchGesture))
        touch.delegate = self
        touch.minimumPressDuration = 0
        touch.allowableMovement = CGFloat(Float.greatestFiniteMagnitude)
        self.addGestureRecognizer(touch)

        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(onPinchGesture))
        pinch.delegate = self
        self.addGestureRecognizer(pinch)

        let rotate = UIRotationGestureRecognizer(target: self, action: #selector(onRotateGesture))
        rotate.delegate = self
        self.addGestureRecognizer(rotate)

        let tap = UITapGestureRecognizer(target: self, action: #selector(onTapGesture))
        tap.numberOfTapsRequired = 1
        tap.delegate = self
        self.addGestureRecognizer(tap)
        self.tapGesture = tap

        tap.require(toFail: longPress)
        tap.require(toFail: pan)

        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(onDoubleTapGesture))
        doubleTap.numberOfTapsRequired = 2
        doubleTap.delegate = self
        self.addGestureRecognizer(doubleTap)
        self.doubleTapGesture = doubleTap
    }

    // MARK: - Helpers
    private func onFrameChanged() {
        stopOngoingAnimation()
        let transform = container.transform
        container.transform = .identity
        shadowContainer.transform = .identity

        container.frame = self.bounds
        container.layer.sublayers?.forEach {
            $0.frame = container.bounds
        }

        shadowContainer.frame = container.frame

        container.transform = transform
        shadowContainer.transform = transform
        recalculateContentParameters()
    }

    private func recalculateContentParameters() {
        if contentSize.width == 0 || contentSize.height == 0 {
            visibleAreaBounds = container.bounds
            contentAspectOffset = .zero
            contentAspectScale = 1
            delegate?.onContentParametersChanged()
            return
        }

        contentAspectScale = min(container.bounds.width / contentSize.width, container.bounds.height / contentSize.height)

        contentAspectOffset = CGPoint(
            x: (container.bounds.width - contentSize.width * contentAspectScale)/2,
            y: (container.bounds.height - contentSize.height * contentAspectScale)/2
        )

        setupVisibleArea()

        applyLimitedTransform(self.container.transform)
        delegate?.onContentParametersChanged()
    }

    private func setupVisibleArea() {
        var insets = Self.visibleAreaBoundsAdditionalInset

        if insets.x >= container.bounds.width/2 {
            insets.x = container.bounds.width/2 - 1
        }

        if insets.y >= container.bounds.height/2 {
            insets.y = container.bounds.height/2 - 1
        }

        visibleAreaBounds = container.bounds.insetBy(
            dx: insets.x,
            dy: insets.y
        )
    }

    private func applyLimitedTransform(
        _ transform: CGAffineTransform,
        animated: Bool = false,
        duration: Double = InteractiveContainerView.defaultAnimationDuration
    ) {
        if animated {
            animateTransform(calculateLimitedTransform(transform), duration: duration, timingFunction: Self.defaultAnimationTimingFunction)
        } else {
            applyTransform(calculateLimitedTransform(transform))
        }
    }

    private func calculateLimitedTransform(_ transform: CGAffineTransform) -> CGAffineTransform {
        CGAffineTransform.applyLimitedTransform(
            transform,
            visibleArea: visibleAreaBounds,
            contentOffset: contentAspectOffset,
            view: shadowContainer
        )

        return shadowContainer.transform
    }

    private func applyTransform(_ transform: CGAffineTransform) {
        CALayer.disableImplicitAnimations {
            self.container.transform = transform
            self.shadowContainer.transform = transform
            self.delegate?.onContentTransformed(transform)
        }
    }

    // MARK: - Animation
    private var animationTransformProvider: AnimationTransformProvider?
    private var animationStartTime: CFTimeInterval = 0
    private var animationDuration: Double?
    private var animationTimingFunction: AnimationTimingFunction?
    private var displayLink: CADisplayLink!

    private func animateTransform(
        _ transform: CGAffineTransform,
        duration: Double,
        timingFunction: AnimationTimingFunction? = nil,
        animationAnchorPoint: CGPoint? = nil
    ) {
        startAnimation(
            animationTransformProvider: DefaultAnimationTransformProvider(
                initialTransform: self.container.transform,
                targetTransform: transform,
                anchorPoint: animationAnchorPoint ?? self.container.center,
                center: self.container.center,
                size: self.container.bounds.size
            ),
            duration: duration,
            timingFunction: timingFunction ?? InteractiveContainerView.defaultAnimationTimingFunction,
            animationAnchorPoint: animationAnchorPoint
        )
    }

    private func startAnimation(
        animationTransformProvider: AnimationTransformProvider,
        duration: Double,
        timingFunction: AnimationTimingFunction?,
        animationAnchorPoint: CGPoint?
    ) {
        self.animationTransformProvider = animationTransformProvider
        self.animationStartTime = CACurrentMediaTime()
        self.animationDuration = duration
        self.animationTimingFunction = timingFunction ?? InteractiveContainerView.defaultAnimationTimingFunction
        self.displayLink.isPaused = false
    }

    private func setupDisplayLink() {
        let displayLink = CADisplayLink(target: self, selector: #selector(updateTransform))
        displayLink.add(to: .main, forMode: .default)
        displayLink.isPaused = true
        self.displayLink = displayLink
    }

    @objc private func updateTransform(displayLink: CADisplayLink) {
        guard let animationTransformProvider = self.animationTransformProvider,
              let animationDuration = self.animationDuration,
              let animationTimingFunction = self.animationTimingFunction else {
            return
        }

        let fractionComplete = animationTimingFunction.interpolated(progress: (displayLink.targetTimestamp - self.animationStartTime) / animationDuration)

        self.applyLimitedTransform(animationTransformProvider.transform(for: fractionComplete))

        if fractionComplete >= 1 {
            self.stopOngoingAnimation()
            return
        }
    }

    /// Stops ongoing animation and applies current transform
    private func stopOngoingAnimation() {
        self.animationTransformProvider = nil
        self.displayLink?.isPaused = true
    }

    /// Uses the pan velocity to simulate scrolling inertion
    /// - Parameter velocity: The velocity vector the moment user raised the finger (points per millisecond)
    private func animatePanDeceleration(velocity: CGPoint) {
        // We do not apply the velocity if the content fits the screen or if the velocity is insignificant
        let scale = container.transform.scale
        if velocity.length < 25 || container.transform.scale <= 1 {
            return
        }

        // the deceleration rate is decreasing as the scale factor increasing =>
        // more you zoom more inertion is applied
        let decelerationRate: CGFloat = UIScrollView.DecelerationRate.normal.rawValue - (UIScrollView.DecelerationRate.normal.rawValue - UIScrollView.DecelerationRate.fast.rawValue)/scale

        // total time necessary to compensate the velocity
        let duration = UIPanGestureRecognizer.decelerationDuration(velocity: velocity, threshold: 0.5 / UIScreen.main.scale, rate: decelerationRate)

        self.startAnimation(
            animationTransformProvider: PanDecelerationAnimationTransformProvider(
                velocity: velocity,
                decelerationRate: decelerationRate,
                duration: duration,
                initialTransform: container.transform
            ),
            duration: duration,
            timingFunction: .linear,
            animationAnchorPoint: nil
        )
    }
}

// MARK: - Gestures handling
extension InteractiveContainerView {
    @objc private func onTouchGesture(_ recognizer: UILongPressGestureRecognizer) {
        switch recognizer.state {
        case .began:
            stopOngoingAnimation()
            delegate?.onUserInteractionStarted()
        case .changed:
            stopOngoingAnimation()
        case .ended, .cancelled:
            delegate?.onUserInteractionEnded()
        default:
            break
        }
    }

    @objc private func onLongPressGesture(_ recognizer: UILongPressGestureRecognizer) {
        delegate?.onContentLongpressed(recognizer.location(in: container), state: recognizer.state)
    }

    @objc private func onTapGesture(_ recognizer: UITapGestureRecognizer) {
        switch recognizer.state {
        case .ended:
            delegate?.onContentTapped(recognizer.location(in: container))
        default:
            break
        }
    }

    @objc private func onDoubleTapGesture(_ recognizer: UITapGestureRecognizer) {
        switch recognizer.state {
        case .ended:
            delegate?.onContentDoubleTapped(recognizer.location(in: container))

            if !shouldZoomOnDoubleTap || !isContentTransformationEnabled {
                return
            }

            applyLimitedTransform(
                container.transform.scaledAroundAnchor(
                    recognizer.location(in: container),
                    scale: Self.doubleTapScale,
                    center: container.center,
                    minScale: minZoomScale,
                    maxScale: maxZoomScale
                ),
                animated: true
            )
        default:
            break
        }
    }

    @objc private func onPinchGesture(_ recognizer: UIPinchGestureRecognizer) {
        let scale = recognizer.scale

        delegate?.onContentPinched(
            scaleDelta: scale,
            around: recognizer.location(in: container),
            state: recognizer.state
        )

        recognizer.scale = 1

        if !isContentTransformationEnabled {
            return
        }

        switch recognizer.state {
        case .changed, .ended:
            applyLimitedTransform(
                container.transform.scaledAroundAnchor(
                    recognizer.location(in: container),
                    scale: scale,
                    center: container.center,
                    minScale: minZoomScale,
                    maxScale: maxZoomScale
                )
            )
        default:
            break
        }
    }

    @objc private func onPanGesture(_ recognizer: UIPanGestureRecognizer) {
        let translation = recognizer.translation(in: container)

        self.delegate?.onContentDragged(
            to: recognizer.location(in: container),
            translation: translation,
            state: recognizer.state
        )

        recognizer.setTranslation(.zero, in: container)

        if !isContentTransformationEnabled {
            return
        }

        switch recognizer.state {
        case .ended, .changed:
            stopOngoingAnimation()
            if recognizer.state == .ended {
                animatePanDeceleration(velocity: recognizer.velocity(in: container))
            } else {
                applyLimitedTransform(container.transform.translatedBy(x: translation.x, y: translation.y))
            }
        default:
            break
        }
    }

    @objc private func onRotateGesture(_ recognizer: UIRotationGestureRecognizer) {
        let rotation = recognizer.rotation

        self.delegate?.onContentRotated(
            angleDelta: rotation,
            around: recognizer.location(in: container),
            state: recognizer.state
        )

        recognizer.rotation = 0

        if !isRotationEnabled || !isContentTransformationEnabled {
            return
        }

        switch recognizer.state {
        case .changed, .ended:
            applyLimitedTransform(
                container.transform.rotatedAroundAnchor(
                    recognizer.location(in: container),
                    rotation: rotation,
                    center: container.center,
                    minScale: self.minZoomScale,
                    maxScale: self.maxZoomScale
                )
            )
        default:
            break
        }
    }
}

// MARK: - UIGestureRecognizerDelegate
extension InteractiveContainerView: UIGestureRecognizerDelegate {
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }

    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer == self.tapGesture,
           otherGestureRecognizer == doubleTapGesture {
            return self.tapShouldRequireDoubleTapToFail
        }

        return false
    }
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
