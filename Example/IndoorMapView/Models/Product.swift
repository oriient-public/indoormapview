//
//  Product.swift
//  IndoorMapViewSample
//
//  Created by Michael Krutoyarskiy on 25/01/2022.
//

import Foundation
import CoreGraphics
import UIKit
import IndoorMapView

struct Product: ClusteringCompatible {
    let id: String
    let name: String
    let coordinate: CGPoint
    let image: UIImage?
    let itemSizeForClustering: CGSize
    let clusterSizeForClustering: CGSize
}

extension Product {
    var iconSize: CGSize {
        let width = itemSizeForClustering.width - 10
        let height = itemSizeForClustering.height - 10
        
        return CGSize(width: width, height: height)
    }
}

extension Product: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
