//
//  Floor.swift
//  IndoorMapView_Example
//
//  Created by Michael Krutoyarskiy on 04/11/2020.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import Foundation
import UIKit

/// Containts information about a floor in a building
/// and the important objects on this floor
struct Floor {
    /// index of the floor in the building
    let index: Int

    /// Human readable title
    let title: String

    /// A part of the walking simulation path for this floor
    let walkingSimulationPath: [CGPoint]

    /// Important areas we want to highlight on this floor
    let markedAreas: [[CGPoint]]

    /// Texts we want to show on this floor
    let texts: [Text]

    // Information about the map image
    /// image name for the map
    let imageName: String

    /// Size of the map's image in meters
    let mapSizeMeters: CGSize

    /// Map's image pixel to meter ratio
    let pixelToMeter: CGFloat

    /// Products to show on this floor
    let products: [Product]
}
