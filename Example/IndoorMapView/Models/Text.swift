//
//  Text.swift
//  IndoorMapView_Example
//
//  Created by Michael Krutoyarskiy on 04/11/2020.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import Foundation
import UIKit

/// Metadata of a text we will show on the map
struct Text {
    let coordinates: CGPoint
    let text: String
    let color: UIColor
    let font: UIFont
}
