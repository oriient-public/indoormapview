//
//  MapLevel.swift
//  IndoorMapView_Example
//
//  Created by Michael Krutoyarskiy on 04/11/2020.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import Foundation
// Objects on a higher level overlay objects on lower levels
enum MapLevel: UInt {
    case user = 100
    case floorTransitions = 90
    case pins = 80
    case texts = 70
    case navigationPath = 50
    case markedAreas = 10
    case products = 85
}
