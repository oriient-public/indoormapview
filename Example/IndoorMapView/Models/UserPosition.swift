//
//  UserPosition.swift
//  IndoorMapView_Example
//
//  Created by Michael Krutoyarskiy on 04/11/2020.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import Foundation
import UIKit

/// Information about a user position
struct UserPosition {
    /// Index of the floor where user is located
    let floorIndex: Int

    /// Coordinates where user is located
    let coordinates: CGPoint

    /// Direction of the user movement
    let heading: CGFloat

    /// The radius of uncertainty for the position
    let accuracy: CGFloat
}
