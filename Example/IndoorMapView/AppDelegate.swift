//
//  AppDelegate.swift
//  IndoorMapView
//
//  Created by Michael on 11/04/2020.
//  Copyright (c) 2020 Oriient New Media Ltd. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

}

