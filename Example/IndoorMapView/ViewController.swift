//
//  ViewController.swift
//  IndoorMapView
//
//  Created by Michael on 11/04/2020.
//  Copyright (c) 2020 Oriient New Media Ltd. All rights reserved.
//

import UIKit
import Combine
import IndoorMapView

class ViewController: UIViewController {
    @IBOutlet weak var mapView: IndoorMapView!
    @IBOutlet weak var floorSelector: UISegmentedControl!
    @IBOutlet weak var togglePlayButton: UIButton!

    private let unitsConverter: UnitsConverter = CartesianUnitsConverter()

    private var userPositionManager: UserPositionManager!
    private var userPositionLayer: UserPositionLayer!
    private var navigationPathLayer: PolylineLayer!
    private var cancellable: [AnyCancellable] = []

    private var productsClusterManager: ClusterManager<Product, IntersectionBasedClustering<Product>>!


    /// Abstract floors in an abstract building (probably in vacuum)
    /// All the coordinates are in metric coordinate system (see `CartesianUnitsConverter`), not in screen points
    // MARK: - Floors metadata
    let floors: [Floor] = [
        Floor(
            index: 1,
            title: "Floor 1",
            walkingSimulationPath: [
                CGPoint(x: 1.5, y: 40),
                CGPoint(x: 1.5, y: 32),
                CGPoint(x: 8, y: 32),
                CGPoint(x: 8, y: 26),
                CGPoint(x: 9, y: 23.5),
                CGPoint(x: 9, y: 18)
            ],
            markedAreas: [
                [CGPoint(x: 5, y: 7), CGPoint(x: 5, y: 13.5), CGPoint(x: 10.8, y: 13.5), CGPoint(x: 10.8, y: 7)]
            ],
            texts: [
                Text(
                    coordinates:  CGPoint(x: 7.7, y: 10.2),
                    text: "Area #1",
                    color: UIColor.darkGray,
                    font: UIFont.systemFont(ofSize: 14, weight: .medium)
                )
            ],
            imageName: "Floor1",
            mapSizeMeters: CGSize(width: 39.4, height: 41.88),
            pixelToMeter: 25,
            products: [
                Product(
                    id: UUID().uuidString,
                    name: "Lobster",
                    coordinate: CGPoint(x: 23, y: 10),
                    image: UIImage(named: "lobster"),
                    itemSizeForClustering: CGSize(width: 44, height: 44),
                    clusterSizeForClustering: CGSize(width: 69, height: 44)
                ),
                Product(
                    id: UUID().uuidString,
                    name: "Strawberry",
                    coordinate: CGPoint(x: 30, y: 25),
                    image: UIImage(named: "strawberry"),
                    itemSizeForClustering: CGSize(width: 40, height: 40),
                    clusterSizeForClustering: CGSize(width: 69, height: 44)
                ),
                Product(
                    id: UUID().uuidString,
                    name: "Strawberry",
                    coordinate: CGPoint(x: 25, y: 25),
                    image: UIImage(named: "strawberry"),
                    itemSizeForClustering: CGSize(width: 20, height: 20),
                    clusterSizeForClustering: CGSize(width: 69, height: 44)
                ),
                Product(
                    id: UUID().uuidString,
                    name: "Bread",
                    coordinate: CGPoint(x: 35, y: 12),
                    image: UIImage(named: "bread"),
                    itemSizeForClustering: CGSize(width: 44, height: 44),
                    clusterSizeForClustering: CGSize(width: 69, height: 44)
                )
            ]
        ),
        Floor(
            index: 2,
            title: "Floor 2",
            walkingSimulationPath: [
                CGPoint(x: 9, y: 18),
                CGPoint(x: 15, y: 18),
                CGPoint(x: 15, y: 6),
                CGPoint(x: 9, y: 6),
            ],
            markedAreas: [
                [
                    CGPoint(x: 30, y: 15.5), CGPoint(x: 32.6, y: 15.5), CGPoint(x: 32.6, y: 14),
                    CGPoint(x: 36.5, y: 14), CGPoint(x: 36.5, y: 8.5), CGPoint(x: 30, y: 8.5)
                ],
                [
                    CGPoint(x: 17.5, y: 1), CGPoint(x: 17.5, y: 5), CGPoint(x: 30, y: 5), CGPoint(x: 30, y: 1)
                ]
            ],
            texts: [
                Text(
                    coordinates:  CGPoint(x: 33.2, y: 11),
                    text: "Area #2",
                    color: UIColor.darkGray,
                    font: UIFont.systemFont(ofSize: 14, weight: .medium)
                ),
                Text(
                    coordinates:  CGPoint(x: 23.5, y: 3),
                    text: "Cashiers",
                    color: UIColor.darkGray,
                    font: UIFont.systemFont(ofSize: 14, weight: .medium)
                )
            ],
            imageName: "Floor2",
            mapSizeMeters: CGSize(width: 39.4, height: 41.88),
            pixelToMeter: 25,
            products: []
        )
    ]

    override func viewDidLoad() {
        super.viewDidLoad()

        let distanceBased = DistanceBasedClustering<Product>(
            intersectionDefinition: .maxSide,
            unitsConverter: self.unitsConverter
        )

        let intersectionBased = IntersectionBasedClustering<Product>(
            unitsConverter: self.unitsConverter,
            itemAnchorPoint: CGPoint(x: 0.5, y: 0.5),
            clusterAnchorPoint: CGPoint(x: 0.5, y: 0.5)
        )
        

        self.productsClusterManager = ClusterManager(
            itemsLevel: MapLevel.products.rawValue,
            clusteringAlgorithm: intersectionBased,
            mapView: self.mapView,
            unitsConverter: self.unitsConverter,
            singleItemProvider: { product, unitsConverter in
                ProductLayer(
                    product: product,
                    unitsConverter: unitsConverter
                )
            },
            clusterItemProvider: { groupedProducts, unitsConverter in
                ClusterLayer(
                    products: groupedProducts,
                    unitsConverter: unitsConverter,
                    iconSize: CGSize(width: 30, height: 30),
                    textSize: CGSize(width: 27, height: 15)
                )
            }
        )


        // The delegate will allow us to react to the user interactions with the map
        self.mapView.delegate = self

        self.mapView.backgroundColor = .clear
        self.mapView.layer.shadowOpacity = 1
        self.mapView.layer.shadowColor = UIColor(white: 0, alpha: 0.5).cgColor
        self.mapView.layer.shadowOffset = .zero
        self.mapView.layer.shadowRadius = 2

        // Coordinates grid might be handy in development and debugging.
        // But be aware that it does not work well with map rotation.
//        self.mapView.isRotationEnabled = false
//        self.mapView.showCoordinatesGridLegend()
//        self.mapView.addContentItem(
//            CoordinatesGridLayer(
//                id: "CoordinatesGrid",
//                config: CoordinatesGridVisualConfig(),
//                unitsConverter: self.unitsConverter
//            ),
//            at: 0
//        )
        self.setupFloorSelector()
        self.setupUserPosition()
        self.setupNavigationPath()
        self.onUserChangedFloor()
    }

    // MARK: - User Position Management
    private func setupUserPosition() {
        let layer = UserPositionLayer(
            headingConfig: HeadingVisualConfig(
                color: UIColor.blue.withAlphaComponent(0.5),
                openningAngle: CGFloat(Float.pi/3),
                radius: 24 //points
            ),
            accuracyConfig: AccuracyVisualConfig(
                color: UIColor.blue.withAlphaComponent(0.25)
            ),
            positionConfig: PositionVisualConfig(
                fillColor: .blue,
                strokeColor: .white,
                radius: 8, //points
                strokeWidth: 2 //points
            ),
            unitsConverter: self.unitsConverter
        )

        mapView.addContentItem(layer, at: MapLevel.user.rawValue)
        self.userPositionLayer = layer

        setupUserPositionState(isIdle: true)

        let manager  = UserPositionManager()

        layer.move(to: manager.currentPosition?.coordinates ?? .zero, animated: false)

        manager.onUserPositionUpdated = { [weak self] position in
            self?.applyUserPosition(position, animated: true)
        }

        manager.onFocusOnUserRequired = { [weak self] position in
            guard let strong = self else {
                return
            }

            let floor = strong.floors[strong.floorSelector.selectedSegmentIndex]

            if floor.index != position.floorIndex,
               let index = strong.floors.firstIndex(where: { $0.index == position.floorIndex }) {
                strong.floorSelector.selectedSegmentIndex = index
                strong.applyFloorSelection()
            }

            strong.mapView.focus(
                on: position.coordinates,
                offset: 0.3 * strong.view.frame.height - 0.5 * strong.view.frame.height,
                headingAngle: position.heading,
                animated: true
            )
        }

        self.userPositionManager = manager

        var walkingPath: [(floorIndex: Int, coordinates: CGPoint)] = []

        for floor in floors {
            walkingPath.append(contentsOf: floor.walkingSimulationPath.map({ (floorIndex: floor.index, coordinates: $0) }))
        }

        for floor in floors.reversed() {
            walkingPath.append(contentsOf: floor.walkingSimulationPath.reversed().map({ (floorIndex: floor.index, coordinates: $0) }))
        }

        manager.path = walkingPath
    }

    private func applyUserPosition(_ position: UserPosition, animated: Bool) {
        let floor = floors[floorSelector.selectedSegmentIndex]

        self.userPositionLayer.setHidden(floor.index != position.floorIndex, animated: animated)
        self.userPositionLayer.move(to: position.coordinates, animated: animated)
        self.userPositionLayer.setHeadingAngle(position.heading, animated: animated)
        self.userPositionLayer.setAccuracyRadiusMeters(position.accuracy, animated: animated)
    }

    private func setupUserPositionState(isIdle: Bool) {
        self.userPositionLayer.setUserPositionColor(isIdle ? UIColor.gray : UIColor.blue, animated: true)
        self.userPositionLayer.setAccuracyHidden(isIdle, animated: true)
        self.userPositionLayer.setHeadingHidden(isIdle, animated: true)
    }

    // MARK: - Floor Selection Management
    private func setupFloorSelector() {
        floorSelector.removeAllSegments()
        for i in 0..<floors.count {
            let floor = floors[i]
            floorSelector.insertSegment(withTitle: floor.title, at: i, animated: false)
        }
        floorSelector.selectedSegmentIndex = 0
    }

    private func applyFloorSelection() {
        let floor = floors[floorSelector.selectedSegmentIndex]

        guard let image = UIImage(named: floor.imageName) else {
            print("No image no fun")
            return
        }

        // To keep map focus while moving between floors
        // we save the state and re-apply it after map changed
        // if will only work well if the images 'match',
        // which is usually the case for floors in the same building
        let center = self.mapView.currentFocusCoordinates
        let heading = self.mapView.currentHeading
        let zoomMeters = self.mapView.currentFocusZoomMeters

        // providing the information about the floor map
        self.unitsConverter.mapSizeInMeters = floor.mapSizeMeters
        self.unitsConverter.mapImagePixelToMeter = floor.pixelToMeter
        self.unitsConverter.mapImageToBuildingOffset = .zero

        // as we now have information about the map we can apply units converter
        if self.mapView.unitsConverter == nil {
            mapView.unitsConverter = unitsConverter
        }

        // setting selected floor map image
        self.mapView.setMapImage(image)

        // All the values are optional as the mapView may not be ready to provide them
        // e.g. units conveter has not beet set
        if let center = center,
           let heading = heading,
           let zoomMeters = zoomMeters {
            self.mapView.focus(
                on: center,
                zoomMeters: zoomMeters,
                headingAngle: heading,
                animated: false
            )
        }

        // Applying selected floor content
        navigationPathLayer.path = [floor.walkingSimulationPath]

        if let position = userPositionManager.currentPosition {
            self.applyUserPosition(position, animated: false)
        }

        setupPins(for: floor)
        setupMarkedAreas(for: floor)
        setupTexts(for: floor)
        setupFloorTransition()
        setupProducts(for: floor)
    }

    // MARK: - Map content
    private func setupNavigationPath() {
        let layer = PolylineLayer(
            id: "NavigationPathLayer",
            config: PolylineVisualConfig(
                fillColor: UIColor(red: 130.0/255.0, green: 154.0/255.0, blue: 233.0/255.0, alpha: 0.8),
                strokeColor: UIColor(red: 65.0/255.0, green: 90.0/255.0, blue: 170.0/255.0, alpha: 0.8),
                lineWidthMeters: 0.2,
                strokeWidthMeters: 0.07,
                lineCap: .square,
                lineJoint: .bevel
            ),
            unitsConverter: self.unitsConverter
        )

        mapView.addContentItem(layer, at: MapLevel.navigationPath.rawValue)

        self.navigationPathLayer = layer
    }

    private func setupProducts(for floor: Floor) {
        productsClusterManager.set(items: floors[floorSelector.selectedSegmentIndex].products)
    }

    private func setupFloorTransition() {
        mapView.removeContentItems(at: MapLevel.floorTransitions.rawValue)

        guard let coordinates = floors.first?.walkingSimulationPath.last else {
            return
        }

        let selected = floors[floorSelector.selectedSegmentIndex]
        let target = floors.firstIndex(where: { $0.index != selected.index }) ?? floorSelector.selectedSegmentIndex
        let targetFloor = floors[target]

        let layer = FloorTransitionLayer(
            id: "TransitionTo.\(target)",
            iconImage: FloorTransitionImage.elevator,
            floorText: "\(targetFloor.index)",
            direction: floorSelector.selectedSegmentIndex > target ? .down : .up,
            coordinates: coordinates,
            config: FloorTransitionVisualConfig(
                backgroundColor: UIColor(red: 130.0/255.0, green: 154.0/255.0, blue: 233.0/255.0, alpha: 1),
                backgroundStrokeColor: UIColor(red: 65.0/255.0, green: 90.0/255.0, blue: 170.0/255.0, alpha: 1),
                badgeColor: UIColor(red: 65.0/255.0, green: 90.0/255.0, blue: 170.0/255.0, alpha: 1)
            ),
            unitsConverter: self.unitsConverter
        )

        mapView.addContentItem(layer, at: MapLevel.floorTransitions.rawValue)
    }

    private func setupPins(for floor: Floor) {
        mapView.removeContentItems(at: MapLevel.pins.rawValue)

        let selected = floors[floorSelector.selectedSegmentIndex]

        guard let coordinates = floorSelector.selectedSegmentIndex == 0 ? selected.walkingSimulationPath.first : selected.walkingSimulationPath.last else {
            return
        }

        let layer = ImageLayer(
            id: UUID().uuidString,
            coordinates: coordinates,
            image: UIImage(named: "pin"),
            config: ImageVisualConfig(
                size: CGSize(width: 30, height: 37),
                shouldScaleWithContent: false,
                shouldRotateWithContent: false,
                anchorPoint: CGPoint(x: 0.5, y: 1),
                clickableAreaExtension: .zero,
                contentsGravity: .resizeAspect
            ),
            unitsConverter: self.unitsConverter
        )

        layer.isSelectable = true

        mapView.addContentItem(layer, at: MapLevel.pins.rawValue)
    }

    private func setupMarkedAreas(for floor: Floor) {
        mapView.removeContentItems(at: MapLevel.markedAreas.rawValue)

        for area in floor.markedAreas {
            let layer = PolygonLayer(
                id: UUID().uuidString,
                vertices: area,
                config: PolygonVisualConfig(
                    fillColor: UIColor.orange.withAlphaComponent(0.2),
                    strokeColor: UIColor.orange.withAlphaComponent(0.5),
                    strokeWidth: 1.5
                ),
                unitsConverter: self.unitsConverter
            )

            mapView.addContentItem(layer, at: MapLevel.markedAreas.rawValue)
        }
    }

    private func setupTexts(for floor: Floor) {
        mapView.removeContentItems(at: MapLevel.texts.rawValue)

        for text in floor.texts {
            let layer = TextLayer(
                id: UUID().uuidString,
                coordinates: text.coordinates,
                text: NSAttributedString(
                    string: text.text,
                    attributes: [
                        NSAttributedString.Key.font: text.font,
                        NSAttributedString.Key.foregroundColor: text.color.cgColor
                    ]
                ),
                config: TextVisualConfig(
                    shouldScaleWithContent: true,
                    shouldRotateWithContent: false
                ),
                unitsConverter: self.unitsConverter
            )

            mapView.addContentItem(layer, at: MapLevel.markedAreas.rawValue)
        }
    }

    // MARK: - Actions
    private func onProductTapped(_ product: Product) {
        mapView.focus(on: product.coordinate, animated: true)
        let alert = UIAlertController(
            title: "You really clicked that \(product.name)",
            message: "",
            preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func onClusterTapped(_ layer: ClusterLayer) {
        if let current = mapView.currentFocusZoomMeters,
           abs(current - mapView.maxGestureZoomMeters) < .ulpOfOne {
           // we can not zoom any closer, lets just show the list of items
        } else {
            self.mapView.show(
                coordinates: layer.products.map { $0.coordinate },
                marginPoints: 27,
                animated: true
            )
        }
    }

    @IBAction func onUserChangedFloor() {
        self.userPositionManager.onUserInteractionStarted()
        self.userPositionManager.onUserInteractionEnded()
        self.applyFloorSelection()
    }

    @IBAction func toggleUserMovement(_ sender: Any) {
        guard let manager = userPositionManager else {
            return
        }

        if manager.isRunning {
            self.togglePlayButton.setImage(UIImage(systemName: "play.fill"), for: .normal)
            manager.stopMovementSimulation()
        } else {
            self.togglePlayButton.setImage(UIImage(systemName: "stop.fill"), for: .normal)
            manager.startMovementSimulation()
        }

        setupUserPositionState(isIdle: !manager.isRunning)
    }
}

// MARK: - IndoorMapViewDelegate
/// We handle only tap and interaction methods,
/// but the delegate also reports pan, pinch, zoom, long-press, double-tap and more
extension ViewController: IndoorMapViewDelegate {
    func onUserInteractionStarted(in mapView: IndoorMapView) {
        userPositionManager?.onUserInteractionStarted()
    }

    func onUserInteractionEnded(in mapView: IndoorMapView) {
        userPositionManager?.onUserInteractionEnded()
    }

    func onCoordinateTapped(_ coordinate: CGPoint, in mapView: IndoorMapView) {
        if let tappedTransition = mapView.items(at: coordinate)[MapLevel.floorTransitions.rawValue]?.first as? FloorTransitionLayer,
           let targetIndexString = tappedTransition.id.split(separator: ".").last,
           let targetIndex = Int(targetIndexString) {

            if targetIndex < 0 || targetIndex >= floors.count {
                return
            }

            floorSelector.selectedSegmentIndex = targetIndex
            applyFloorSelection()
            return
        }

        if let tappedPinLayer = mapView.items(at: coordinate)[MapLevel.pins.rawValue]?.first as? ImageLayer {
            mapView.focus(on: tappedPinLayer.coordinates, animated: true)
            return
        }

        if let tappedProductLayer = mapView.items(at: coordinate)[MapLevel.products.rawValue]?.first as? ProductLayer {
            self.onProductTapped(tappedProductLayer.product)
            return
        }

        if let tappedClusterLayer = mapView.items(at: coordinate)[MapLevel.products.rawValue]?.first as? ClusterLayer {
            self.onClusterTapped(tappedClusterLayer)
            return
        }
    }
}

