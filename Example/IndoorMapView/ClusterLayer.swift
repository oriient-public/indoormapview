//
//  ClusterLayer.swift
//  IndoorMapView_Example
//
//  Created by Michael Krutoyarskiy on 17/11/2022.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import Foundation
import QuartzCore
import IndoorMapView
import UIKit

class ClusterLayer: CALayer {
    private let unitsConverter: UnitsConverter
    private let iconLayer: CALayer
    private let clusterId: String
    let coordinates: CGPoint
    let products: [Product]

    var isSelected: Bool = false

    init(
        products: [Product],
        unitsConverter: UnitsConverter,
        platformImage: UIImage? = UIImage(named: "clusterPlatform"),
        iconSize: CGSize,
        textSize: CGSize
    ) {
        self.coordinates = products[0].coordinate
        self.clusterId = products[0].id
        self.unitsConverter = unitsConverter
        self.iconLayer = CALayer()
        self.products = products
        super.init()

        let clusterSize = products[0].clusterSizeForClustering
        self.apply(platformImage: platformImage, size: clusterSize)
        self.setupIconLayer(size: iconSize, platformSize: clusterSize, icon: products[0].image, textSize: textSize)
        self.setupTextLayer(size: textSize, platformSize: clusterSize, iconSize: iconSize)
        self.position = unitsConverter.convertCoordinateToPointOnScreen(coordinates)
    }

    private func apply(platformImage: UIImage?, size: CGSize) {
        self.contentsGravity = .resizeAspect
        self.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        self.contents = platformImage?.cgImage
    }

    private func setupIconLayer(size: CGSize, platformSize: CGSize, icon: UIImage?, textSize: CGSize) {
        iconLayer.contentsGravity = .resizeAspect
        iconLayer.contents = icon?.cgImage
        iconLayer.frame = CGRect(
            x: (platformSize.width - size.width)/2 - textSize.width/2,
            y: (platformSize.height - size.height)/2,
            width: size.width,
            height: size.height
        )
        self.addSublayer(iconLayer)
    }
    
    private func setupTextLayer(size: CGSize, platformSize: CGSize, iconSize: CGSize) {
        let textLayer = CATextLayer()
        let productsCount = products.count - 1
        let isNumberOfProductsIsBig = productsCount > 99
        let fontSize: CGFloat = isNumberOfProductsIsBig ? 10 : 13
        let iconRightBorder = (platformSize.width - iconSize.width)/2 + iconSize.width / 2
        let iconGap: CGFloat = isNumberOfProductsIsBig ? 3 : 0
        textLayer.frame = CGRect(
            x: iconRightBorder + iconGap,
            y: (platformSize.height - size.height)/2 + (isNumberOfProductsIsBig ? 2 : 0),
            width: size.width,
            height: size.height
        )
        textLayer.contentsScale = UIScreen.main.scale
        textLayer.fontSize = fontSize
        textLayer.font = UIFont.systemFont(ofSize: fontSize, weight: .semibold)
        textLayer.alignmentMode = .center
        textLayer.string = isNumberOfProductsIsBig ? "+99.." : "+ \(productsCount)"
        textLayer.backgroundColor = UIColor.white.cgColor
        textLayer.foregroundColor = UIColor(red: 242/255, green: 98/255, blue: 34/255, alpha: 1).cgColor

        self.addSublayer(textLayer)
    }

    // required for the animation
    override init(layer: Any) {
        let layer = layer as! ClusterLayer
        self.unitsConverter = layer.unitsConverter
        self.coordinates = layer.coordinates
        self.clusterId = layer.clusterId
        self.iconLayer = CALayer(layer: layer.iconLayer)
        self.products = layer.products
        super.init(layer: layer)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


extension ClusterLayer: MapObject {
    var id: MapObjectID { clusterId }

    func setContainerScale(_ scale: CGFloat, rotation: CGFloat) {
        let scale = 1/scale
        self.transform = CATransform3DScale(CATransform3DMakeRotation(CGFloat(2 * Double.pi) - rotation, 0, 0, 1), scale, scale, 1)
    }

    func unitsConverterUpdated() {
        self.position = unitsConverter.convertCoordinateToPointOnScreen(coordinates)
    }

    var isSelectable: Bool { true }

    func isPointInside(_ point: CGPoint) -> Bool {
        if self.isHidden { return false }
        return self.frame.contains(point)
    }
}
