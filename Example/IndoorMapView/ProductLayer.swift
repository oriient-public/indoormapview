//
//  ProductLayer.swift
//  IndoorMapViewSample
//
//  Created by Michael Krutoyarskiy on 25/01/2022.
//

import Foundation
import QuartzCore
import IndoorMapView
import UIKit


class ProductLayer: CALayer {
    let product: Product
    let unitsConverter: UnitsConverter
    private let iconLayer: CALayer

    var isSelected: Bool = false

    init(
        product: Product,
        unitsConverter: UnitsConverter,
        platformImage: UIImage? = UIImage(named: "platform")
    ) {
        self.product = product
        self.unitsConverter = unitsConverter
        self.iconLayer = CALayer()
        super.init()

        let itemSize = product.itemSizeForClustering
        self.backgroundColor = UIColor.red.withAlphaComponent(0.2).cgColor
        self.apply(platformImage: platformImage, size: itemSize)
        self.setupIconLayer(size: product.iconSize, platfromSize: itemSize)
        self.position = unitsConverter.convertCoordinateToPointOnScreen(product.coordinate)
    }

    private func apply(platformImage: UIImage?, size: CGSize) {
        self.contentsGravity = .resizeAspect
        self.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        self.contents = platformImage?.cgImage
    }

    private func setupIconLayer(size: CGSize, platfromSize: CGSize) {
        iconLayer.contentsGravity = .resizeAspect
        iconLayer.contents = product.image?.cgImage
        iconLayer.frame = CGRect(
            x: (platfromSize.width - size.width)/2,
            y: (platfromSize.height - size.height)/2,
            width: size.width,
            height: size.height
        )
        self.addSublayer(iconLayer)
    }

    // required for the animation
    override init(layer: Any) {
        let layer = layer as! ProductLayer
        self.product = layer.product
        self.unitsConverter = layer.unitsConverter
        self.iconLayer = CALayer(layer: layer.iconLayer)
        super.init(layer: layer)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


extension ProductLayer: MapObject {
    var id: MapObjectID { product.id }

    func setContainerScale(_ scale: CGFloat, rotation: CGFloat) {
        let scale = 1/scale
        self.transform = CATransform3DScale(CATransform3DMakeRotation(CGFloat(2 * Double.pi) - rotation, 0, 0, 1), scale, scale, 1)
    }

    func unitsConverterUpdated() {
        self.position = unitsConverter.convertCoordinateToPointOnScreen(product.coordinate)
    }

    var isSelectable: Bool { true }

    func isPointInside(_ point: CGPoint) -> Bool {
        if self.isHidden { return false }
        return self.frame.contains(point)
    }
}
