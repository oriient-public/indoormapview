//
//  UserPositionManager.swift
//  IndoorMapView_Example
//
//  Created by Michael Krutoyarskiy on 04/11/2020.
//  Copyright © 2020 Oriient. All rights reserved.
//

import Foundation
import CoreGraphics

/// This class will simulate user position updates which we can then present on the map
/// When `startMovementSimulation` the managed starts to infinitely imitate a walk from the start of the `path` to the end.
/// The manager also reports when auto-focus on the user position required, see `onFocusOnUserRequired`.
/// Auto-focus is disabled every time `onUserInteractionWithMap` called but recovers after `autofocusRecoveryTime`.
class UserPositionManager {
    /// Called when it is required to focus the map on user position
    var onFocusOnUserRequired: ((UserPosition) -> Void)?

    /// Called when the user position updated and it should be represented on the map
    var onUserPositionUpdated: ((UserPosition) -> Void)?

    /// true if the manager actively simulates a walk
    private(set) var isRunning = false

    /// Last appied position
    private(set) var currentPosition: UserPosition?

    /// The next index in `path` we are currently heading to
    private var targetIndexInPath: Int = 0

    /// Next scheduled step
    private var step: DispatchWorkItem?

    /// true if we should request focus for the next user position, false if should not (usually becase of a recent user interaction)
    private var shouldKeepFocus = true

    /// Scheduled auto-focus recovery
    private var focusRecovery: DispatchWorkItem?

    /// Distance we imitate per each step
    private let movementPerStep: CGFloat = 0.6

    /// Time interval between steps
    private let stepDuration: Double = 0.3

    /// How long do we wait after user interaction before recovering the auto-focus
    private let autofocusRecoveryTime: Double = 3

    /// Path used to imitate the walk, start -> end, start -> end, etc
    /// To imitate a end -> start walk the path should containt relevant coordinates
    var path: [(floorIndex: Int, coordinates: CGPoint)] = [] {
        didSet {
            if isRunning {
                stopMovementSimulation()
                startMovementSimulation()
            } else {
                let currentPosition = UserPosition(
                    floorIndex: path[0].floorIndex,
                    coordinates: path[0].coordinates,
                    heading: path[0].coordinates.angle(to: path[1].coordinates),
                    accuracy: CGFloat(Float.random(in: 3...5))
                )
                self.currentPosition = currentPosition
                onUserPositionUpdated?(currentPosition)
            }
        }
    }

    /// Report user interactions which should cancel auto-focus
    func onUserInteractionStarted() {
        shouldKeepFocus = false
        focusRecovery?.cancel()
    }

    func onUserInteractionEnded() {
        if shouldKeepFocus {
            return
        }

        let task = DispatchWorkItem { [weak self] in
            self?.shouldKeepFocus = true
        }
        self.focusRecovery = task
        DispatchQueue.main.asyncAfter(deadline: .now() + self.autofocusRecoveryTime, execute: task)
    }

    /// Start movement from the start of the `path`
    func startMovementSimulation() {
        if isRunning {
            return
        }
        isRunning = true

        if path.count < 2 {
            return
        }

        currentPosition = UserPosition(
            floorIndex: path[0].floorIndex,
            coordinates: path[0].coordinates,
            heading: path[0].coordinates.angle(to: path[1].coordinates),
            accuracy: CGFloat(Float.random(in: 3...5))
        )
        targetIndexInPath = 1
        makeAStep()
    }

    /// Stop movement
    func stopMovementSimulation() {
        isRunning = false
        step?.cancel()
        focusRecovery?.cancel()
    }

    /// Each step moves `currentPosition` closer to `path[targetIndexInPath]`
    /// heading calculated using starting and target coordinates
    /// position accuracy is randomized
    private func makeAStep() {
        guard let currentPosition = currentPosition else {
            return
        }

        let targetPosition = path[targetIndexInPath]
        let distanceLeft = currentPosition.coordinates.distance(to: targetPosition.coordinates)

        // we are very close to the tarted so it is the last step
        if distanceLeft < 1.5*movementPerStep {
            targetIndexInPath = (targetIndexInPath + 1)%path.count

            let newTarget = path[targetIndexInPath]
            let heading = targetPosition.coordinates.angle(to: newTarget.coordinates)

            self.currentPosition = UserPosition(
                floorIndex: targetPosition.floorIndex,
                coordinates: targetPosition.coordinates,
                heading: heading,
                accuracy: CGFloat(Float.random(in: 3...5))
            )
        } else {
            let heading = currentPosition.coordinates.angle(to: targetPosition.coordinates)

            self.currentPosition = UserPosition(
                floorIndex: currentPosition.floorIndex,
                coordinates: CGPoint(
                    x: currentPosition.coordinates.x + movementPerStep * cos(heading),
                    y: currentPosition.coordinates.y + movementPerStep * sin(heading)
                ),
                heading: currentPosition.heading,
                accuracy: currentPosition.accuracy
            )
        }

        onUserPositionUpdated?(currentPosition)

        if shouldKeepFocus {
            onFocusOnUserRequired?(currentPosition)
        }

        step?.cancel()
        let step = DispatchWorkItem { [weak self] in
            self?.makeAStep()
        }
        self.step = step
        DispatchQueue.main.asyncAfter(deadline: .now() + self.stepDuration, execute: step)
    }
}
