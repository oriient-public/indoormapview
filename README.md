# IndoorMapView

[![Version](https://img.shields.io/cocoapods/v/IndoorMapView.svg?style=flat)](https://cocoapods.org/pods/IndoorMapView)
[![License](https://img.shields.io/cocoapods/l/IndoorMapView.svg?style=flat)](https://cocoapods.org/pods/IndoorMapView)
[![Platform](https://img.shields.io/cocoapods/p/IndoorMapView.svg?style=flat)](https://cocoapods.org/pods/IndoorMapView)

IndoorMapView provides a basic toolkit to present an interactive indoor map to a user

## Important: Zooming and scrolling behaves quite unpredictably on the simulators, please test using a real device.

## Overview

IndoorMapView helps you to build an interactive indoor map expierence.

### Main features: 

- Scrolling, zoom and rotation of the map;
- Focusing on a specific point with required scale and rotation angle;
- Conversion between screen and building coordinate systems;
- Prepared set of popular map objects;
- Custom map objects;
- Objects selection and other user interactions;


### `UnitsConverter` interface allows to support any coordinate system.

Alerady implemented `CartesianUnitsConverter` provides conversion between UIKit coordinate system 
(Units are points of the screen, origin is at Top-Left, X - oriented to the right, Y - oriented downwards, 0 rotation corresponds to the Y axis)
and Cartesian metric coordinate system 
(Units are meters, origin is at Bottom-Left, X - oriented to the right, Y - oriented upwards, 0 rotation corresponds to the X axis).
But any other coordinate system can be used by implementing custom `UnitsConverter`.

#### Available map objects:
    
- User position (blue-dot with heading and accuracy radius)
- Circle
- Polygon
- Polyline
- Image (allows asynchronous loading of the images)
- Text (Supports NSAttributed string)
- Floor transition (with default set of icons)

**To add a new map object type just subclass `CALayer` and implement simple `MapObject` protocol.**

## Example

The example demonstrates the usage of `IndoorMapView` to present a building with two floors to the user.

**To run the example project, clone the repo, and run `pod install` from the Example directory first.**

All the objects are defined using their coordinates in the building, which placed correctly on the screen thanks to the `CartesianUnitsConverter`.
You can see a navigation path between two orange pins on the different floors connected by an elevator (clickable floor transition layer). 
Click **Play** button to start user walk imitation, the map will automatically follow the user movements and direction.
There are also a few marked (with polygons layers) and signed (with text layers) areas on the map.

## Requirements

The IndoorMapView depends on UIKit and [InteractiveContainerView](https://gitlab.com/oriient-public/interactivecontainerview)  and supports iOS 10+.

## Installation

IndoorMapView is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'IndoorMapView'
```

## License

IndoorMapView is available under the Oriient New Media Ltd. license. See the LICENSE file for more info.
