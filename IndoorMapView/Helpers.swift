//
//  Helpers.swift
//  IndoorMapView
//
//  Created by Michael Krutoyarskiy on 04/11/2020.
//

import Foundation
import QuartzCore
import UIKit

public extension CALayer {
    @objc func setHidden(_ hidden: Bool, animated: Bool) {
        if animated && self.isHidden != hidden {
            self.addFadeAnimation()
        }
        self.isHidden = hidden
    }
}

extension UIColor {
    func image(of size: CGSize) -> UIImage {
        return UIGraphicsImageRenderer(size: size).image { rendererContext in
            self.setFill()
            rendererContext.fill(CGRect(origin: .zero, size: size))
        }
    }
}
