//
//  IPSUnitsConverter.swift
//  IPSMapView
//
//  Created by Michael Krutoyarskiy on 02/04/2020.
//  Copyright © 2020 Michael Krutoyarskiy. All rights reserved.
//

import Foundation
import UIKit

/// UnitsConverter is used to convert coordinates/angles/distances between some building coordinate system and screen coordinate system
/// Most of the parameters are set by the MapView,
/// but the map image's information (mapImagePixelToMeter, mapImageToBuildingOffset, mapSizeInMeters)
/// should be set manually when setting the map image ot MapView
@objc public protocol UnitsConverter {
    // MARK: Parameters
    /// How many pixels of the map image corresponds to a physical meter (set by client)
    var mapImagePixelToMeter: CGFloat { get set }

    /// Offset of the building's coordinate system's origin (set by client)
    var mapImageToBuildingOffset: CGPoint { get set }

    /// Size of the map image in meters (set by client)
    var mapSizeInMeters: CGSize { get set }

    /// Current content container scale (set by MapView)
    var containerScale: CGFloat { get set }

    /// Current content container rotation (set by MapView)
    var containerRotation: CGFloat { get set }

    /// The scale applied to the content to fit the container (set by MapView)
    var containerContentScale: CGFloat { get set }

    /// The offset of the content inside the container (set by MapView)
    var containerContentOffset: CGPoint { get set }

    /// Pixels per point (set by MapView)
    var screenScale: CGFloat { get set }

    /// Bounds of the currently visible part of the content (set by MapView)
    var visibleBounds: CGRect { get set }

    // MARK: Conversion methods
    /// Converts physical user heading to the user heading rotation on map
    func convertHeadingAngleToUserRotation(_ angle: CGFloat) -> CGFloat

    /// Converts the user heading rotation on map to physical user heading
    func convertUserRotationToHeadingAngle(_ angle: CGFloat) -> CGFloat

    /// Calculates map rotation to apply so the user heading will appear directed to the top of the screen
    func convertHeadingAngleToMapRotation(_ angle: CGFloat) -> CGFloat

    /// Calculates user heading which matches map rotation directed to the top of the screen
    func convertMapRotationToHeadingAngle(_ angle: CGFloat) -> CGFloat

    /// Converts distance in meters to distance in screen points
    func convertMetersToPoints(_ meters: CGFloat) -> CGFloat

    /// Converts distance in screen points to distance in meters
    func convertPointsToMeters(_ points: CGFloat) -> CGFloat

    /// Converts coordinates relative to a building coordinates system to points relative to the content layer
    func convertCoordinateToPointOnScreen(_ coordinates: CGPoint) -> CGPoint

    /// Converts points relative to the content layer to coordinates relative to a building
    func convertPointOnScreenToCoordinate(_ point: CGPoint) -> CGPoint

    /// Converts focus meters (how many meters of the map should be visible on screen horisontally) to content scale factor
    func convertFocusMetersToZoomScale(_ meters: CGFloat) -> CGFloat

    /// Converts content scale factor to focus meters (how many meters of the map should be visible on screen horisontally)
    func convertZoomScaleToFocusMeters(_ zoomScale: CGFloat) -> CGFloat

    /// Converts coordinates relative to a building to pixels relative to the map image
    func convertCoordinatesToPointOnImage(_ coordinates: CGPoint) -> CGPoint

    /// Converts pixels relative to the map image to coordinates in meters relative to a building
    func convertPointOnImageToCoordinates(_ point: CGPoint) -> CGPoint
}

