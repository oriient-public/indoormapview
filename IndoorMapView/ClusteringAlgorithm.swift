import UIKit

/// A protocol that defines the methods to implement to cluster items
public protocol ClusteringAlgorithm {
    // Placeholder name to a type that is going to be clustered
    associatedtype ItemType
    
    /// Function that makes clusters from items
    ///
    /// - Parameters:
    ///     - S: Generic sequence of items to be clustered
    ///
    ///  - Note: returns two-dimensional array of items
    func cluster<S: Sequence>(items: S) -> [[ItemType]] where S.Element == ItemType
}

public extension ClusteringAlgorithm {

    /// Function with O(n^2) complexity to create clusters
    ///
    /// - Parameters:
    ///     - S: Generic sequence of items to be clustered
    ///     - ShouldBeClustered: Function that decides if an item and a cluster should be clustered
    ///
    ///  - Note: returns two-dimensional array of items
    func greedyClustering<S: Sequence>(
        items: S,
        shouldBeClustered: (ItemType, [ItemType]) -> Bool
    ) -> [[ItemType]] where S.Element == ItemType {
        var clusters: [[ItemType]] = []

        items.forEach { item in
            if let clusterIndex = clusters.firstIndex(
                where: { cluster in shouldBeClustered(item, cluster) }
            ) {
                // item belongs to one of the existing clusters => we will add it to this cluster
                var cluster = clusters[clusterIndex]
                cluster.append(item)
                clusters[clusterIndex] = cluster
            } else {
                // item does not belong to any of the existing clusters, we will create a new cluster made of this item
                clusters.append([item])
            }
        }

        return clusters
    }
}

/// A protocol that define a mandatory parameter of an item to be clustered
public protocol ClusteringCompatible: Hashable {
    var coordinate: CGPoint { get }
    var itemSizeForClustering: CGSize { get }
    var clusterSizeForClustering: CGSize { get }
}

/// Clustering algorithm based on distance between two items
public struct DistanceBasedClustering<ItemType>: ClusteringAlgorithm where ItemType: ClusteringCompatible {
    public typealias ItemType = ItemType

    private let unitsConverter: UnitsConverter
    private let intersectionDefinition: IntersectionDefinition

    public enum IntersectionDefinition {
        case diagonal
        case maxSide
    }

    /// Algorithm initializer
    ///
    /// - Parameters:
    ///     - IntersectionDefinition: Enum that declares a rule for clustering: diagonal or maxSide
    ///     - ItemSize: Size of a single item
    ///     - ClusterItemSize: Size of a cluster item
    ///     - UnitsConverter: Helper to convert units
    ///
    public init(
        intersectionDefinition: IntersectionDefinition,
        unitsConverter: UnitsConverter
    ) {
        self.intersectionDefinition = intersectionDefinition
        self.unitsConverter = unitsConverter
    }

    public func cluster<S>(items: S) -> [[ItemType]] where S : Sequence, ItemType == S.Element {
        return greedyClustering(items: items) { item, cluster in
            let clusterItem = cluster[0]
            let minimumDistance: CGFloat = minimumDistance(for: clusterItem, itemsCount: cluster.count)
            // Calculate a required distance between item and cluster to be cluster
            let scaledDistance = minimumDistance/self.unitsConverter.containerScale
            
            return self.unitsConverter.convertMetersToPoints(item.coordinate.distance(to: clusterItem.coordinate)) <= scaledDistance
        }
    }
    
    private func minimumDistance(for item: ItemType, itemsCount: Int) -> CGFloat {
        let itemSize = item.itemSizeForClustering
        let clusterSize = item.clusterSizeForClustering
        
        switch intersectionDefinition {
        case .diagonal:
            return itemsCount == 1
            ? hypot(itemSize.width, itemSize.height)
            : hypot(clusterSize.width, clusterSize.height)
        case .maxSide:
            return itemsCount == 1
            ? max(itemSize.width, itemSize.height)
            : max(clusterSize.width, clusterSize.height)
        }
    }
}

/// Clustering algorithm based on intersection between two items
public struct IntersectionBasedClustering<ItemType>: ClusteringAlgorithm where ItemType: ClusteringCompatible {
    public typealias ItemType = ItemType

    private let unitsConverter: UnitsConverter
    private let itemAnchorPoint: CGPoint
    private let clusterAnchorPoint: CGPoint

    /// Algorithm initializer
    ///
    /// - Parameters:
    ///     - UnitsConverter: Helper to convert units
    ///     - ItemAnchorPoint: Item image anchor point
    ///     - ClusterAnchorPoint: Cluster image anchor point
    ///
    public init(
        unitsConverter: UnitsConverter,
        itemAnchorPoint: CGPoint,
        clusterAnchorPoint: CGPoint
    ) {
        self.unitsConverter = unitsConverter
        self.itemAnchorPoint = itemAnchorPoint
        self.clusterAnchorPoint = clusterAnchorPoint
    }

    public func cluster<S>(items: S) -> [[ItemType]] where S : Sequence, ItemType == S.Element {
        let scale = CGAffineTransform(
            scaleX: self.unitsConverter.containerScale,
            y: self.unitsConverter.containerScale
        ).inverted()

        return greedyClustering(items: items) { item, cluster in
            let clusterItem = cluster[0]
            let itemSize = item.itemSizeForClustering
            let clusterItemSize = cluster.count == 1 ? clusterItem.itemSizeForClustering : clusterItem.clusterSizeForClustering
            let clusterAnchorPoint = cluster.count == 1 ? itemAnchorPoint : clusterAnchorPoint
            // Create a rectangle for a single item
            let itemRect = self.rectangle(
                size: itemSize,
                coordinates: item.coordinate,
                anchorPoint: itemAnchorPoint,
                scale: scale
            )
            // Create a rectangle for a cluster item
            let clusterRect = self.rectangle(
                size: clusterItemSize,
                coordinates: clusterItem.coordinate,
                anchorPoint: clusterAnchorPoint,
                scale: scale
            )
            // If they intersect => create a cluster
            return itemRect.intersects(clusterRect)
        }
    }

    private func rectangle(
        size: CGSize,
        coordinates: CGPoint,
        anchorPoint: CGPoint,
        scale: CGAffineTransform
    ) -> CGRect {
        let scaledSize = size.applying(scale)
        let origin = self.unitsConverter.convertCoordinateToPointOnScreen(coordinates)

        return CGRect(
            x: origin.x - anchorPoint.x*scaledSize.width,
            y: origin.y - anchorPoint.y*scaledSize.height,
            width: scaledSize.width,
            height: scaledSize.height
        )
    }
}

