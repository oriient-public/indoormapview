//
//  IndoorMapViewDelegate.swift
//  IPSMapView
//
//  Created by Michael Krutoyarskiy on 27/10/2020.
//  Copyright © 2020 Michael Krutoyarskiy. All rights reserved.
//

import Foundation
import UIKit

/// Allows to handle user interactions with the map
/// Important: The delegate methods for the gestures are called before map consumes the gestures to apply content transformations.
/// You can use `IndoorMapView.isMapTransformationEnabled` to disable the transformation and handle the gestures in a custom way.
/// Do not forget to re-enable when finished.
public protocol IndoorMapViewDelegate: AnyObject {
    /// User started interacting with the map
    func onUserInteractionStarted(in mapView: IndoorMapView)

    /// User stopped interacting with the map
    func onUserInteractionEnded(in mapView: IndoorMapView)
    
    /// Map transformation updated
    func onMapTransformationUpdated(in mapView: IndoorMapView)

    /// User long pressed the map
    /// - Parameters:
    ///   - coordinate: Location of the gesture
    ///   - state: State of the gesture
    ///   - mapView: The map view where the gesture happend
    func onCoordinateLongpressed(_ coordinate: CGPoint, state: UIGestureRecognizer.State, in mapView: IndoorMapView)
    
    /// User tapped the map
    /// - Parameters:
    ///   - coordinate: Location of the gesture
    ///   - mapView: The map view where the gesture happend
    func onCoordinateTapped(_ coordinate: CGPoint, in mapView: IndoorMapView)

    /// User double tapped the map
    /// - Parameters:
    ///   - coordinate: Location of the gesture
    ///   - mapView: The map view where the gesture happend
    func onCoordinateDoubleTapped(_ coordinate: CGPoint, in mapView: IndoorMapView)

    /// User dragged the map
    /// - Parameters:
    ///   - coordinate: Location of the gesture
    ///   - translation: Translation since the last method call
    ///   - state: State of the gesture
    ///   - mapView: The map view where the gesture happend
    func onContentDragged(to coordinate: CGPoint, translation: CGPoint, state: UIGestureRecognizer.State, in mapView: IndoorMapView)

    /// User pinched the map
    /// - Parameters:
    ///   - scaleDelta: Scale since the last call
    ///   - coordinate: Location of the point between the fingers
    ///   - state: State of the gesture
    ///   - mapView: The map view where the gesture happend
    func onContentPinched(scaleDelta: CGFloat, around coordinate: CGPoint, state: UIGestureRecognizer.State, in mapView: IndoorMapView)

    /// User rotated the map
    /// - Parameters:
    ///   - angleDelta: Rotation (degress) since the last call
    ///   - coordinate: Location of the point between the fingers
    ///   - state: State of the gesture
    ///   - mapView: The map view where the gesture happend
    func onContentRotated(angleDelta: CGFloat, around coordinate: CGPoint, state: UIGestureRecognizer.State, in mapView: IndoorMapView)
}

// To make all the methods optional
public extension IndoorMapViewDelegate {
    func onUserInteractionStarted(in mapView: IndoorMapView) { }

    func onUserInteractionEnded(in mapView: IndoorMapView) { }
    
    func onMapTransformationUpdated(in mapView: IndoorMapView) { }

    func onCoordinateLongpressed(_ coordinate: CGPoint, state: UIGestureRecognizer.State, in mapView: IndoorMapView) { }
    
    func onCoordinateTapped(_ coordinate: CGPoint, in mapView: IndoorMapView) { }

    func onCoordinateDoubleTapped(_ coordinate: CGPoint, in mapView: IndoorMapView) { }

    func onContentDragged(to coordinate: CGPoint, translation: CGPoint, state: UIGestureRecognizer.State, in mapView: IndoorMapView) { }

    func onContentPinched(scaleDelta: CGFloat, around coordinate: CGPoint, state: UIGestureRecognizer.State, in mapView: IndoorMapView) { }

    func onContentRotated(angleDelta: CGFloat, around coordinate: CGPoint, state: UIGestureRecognizer.State, in mapView: IndoorMapView) { }
}
