//
//  IndoorMapViewCodeResources.swift
//  IndoorMapView
//
//  Created by Michael Krutoyarskiy on 30/11/2022.
//

import Foundation

public final class IndoorMapViewCodeResources {
    public static let resourceBundle: Bundle = {
        let candidates = [
            // Bundle should be present here when the package is linked into an App.
            Bundle.main.resourceURL,

            // Bundle should be present here when the package is linked into a framework.
            Bundle(for: IndoorMapViewCodeResources.self).resourceURL,
        ]

        let bundleName = "IndoorMapView_IndoorMapView"

        for candidate in candidates {
            let bundlePath = candidate?.appendingPathComponent(bundleName + ".bundle")
            if let bundle = bundlePath.flatMap(Bundle.init(url:)) {
                return bundle
            }
        }

        // Return whatever bundle this code is in as a last resort.
        return Bundle(for: IndoorMapViewCodeResources.self)
    }()
}
