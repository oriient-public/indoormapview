//
//  CoordinatesGridLegendView.swift
//  IPSMapView
//
//  Created by Michael Krutoyarskiy on 26/08/2020.
//  Copyright © 2020 Michael Krutoyarskiy. All rights reserved.
//

import Foundation
import UIKit

/// Shows cordinates grid legend over the map. Does not support map rotation.
public class CoordinatesGridLegendView: UIView {

    // MARK: - Public properties
    public var legendBgColor: UIColor = UIColor(white: 1, alpha: 0.9) {
        didSet {
            setupLegendBg()
        }
    }

    public var legendStrokeColor: UIColor = UIColor(white: 0.3, alpha: 0.8) {
        didSet {
            setupLegendBg()
        }
    }

    public var legendWidth: CGFloat = 30 {
        didSet {
            setupLegendBg()
        }
    }

    public var legendHeight: CGFloat = {
        if let window = UIApplication.shared.keyWindow,
            #available(iOS 11.0, *) {
            return window.safeAreaInsets.bottom + 15
        }
        return 15
    }()
    {
        didSet {
            setupLegendBg()
            resetTextAttributes()
        }
    }

    public var textColor: UIColor = .blue {
        didSet {
            resetTextAttributes()
        }
    }

    public var font: UIFont = .systemFont(ofSize: 10) {
        didSet {
            resetTextAttributes()
        }
    }

    public var legendStrokeWidth: CGFloat = 1 {
        didSet {
            setupLegendBg()
        }
    }

    public override var bounds: CGRect {
        didSet {
            setupLegendBg()
            if let lastAppliedScale = lastAppliedScale {
                updateCoordinates(lastAppliedScale)
            }
        }
    }

    public var unitsConverter: UnitsConverter? {
        didSet {
            setupLegendBg()
            if let lastAppliedScale = lastAppliedScale {
                updateCoordinates(lastAppliedScale)
            }
        }
    }

    public var contentCoordinatesConverter: ((CGPoint) -> CGPoint)? {
        didSet {
            if let lastAppliedScale = lastAppliedScale {
                updateCoordinates(lastAppliedScale)
            }
        }
    }

    // MARK: - Private properties
    private let backgroundLayer = CAShapeLayer()
    private var lastAppliedScale: CGFloat?
    private var xTextLayers: [Int: CATextLayer] = [:]
    private var yTextLayers: [Int: CATextLayer] = [:]
    private var textHeight: CGFloat = 15

    // MARK: - Init
    public override init(frame: CGRect) {
        super.init(frame: frame)

        self.isUserInteractionEnabled = false
        self.backgroundColor = .clear
        self.layer.addSublayer(backgroundLayer)
        resetTextAttributes()
        setupLegendBg()
    }

    public func contentTransformed(_ scale: CGFloat) {
        updateCoordinates(scale)
    }

    // MARK: - Coordinates
    private var textAttributes: [NSAttributedString.Key: Any] = [:]

    private func resetTextAttributes() {
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center

        textHeight = ("999" as NSString).size(withAttributes: [.font: font]).height

        textAttributes = [
            NSAttributedString.Key.strokeWidth: 0,
            NSAttributedString.Key.foregroundColor: textColor.cgColor,
            NSAttributedString.Key.font: font,
            NSAttributedString.Key.paragraphStyle: paragraph
        ]
    }

    private func createNewLayer() -> CATextLayer {
        let textLayer = CATextLayer()

        textLayer.disableImplicitActions(for: "string")
        textLayer.disableImplicitPositionAnimation()
        textLayer.alignmentMode = CATextLayerAlignmentMode(rawValue: "center")
        textLayer.contentsScale = UIScreen.main.scale
        textLayer.alignmentMode = CATextLayerAlignmentMode.center

        self.layer.addSublayer(textLayer)
        return textLayer
    }

    private func updateCoordinates(_ scale: CGFloat) {
        guard let unitsConverter = self.unitsConverter,
              let contentCoordinatesConverter = self.contentCoordinatesConverter else {
            return
        }
        lastAppliedScale = scale
        let parameters = GridParameters.calculateGridParameters(scale: scale, unitsConverter: unitsConverter)

        func addCoordinate(
            coordinate: Int,
            frame: CGRect,
            unusedTextLayers: inout [Int: CATextLayer],
            textLayers: inout [Int: CATextLayer]
        ) {
            if let layer = unusedTextLayers.removeValue(forKey: coordinate) {
                layer.frame = frame
            } else {
                let layer = createNewLayer()
                textLayers[coordinate] = layer
                layer.frame = frame
                layer.string = NSAttributedString(string: "\(coordinate)", attributes: textAttributes)
            }
        }

        func cleanUnused(
            unusedTextLayers: inout [Int: CATextLayer],
            textLayers: inout [Int: CATextLayer]
        ) {
            unusedTextLayers.forEach { key, value in
                value.removeFromSuperlayer()
                textLayers.removeValue(forKey: key)
            }
        }

        // vertical grid
        var x: CGFloat = 0
        let textWidth: CGFloat = legendWidth
        var unusedTextLayers = xTextLayers
        while x <= parameters.maxX + parameters.spacingMeters {
            let pointX = contentCoordinatesConverter(unitsConverter.convertCoordinateToPointOnScreen(CGPoint(x: x, y: 0))).x
            if pointX >= bounds.minX + legendWidth && pointX <= bounds.maxX {
                addCoordinate(
                    coordinate: Int(x),
                    frame: CGRect(x: pointX - textWidth/2, y: bounds.maxY - legendHeight + 3, width: textWidth, height: legendHeight),
                    unusedTextLayers: &unusedTextLayers,
                    textLayers: &xTextLayers
                )
            }
            x += parameters.spacingMeters
        }

        cleanUnused(unusedTextLayers: &unusedTextLayers, textLayers: &xTextLayers)

        // horisontal grid
        var y: CGFloat = 0
        unusedTextLayers = yTextLayers

        while y <= parameters.maxY + parameters.spacingMeters {
            let pointY = contentCoordinatesConverter(unitsConverter.convertCoordinateToPointOnScreen(CGPoint(x: 0, y: y))).y
            if pointY >= bounds.minY && pointY <= bounds.maxY - legendHeight {
                addCoordinate(
                    coordinate: Int(y),
                    frame: CGRect(x: bounds.minX, y: pointY - textHeight/2, width: legendWidth, height: textHeight),
                    unusedTextLayers: &unusedTextLayers,
                    textLayers: &yTextLayers
                )
            }
            y += parameters.spacingMeters
        }

        cleanUnused(unusedTextLayers: &unusedTextLayers, textLayers: &yTextLayers)
    }

    // MARK: - BG
    private func setupLegendBg() {
        backgroundLayer.strokeColor = legendStrokeColor.cgColor
        backgroundLayer.fillColor = legendBgColor.cgColor
        backgroundLayer.lineWidth = legendStrokeWidth

        let bgPath = UIBezierPath()

        // bot-left
        bgPath.move(to: CGPoint(x: 0, y: bounds.width))

        // to top-left
        bgPath.addLine(to: CGPoint(x: 0, y: 0))

        // to top-left + Hwidth
        bgPath.addLine(to: CGPoint(x: legendWidth, y: 0))

        // to bot-left + Hwidth + Vwidth
        bgPath.addLine(to: CGPoint(x: legendWidth, y: bounds.height - legendHeight))

        // to bot-right + Vwidth
        bgPath.addLine(to: CGPoint(x: bounds.width, y: bounds.height - legendHeight))

        // to bot-right
        bgPath.addLine(to: CGPoint(x: bounds.width, y: bounds.height))

        // back to bot-left
        bgPath.addLine(to: CGPoint(x: 0, y: bounds.height))

        backgroundLayer.path = bgPath.cgPath
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
