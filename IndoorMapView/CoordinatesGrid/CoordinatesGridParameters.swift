//
//  CoordinatesGridParameters.swift
//  IPSMapView
//
//  Created by Michael Krutoyarskiy on 26/08/2020.
//  Copyright © 2020 Michael Krutoyarskiy. All rights reserved.
//

import Foundation
import CoreGraphics

/// Helper object with coordinates grid parameters
struct GridParameters {
    static let validSpacing: [CGFloat] = [1, 2, 5, 10, 20, 50, 100]
    static let subGridRatio: CGFloat = 10
    static let mainGridRatio: CGFloat = 10

    let spacing: CGFloat
    let spacingMeters: CGFloat
    let maxX: CGFloat
    let maxY: CGFloat

    public static func calculateGridParameters(scale: CGFloat, unitsConverter: UnitsConverter) -> GridParameters {
        let mapSize = unitsConverter.mapSizeInMeters
        let targetSpacing = mapSize.width/scale/Self.mainGridRatio
        var spacingMeters: CGFloat = 1

        for i in 0..<Self.validSpacing.count-1 {
            if abs(Self.validSpacing[i] - targetSpacing) < abs(Self.validSpacing[i+1] - targetSpacing) {
                spacingMeters = Self.validSpacing[i]
                break
            } else {
                spacingMeters = Self.validSpacing[i+1]
            }
        }

        return GridParameters(
            spacing: unitsConverter.convertMetersToPoints(spacingMeters),
            spacingMeters: spacingMeters,
            maxX: unitsConverter.mapSizeInMeters.width + max(0, unitsConverter.mapImageToBuildingOffset.x),
            maxY: unitsConverter.mapSizeInMeters.height + max(0, unitsConverter.mapImageToBuildingOffset.y)
        )
    }
}
