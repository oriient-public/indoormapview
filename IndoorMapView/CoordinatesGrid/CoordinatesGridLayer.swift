//
//  CoordinatesGrid.swift
//  IPSMapView
//
//  Created by Michael Krutoyarskiy on 25/08/2020.
//  Copyright © 2020 Michael Krutoyarskiy. All rights reserved.
//

import Foundation
import UIKit

/// Shows coordinates grid over the map. Does not support map rotation.
open class CoordinatesGridLayer: CALayer {
    public let id: String
    public var isSelectable: Bool = false
    private let config: CoordinatesGridVisualConfig
    private let unitsConverter: UnitsConverter

    private let gridLayer: CAShapeLayer
    private let subGridLayer: CAShapeLayer
    private var lastAppliedScale: CGFloat?
    public var drawSubgrid = false

    public init(id: String, config: CoordinatesGridVisualConfig, unitsConverter: UnitsConverter) {
        self.id = id
        self.config = config
        self.unitsConverter = unitsConverter
        self.gridLayer = CAShapeLayer()
        self.subGridLayer = CAShapeLayer()
        super.init()

        self.addSublayer(gridLayer)
        self.addSublayer(subGridLayer)

        self.masksToBounds = false
        self.frame = .zero
        self.applyVisualConfig(config)
    }

    override init(layer: Any) {
       let layer = layer as! CoordinatesGridLayer
       self.id = layer.id
       self.config = layer.config
       self.unitsConverter = layer.unitsConverter
       self.gridLayer = layer.gridLayer
       self.subGridLayer = layer.subGridLayer
       super.init(layer: layer)
    }

    private func applyVisualConfig(_ config: CoordinatesGridVisualConfig) {
        self.gridLayer.strokeColor = config.gridColor.cgColor
        self.subGridLayer.strokeColor = config.subGridColor.cgColor
        self.gridLayer.lineWidth = config.gridLineWidth
        self.subGridLayer.lineWidth = config.subGridLineWidth
    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func resetPaths(scale: CGFloat) {
        let parameters = GridParameters.calculateGridParameters(scale: scale, unitsConverter: unitsConverter)
        let spacing = parameters.spacing
        let visibleBounds = unitsConverter.visibleBounds.insetBy(dx: -2*spacing, dy: -2*spacing)

        let gridPath = UIBezierPath()
        let subGridPath = UIBezierPath()

        self.gridLayer.lineWidth = config.gridLineWidth/scale
        self.subGridLayer.lineWidth = config.subGridLineWidth/scale
        lastAppliedScale = scale

        func addVerticalSegment(x: CGFloat, addSubgrid: Bool) {
            let x = unitsConverter.convertCoordinateToPointOnScreen(CGPoint(x: x, y: 0)).x

            if x >= visibleBounds.minX && x <= visibleBounds.maxX {
                gridPath.move(to: CGPoint(x: x, y: visibleBounds.minY))
                gridPath.addLine(to: CGPoint(x: x, y: visibleBounds.maxY))
            } else {
                return
            }

            if !addSubgrid {
                return
            }

            var subX = x + spacing/GridParameters.subGridRatio
            if subX > visibleBounds.maxX {
                return
            }
            for _ in 1..<Int(GridParameters.subGridRatio) {
                if subX >= visibleBounds.minX && subX <= visibleBounds.maxX {
                    subGridPath.move(to: CGPoint(x: subX, y: visibleBounds.minY))
                    subGridPath.addLine(to: CGPoint(x: subX, y: visibleBounds.maxY))
                } else {
                    break
                }
                subX += spacing/GridParameters.subGridRatio
            }
        }

        func addHorisontalSegment(y: CGFloat, addSubgrid: Bool) {
            let y = unitsConverter.convertCoordinateToPointOnScreen(CGPoint(x: 0, y: y)).y

            if y >= visibleBounds.minY && y <= visibleBounds.maxY {
                gridPath.move(to: CGPoint(x: visibleBounds.minX, y: y))
                gridPath.addLine(to: CGPoint(x: visibleBounds.maxX, y: y))
            } else {
                return
            }

            if !addSubgrid {
                return
            }

            var subY = y - spacing/GridParameters.subGridRatio
            if subY > visibleBounds.maxY {
                return
            }

            for _ in 1..<Int(GridParameters.subGridRatio) {
                if subY >= visibleBounds.minY && subY <= visibleBounds.maxY {
                    subGridPath.move(to: CGPoint(x: visibleBounds.minX, y: subY))
                    subGridPath.addLine(to: CGPoint(x: visibleBounds.maxX, y: subY))
                } else {
                    break
                }
                subY -= spacing/GridParameters.subGridRatio
            }
        }

        // vertical
        var x: CGFloat = 0
        while x <= parameters.maxX{
            addVerticalSegment(x: x, addSubgrid: drawSubgrid)
            x += parameters.spacingMeters
        }
        addVerticalSegment(x: x, addSubgrid: false)

        // horisontal
        var y: CGFloat = 0
        while y <= parameters.maxY {
            addHorisontalSegment(y: y, addSubgrid: drawSubgrid)
            y += parameters.spacingMeters
        }
        addHorisontalSegment(y: y, addSubgrid: false)

        subGridLayer.path = subGridPath.cgPath
        gridLayer.path = gridPath.cgPath
    }
}

@objc public class CoordinatesGridVisualConfig: NSObject {
    let gridColor: UIColor
    let subGridColor: UIColor
    let gridLineWidth: CGFloat
    let subGridLineWidth: CGFloat

    public init(
        gridColor: UIColor = .blue,
        subGridColor: UIColor = .blue,
        gridLineWidth: CGFloat = 0.6,
        subGridLineWidth: CGFloat = 0.125
    ) {
        self.gridColor = gridColor
        self.subGridColor = subGridColor
        self.gridLineWidth = gridLineWidth
        self.subGridLineWidth = subGridLineWidth
        super.init()
    }
}

extension CoordinatesGridLayer: MapObject {
    public func setContainerScale(_ scale: CGFloat, rotation: CGFloat) {
        resetPaths(scale: scale)
    }

    public func isPointInside(_ point: CGPoint) -> Bool { false }

    public func unitsConverterUpdated() {
        if let lastAppliedScale = lastAppliedScale {
            resetPaths(scale:  lastAppliedScale)
        }
    }
}

