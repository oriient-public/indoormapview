//
//  IndoorMapView.swift
//  IPSMapView
//
//  Created by Michael Krutoyarskiy on 23/03/2020.
//  Copyright © 2020 Michael Krutoyarskiy. All rights reserved.
//

import Foundation
import Combine
import UIKit

@_exported import InteractiveContainerView

/// IndoorMapView manages user gestures to apply correct transformation to the content items.
/// Each content item is provided with the relevant updates of rotation/scale and coordinates invalidation,
/// Provide `unitsConverter` so the map view will be able to configure it will all the necessary parameters.
/// Use `setMapImage()` to set map image representation.
/// Use `delegate` to react to transform updates and user interactions.
/// Use `focus(on:zoomMeters:headingAngle:animationDuration:animated:)` to focus on some area of the map.
@objc public class IndoorMapView: UIView {
    // MARK: - Public properties
    /// UnitsConverter is used to convert coordinates/angles/distances between some building coordinate system and screen coordinate system.
    /// Should be provided right after the IndoorMapView initialization
    @objc public var unitsConverter: UnitsConverter? {
        didSet {
            unitsConverter?.visibleBounds = self.interactiveContainer.convertContent(self.interactiveContainer.frame, from: self)
            unitsConverter?.containerContentOffset = interactiveContainer.contentAspectOffset
            unitsConverter?.containerContentScale = interactiveContainer.contentAspectScale
            unitsConverter?.containerScale = interactiveContainer.currentContentTransform.scale
            unitsConverter?.containerRotation = interactiveContainer.currentContentTransform.rotation
        }
    }

    /// If true, the IndoorMapView will automatically rotate maps to better fit the screen bounds
    /// (e.g. map with 2:1 (w:h) ratio will be rotated to fit portrait mode screen
    /// Enabled by default.
    public var shouldApplyInitialRotationUpToScreenRatio = true

    /// Enable/Disable zoom-in action on double tap. Enabled by default
    public var shouldZoomOnDoubleTap: Bool {
        get {
            self.interactiveContainer.shouldZoomOnDoubleTap
        }
        set {
            self.interactiveContainer.shouldZoomOnDoubleTap = newValue
        }
    }

    /// Enable/Disable user gestures handling to transform the content
    public var isMapTransformationEnabled: Bool {
        get {
            self.interactiveContainer.isContentTransformationEnabled
        }
        set {
            self.interactiveContainer.isContentTransformationEnabled = newValue
        }
    }

    /// Map image alhpa value
    public var mapImageAlpha: Float {
        get {
            mapImageLayer.opacity
        }
        set {
            mapImageLayer.opacity = newValue
        }
    }

    /// Map image background color
    public var mapImageBackgroundColor: UIColor? = nil {
        didSet {
            if let image = self.currentMapImage, let color = self.mapImageBackgroundColor {
                self.mapImageBackgroundLayer.contents = color.image(of: image.size).cgImage
            } else {
                self.mapImageBackgroundLayer.contents = nil
            }
        }
    }

    /// Width(meters) of the visible part of the map when default focus is required
    public var defaultZoomMeters: CGFloat = 15

    /// Minimal width (meters) of the visible part of the map allowed user can reach
    /// using pinch gesture
    public var maxGestureZoomMeters: CGFloat = 8 {
        didSet {
            if let unitsConverter = unitsConverter {
                interactiveContainer.maxZoomScale = unitsConverter.convertFocusMetersToZoomScale(maxGestureZoomMeters)
            }
        }
    }

    /// Minimal width (meters) of the visible part of the map allowed user can reach
    /// using pinch gesture
    public var minGestureZoomMeters: CGFloat? {
        didSet {
            if let unitsConverter = unitsConverter,
               let value = minGestureZoomMeters {
                interactiveContainer.minZoomScale = unitsConverter.convertFocusMetersToZoomScale(value)
            } else {
                interactiveContainer.minZoomScale = 0.5
            }
        }
    }

    /// Allows to handle user interactions with the map
    @available(*, deprecated, message: "Now it is better to use observable map values.")
    public weak var delegate: IndoorMapViewDelegate?

    /// Width(meters) of the visible part of the map
    public var currentFocusZoomMeters: CGFloat? {
        unitsConverter?.convertZoomScaleToFocusMeters(interactiveContainer.currentContentTransform.scale)
    }

    /// Coordinates of the point of the map which is currently in the center of the screen
    public var currentFocusCoordinates: CGPoint? {
        unitsConverter?.convertPointOnScreenToCoordinate(interactiveContainer.currentFocus)
    }

    /// Current map rotation expressed as heading
    public var currentHeading: CGFloat? {
        unitsConverter?.convertMapRotationToHeadingAngle(interactiveContainer.currentContentTransform.rotation)
    }

    /// MapView bounds
    /// *Resizing of the IndoorMapView invalidates all the coordinates and will trigger all the map objects to re-evaluate they position
    public override var bounds: CGRect {
        didSet {
            unitsConverter?.visibleBounds = self.interactiveContainer.convertContent(self.interactiveContainer.frame, from: self)
        }
    }
    
    // MARK: - Observable properties
    /// Observable value for coordinate that was tapped
    public var onCoordinateTap: AnyPublisher<CGPoint, Never> {
        _onCoordinateTap.eraseToAnyPublisher()
    }
    
    private var _onCoordinateTap = PassthroughSubject<CGPoint, Never>()
    
    /// Observable map value for every transformation change
    public var onMapTransformation: AnyPublisher<Void, Never> {
        _onMapTransformation.eraseToAnyPublisher()
    }
    
    private let _onMapTransformation = PassthroughSubject<Void, Never>()
    
    /// User started interacting with the map
    public var userInteractionStarted: AnyPublisher<Void, Never> {
        _userInteractionStarted.eraseToAnyPublisher()
    }
    
    private let _userInteractionStarted = PassthroughSubject<Void, Never>()

    /// User stopped interacting with the map
    public var userInteractionEnded: AnyPublisher<Void, Never> {
        _userInteractionEnded.eraseToAnyPublisher()
    }
    
    private let _userInteractionEnded = PassthroughSubject<Void, Never>()
    
    /// User long pressed the map
    /// - Parameters:
    ///   - coordinate: Location of the gesture
    ///   - state: State of the gesture
    public var onCoordinateLongpressed: AnyPublisher<(coordinate: CGPoint, state: UIGestureRecognizer.State), Never> {
        _onCoordinateLongpressed.eraseToAnyPublisher()
    }
    
    private let _onCoordinateLongpressed = PassthroughSubject<(coordinate: CGPoint, state: UIGestureRecognizer.State), Never>()

    /// User double tapped the map
    /// - Parameters:
    ///   - coordinate: Location of the gesture
    public var onCoordinateDoubleTapped: AnyPublisher<CGPoint, Never> {
        _onCoordinateDoubleTapped.eraseToAnyPublisher()
    }
    
    private let _onCoordinateDoubleTapped = PassthroughSubject<CGPoint, Never>()
    
    /// User dragged the map
    /// - Parameters:
    ///   - coordinate: Location of the gesture
    ///   - translation: Translation since the last method call
    ///   - state: State of the gesture
    public var onContentDragged: AnyPublisher<(coordinate: CGPoint, translation: CGPoint, state: UIGestureRecognizer.State), Never> {
        _onContentDragged.eraseToAnyPublisher()
    }
    
    private let _onContentDragged = PassthroughSubject<(coordinate: CGPoint, translation: CGPoint, state: UIGestureRecognizer.State), Never>()
    
    /// User pinched the map
    /// - Parameters:
    ///   - scaleDelta: Scale since the last call
    ///   - coordinate: Location of the point between the fingers
    ///   - state: State of the gesture
    public var onContentPinched: AnyPublisher<(scaleDelta: CGFloat, coordinate: CGPoint, state: UIGestureRecognizer.State), Never> {
        _onContentPinched.eraseToAnyPublisher()
    }
    
    public let _onContentPinched = PassthroughSubject<(scaleDelta: CGFloat, coordinate: CGPoint, state: UIGestureRecognizer.State), Never>()

    /// User rotated the map
    /// - Parameters:
    ///   - angleDelta: Rotation (degress) since the last call
    ///   - coordinate: Location of the point between the fingers
    ///   - state: State of the gesture
    public var onContentRotated: AnyPublisher<(angleDelta: CGFloat, coordinate: CGPoint, state: UIGestureRecognizer.State), Never> {
        self._onContentRotated.eraseToAnyPublisher()
    }
    
    private let _onContentRotated = PassthroughSubject<(angleDelta: CGFloat, coordinate: CGPoint, state: UIGestureRecognizer.State), Never>()


    // MARK: - Public methods
    @objc public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    @objc public required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }


    /// Focus map on a specific point
    /// - Parameters:
    ///   - coordinate: Coordinates of the focus point
    ///   - offset: Value in points to offset a focused coordinate. If `offset > 0` focus point will be higher on the screen. If `offset < 0` focus point will be lower on the screen
    ///   - zoomMeters: Width(meters) of the map to be visible. Default: defaultZoomMeters
    ///   - headingAngle: Heading angle to apply. Ignored if `isRotationEnabled == false`
    ///   - animationDuration: Duration of the animation (ignored if `animated == false`). Default is: InteractiveContainerView.defaultAnimationDuration.
    ///   - animated: true to animate the transformation, false to transform immediately.
    ///   - timingFunction: Timing function for the animation (ignored if `animated == false`). Default is: InteractiveContainerView.defaultAnimationTimingFunction.
    public func focus(
        on coordinate: CGPoint,
        offset: CGFloat? = nil,
        zoomMeters: CGFloat? = nil,
        headingAngle: CGFloat? = nil,
        animationDuration: Double? = nil,
        animated: Bool = false,
        timingFunction: AnimationTimingFunction = InteractiveContainerView.defaultAnimationTimingFunction
    ) {
        guard let unitsConverter = unitsConverter else {
            return
        }
        let zoomMeters = zoomMeters ?? defaultZoomMeters
        var rotation = interactiveContainer.currentContentTransform.rotation
        if  isRotationEnabled,
            let headingAngle = headingAngle {
            rotation = unitsConverter.convertHeadingAngleToMapRotation(headingAngle)
        }
        
        var focusCoordinate = coordinate
        
        if let offset = offset {
            focusCoordinate = coordinatesForFocus(
                originalCoordinates: coordinate,
                offset: offset,
                targetZoomMeters: zoomMeters,
                targetHeading: headingAngle ?? self.currentHeading ?? 0
            )
        }
        
        interactiveContainer.focus(
            on: unitsConverter.convertCoordinateToPointOnScreen(focusCoordinate),
            zoomScale: unitsConverter.convertFocusMetersToZoomScale(zoomMeters),
            rotation: rotation,
            duration: animationDuration,
            animated: animated,
            timingFunction: timingFunction,
            offset: offset
        )
    }

    /// Adjusts the focus of the map so all the provided coordinates will be visible on screen.
    /// Will not zoom closer than maxGestureZoomMeters.
    /// Keeps current map rotation.
    /// Will have no effect if coordinates.count < 2.
    /// - Parameters:
    ///   - coordinates: Coordinates to show on screen, defined in meters;
    ///   - marginPoints: Margin from the sides of the screen, defined in points;
    ///   - animated: true to animate the transformation, false to transform immediately.
    ///   - animationDuration: Duration of the animation (ignored if `animated == false`). Default is: InteractiveContainerView.defaultAnimationDuration.
    ///   - timingFunction: Timing function for the animation (ignored if `animated == false`). Default is: InteractiveContainerView.defaultAnimationTimingFunction.
    public func show(
        coordinates: [CGPoint],
        marginPoints: CGFloat,
        animated: Bool,
        animationDuration: Double? = nil,
        timingFunction: AnimationTimingFunction = InteractiveContainerView.defaultAnimationTimingFunction
    ) {
        guard let unitsConverter = unitsConverter,
              coordinates.count > 1 else {
            return
        }

        let contentTransform = self.interactiveContainer.currentContentTransform
        let converted = coordinates.map { unitsConverter.convertCoordinateToPointOnScreen($0).applying(contentTransform) }

        let maxX = converted.max(by: { $0.x < $1.x })?.x ?? converted[0].x
        let minX = converted.min(by: { $0.x < $1.x })?.x ?? converted[0].x
        let maxY = converted.max(by: { $0.y < $1.y })?.y ?? converted[0].y
        let minY = converted.min(by: { $0.y < $1.y })?.y ?? converted[0].y
        let width = abs(maxX - minX)
        let height = abs(maxY - minY)
        let targetWidth = self.bounds.size.width - 2*marginPoints
        let targetHeight = self.bounds.size.height - 2*marginPoints

        let center = CGPoint(
            x: minX + width/2,
            y: minY + height/2
        ).applying(contentTransform.inverted())

        var targetScale = CGFloat.greatestFiniteMagnitude

        if width < .ulpOfOne {
            if height < .ulpOfOne {
                // both sides are nearly zero, so all the points are basically in the same place
            } else {
                targetScale = contentTransform.scale * targetHeight / height
            }
        } else {
            if height < .ulpOfOne {
                targetScale = contentTransform.scale * targetWidth / width
            } else {
                targetScale = min(
                    contentTransform.scale * targetWidth / width,
                    contentTransform.scale * targetHeight / height
                )
            }
        }

        let targetZoomMeters = max(self.maxGestureZoomMeters, unitsConverter.convertZoomScaleToFocusMeters(targetScale))

        self.focus(
            on: unitsConverter.convertPointOnScreenToCoordinate(center),
            zoomMeters: targetZoomMeters,
            headingAngle: nil,
            animationDuration: animationDuration,
            animated: animated,
            timingFunction: timingFunction
        )
    }

    /// Allow user to rotate the map.
    /// In case IndoorMapView.shouldApplyInitialRotationUpToScreenRatio == true
    @objc public var isRotationEnabled: Bool = true {
        didSet {
            if oldValue != isRotationEnabled {
                self.interactiveContainer.isRotationEnabled = isRotationEnabled
            }
        }
    }

    /// Sets image representation of the map
    /// Changing of the image will invalidate al the map objects coordinates
    /// and will move the map to default focus.
    /// To keep the focus while switching between floors save currentFocusZoomMeters, currentFocusCoordinates and currentHeading
    /// before setting the new map image, reset your units converter and apply saved values using focus function
    /// - Parameter image: Image representation of the map
    @objc public func setMapImage(_ image: UIImage) {
        self.currentMapImage = image

        if shouldShowCoordinatesGridLegend {
            self.hideCoordinatesGridLegend()
            self.showCoordinatesGridLegend()
        }

        mapImageBackgroundLayer.contents = self.mapImageBackgroundColor?.image(of: image.size).cgImage
        mapImageLayer.contents = image.cgImage
        interactiveContainer.contentSize = image.size.screenPoints
        if let unitsConverter = unitsConverter {
            interactiveContainer.maxZoomScale = unitsConverter.convertFocusMetersToZoomScale(maxGestureZoomMeters)
        }

        // we rotate and scale landscape maps so they will fit the screen
        if shouldApplyInitialRotationUpToScreenRatio && image.size.width > image.size.height && self.bounds.size.width < self.bounds.size.height {
            interactiveContainer.defaultContentRotation = CGFloat(Double.pi / 2)
            if interactiveContainer.contentSize.height != 0 {
                interactiveContainer.defaultContentScale = interactiveContainer.contentSize.width/interactiveContainer.contentSize.height
            }
        } else {
            interactiveContainer.defaultContentRotation = 0
            interactiveContainer.defaultContentScale = 1
        }

        interactiveContainer.applyDefaultFocus()
    }

    /// Clears map image representation (Does not remove map objects)
    @objc public func clearMapImage() {
        self.currentMapImage = nil
        self.mapImageBackgroundLayer.contents = nil
        self.mapImageLayer.contents = nil
        let shouldShow = shouldShowCoordinatesGridLegend
        self.hideCoordinatesGridLegend()
        self.shouldShowCoordinatesGridLegend = shouldShow
    }

    /// Returns the item whose ID matches the specified value.
    /// - Parameter id: ID to search for
    @objc public func contentItem(_ id: MapObjectID) -> ContentItem? { contentItems[id]?.item }

    @objc public func addContentItem(_ item: ContentItem, at level: UInt) {
        if let current = contentItems[item.id] {
            if current.level == level {
                return
            }

            removeContentItem(with: item.id)
        }

        item.setContainerScale(interactiveContainer.currentContentTransform.scale, rotation: interactiveContainer.currentContentTransform.rotation)
        getContentLayer(level).addSublayer(item)
        contentItems[item.id] = (item: item, level: level)
    }

    /// Removes content item from the map.
    /// - Parameter item: Content item to remove
    @objc public func removeContentItem(_ item: ContentItem) {
        removeContentItem(with: item.id)
    }

    /// Removes content item from the map using its ID.
    /// - Parameter item: Content item ID to search for
    @objc public func removeContentItem(with id: MapObjectID) {
        if let current = contentItems.removeValue(forKey: id) {
            current.item.removeFromSuperlayer()

            let layer = getContentLayer(current.level)
            if (layer.sublayers ?? []).isEmpty {
                layer.removeFromSuperlayer()
                _ = contentLayers.removeValue(forKey: current.level)
            }
        }
    }

    /// Remove all the content items on a specific level
    /// - Parameter level: Level to clean up
    @objc public func removeContentItems(at level: UInt) {
        guard let layer = contentLayers.removeValue(forKey: level) else {
            return
        }

        layer.removeFromSuperlayer()

        contentItems = contentItems.filter { (_, item) -> Bool in
            item.level != level
        }
    }

    /// Remove all the content items from the map
    @objc public func removeAllContentItems() {
        contentLayers.forEach { (_, layer) in
            layer.removeFromSuperlayer()
        }

        contentLayers = [:]
        contentItems = [:]
    }

    /// Returns all the content items for which `MapObject.isSelectable` and `MapObject.isPointInside(_:)` are true.
    /// - Parameter coordinates: coordinates of the point to check
    /// - Returns: Items grouped by levels
    @objc public func items(at coordinates: CGPoint) -> [UInt: [ContentItem]] {
        guard let point = unitsConverter?.convertCoordinateToPointOnScreen(coordinates) else {
            return [:]
        }

        // isSelectable == true goes last
        // higher levels go before lower
        // higher zPosition go before lower
        let sorted = contentItems.sorted { (item1, item2) -> Bool in
            if !item1.value.item.isSelectable {
                return false
            }

            if !item2.value.item.isSelectable {
                return true
            }

            if item1.value.level != item2.value.level {
                return item1.value.level > item2.value.level
            }

            return item1.value.item.zPosition > item2.value.item.zPosition
        }

        var tappedItems = [UInt: [ContentItem]]()

        for item in sorted {
            // we reached the non-selectable items and can stop
            if !item.value.item.isSelectable {
                return tappedItems
            }

            if item.value.item.isPointInside(point) {
                var itemsPerLevel = tappedItems[item.value.level] ?? []
                itemsPerLevel.append(item.value.item)
                tappedItems[item.value.level] = itemsPerLevel
            }
        }

        return tappedItems
    }

    /// Creates a represenation of a visible area of the map
    /// - Parameters:
    ///   - center: coordinates of the center of the area
    ///   - size: size of the area (meters)
    /// - Returns: image of the area
    public func getSnapshot(center: CGPoint, size: CGSize, afterScreenUpdates: Bool) -> UIView? {
        guard let unitsConverter = unitsConverter else {
            return nil
        }

        let center = unitsConverter.convertCoordinateToPointOnScreen(center)
        let size = CGSize(
            width: unitsConverter.convertMetersToPoints(size.width),
            height: unitsConverter.convertMetersToPoints(size.height)
        )

        return interactiveContainer.getContentSnapshot(center: center, size: size, afterScreenUpdates: afterScreenUpdates)
    }

    /// Delay triggering tap gesture until double tap gesture is failed
    /// Useful if tap and double tap used for the same object/area
    public var tapShouldRequireDoubleTapToFail: Bool {
        set {
            interactiveContainer.tapShouldRequireDoubleTapToFail = newValue
        }
        get {
            interactiveContainer.tapShouldRequireDoubleTapToFail
        }
    }

    // MARK: - Coordinates Grid Legend
    public private(set) var coordinatesGridLegendView: CoordinatesGridLegendView?

    public var coordinatesGridLegendWidth: CGFloat? {
        didSet {
            if let value = coordinatesGridLegendWidth {
                coordinatesGridLegendView?.legendWidth = value
            }
        }
    }
    public var coordinatesGridLegendHeight: CGFloat? {
        didSet {
            if let value = coordinatesGridLegendHeight {
                coordinatesGridLegendView?.legendHeight = value
            }
        }
    }

    /// Shows the coordinates for the coordinates grid map object. Does not support map rotation.
    public func showCoordinatesGridLegend() {
        if coordinatesGridLegendView != nil {
            return
        }

        shouldShowCoordinatesGridLegend = true
        let legend = CoordinatesGridLegendView(frame: self.bounds)
        legend.frame = self.bounds
        legend.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        legend.translatesAutoresizingMaskIntoConstraints = true

        if let width = coordinatesGridLegendWidth {
            legend.legendWidth = width
        }

        if let height = coordinatesGridLegendHeight {
            legend.legendHeight = height
        }

        legend.contentCoordinatesConverter = { [weak self] point in
            self?.interactiveContainer.convertContent(point, to: self?.coordinatesGridLegendView) ?? .zero
        }
        self.addSubview(legend)
        legend.unitsConverter = self.unitsConverter
        self.coordinatesGridLegendView = legend
    }

    /// Hides the coordinates for the coordinates grid map object.
    public func hideCoordinatesGridLegend() {
        shouldShowCoordinatesGridLegend = false
        coordinatesGridLegendView?.removeFromSuperview()
        coordinatesGridLegendView = nil
    }

    // MARK: - Private properties
    private let interactiveContainer = InteractiveContainerView()
    private let mapImageLayer = CALayer()
    private let mapImageBackgroundLayer = CALayer()
    private var contentLayers: [UInt: CALayer] = [:]
    private var contentItems: [MapObjectID: (item: ContentItem, level: UInt)] = [:]
    private var shouldShowCoordinatesGridLegend = false
    private var currentMapImage: UIImage?

    // MARK: - Initial setup
    private func commonInit() {
        self.clipsToBounds = false

        interactiveContainer.clipsToBounds = false
        interactiveContainer.frame = self.bounds
        interactiveContainer.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        interactiveContainer.translatesAutoresizingMaskIntoConstraints = true
        self.addSubview(interactiveContainer)

        mapImageBackgroundLayer.zPosition = -2
        mapImageBackgroundLayer.contentsGravity = .resizeAspect
        interactiveContainer.addContentLayer(mapImageBackgroundLayer)

        mapImageLayer.zPosition = -1
        mapImageLayer.contentsGravity = .resizeAspect
        interactiveContainer.addContentLayer(mapImageLayer)
        interactiveContainer.delegate = self
    }

    // MARK: - Content Transformation Management
    private func populateNewTransformToContent(_ transform: CGAffineTransform) {
        let scale = transform.scale
        let rotation = transform.rotation

        unitsConverter?.visibleBounds = self.interactiveContainer.convertContent(self.interactiveContainer.frame, from: self)
        unitsConverter?.containerScale = scale
        unitsConverter?.containerRotation = rotation

        contentItems.forEach {
            $0.value.item.setContainerScale(scale, rotation: rotation)
        }

        coordinatesGridLegendView?.contentTransformed(scale)
    }
    
    // MARK: - Calculation of focus coordinate with offset
    private func coordinatesForFocus(
        originalCoordinates: CGPoint,
        offset: CGFloat,
        targetZoomMeters: CGFloat,
        targetHeading: CGFloat
    ) -> CGPoint {
        guard let unitsConverter = self.unitsConverter else { return originalCoordinates }
        
        let targetScale = unitsConverter.containerScale * (self.currentFocusZoomMeters ?? 1) / targetZoomMeters
        let angle = CGFloat(Float.pi) - targetHeading
        let offsetMeters: CGFloat = unitsConverter.convertPointsToMeters(offset) / targetScale
        return CGPoint(
            x: originalCoordinates.x + cos(angle) * offsetMeters,
            y: originalCoordinates.y - sin(angle) * offsetMeters
        )
    }

    // MARK: - Content Layers Management
    private func getContentLayer(_ level: UInt) -> CALayer {
        if let layer = contentLayers[level] {
            return layer
        }

        let layer = CALayer()
        layer.zPosition = CGFloat(level)
        interactiveContainer.addContentLayer(layer)
        contentLayers[level] = layer
        return layer
    }
}

// MARK: - InteractiveContainerViewDelegate
extension IndoorMapView: InteractiveContainerViewDelegate {
    public func onContentDragged(to point: CGPoint, translation: CGPoint, state: UIGestureRecognizer.State) {
        guard let converter = self.unitsConverter else {
            return
        }
        
        let translation = CGPoint(
            x: converter.convertPointsToMeters(translation.x),
            y: -converter.convertPointsToMeters(translation.y)
        )
        let coordinate = converter.convertPointOnScreenToCoordinate(point)

        delegate?.onContentDragged(
            to: coordinate,
            translation: translation,
            state: state,
            in: self
        )
        
        _onContentDragged.send((
            coordinate: coordinate,
            translation: translation,
            state: state)
        )
    }

    public func onContentPinched(scaleDelta: CGFloat, around point: CGPoint, state: UIGestureRecognizer.State) {
        guard let converter = self.unitsConverter else {
            return
        }
        
        let coordinate = converter.convertPointOnScreenToCoordinate(point)

        delegate?.onContentPinched(
            scaleDelta: scaleDelta,
            around: coordinate,
            state: state,
            in: self
        )
        
        _onContentPinched.send((
            scaleDelta: scaleDelta,
            coordinate: coordinate,
            state: state
        ))
    }

    public func onContentRotated(angleDelta: CGFloat, around point: CGPoint, state: UIGestureRecognizer.State) {
        guard let converter = self.unitsConverter else {
            return
        }
        
        let coordinate = converter.convertPointOnScreenToCoordinate(point)
        
        delegate?.onContentRotated(
            angleDelta: angleDelta,
            around: coordinate,
            state: state,
            in: self
        )
        
        _onContentRotated.send((
            angleDelta: angleDelta,
            coordinate: coordinate,
            state: state
        ))
        
    }

    public func onContentDoubleTapped(_ point: CGPoint) {
        guard let converter = self.unitsConverter else { return }
        
        let coordinate = converter.convertPointOnScreenToCoordinate(point)
         
        delegate?.onCoordinateDoubleTapped(converter.convertPointOnScreenToCoordinate(point), in: self)
        
        _onCoordinateDoubleTapped.send(coordinate)
    }

    public func onContentTapped(_ point: CGPoint) {
        guard let converter = self.unitsConverter else { return }
        
        let coordinate = converter.convertPointOnScreenToCoordinate(point)
        
        delegate?.onCoordinateTapped(coordinate, in: self)
        _onCoordinateTap.send(coordinate)
    }

    public func onContentLongpressed(_ point: CGPoint, state: UIGestureRecognizer.State) {
        guard let converter = self.unitsConverter else { return }

        let coordinate = converter.convertPointOnScreenToCoordinate(point)
        
        delegate?.onCoordinateLongpressed(converter.convertPointOnScreenToCoordinate(point), state: state, in: self)
        _onCoordinateLongpressed.send((
            coordinate: coordinate,
            state: state
        ))
    }

    public func onUserInteractionStarted() {
        delegate?.onUserInteractionStarted(in: self)
        _userInteractionStarted.send(())
    }

    public func onUserInteractionEnded() {
        delegate?.onUserInteractionEnded(in: self)
        _userInteractionEnded.send(())
    }

    public func onContentTransformed(_ newTransform: CGAffineTransform) {
        self.populateNewTransformToContent(newTransform)
    
        delegate?.onMapTransformationUpdated(in: self)
        _onMapTransformation.send()
    }

    public func onContentParametersChanged() {
        unitsConverter?.containerContentOffset = interactiveContainer.contentAspectOffset
        unitsConverter?.containerContentScale = interactiveContainer.contentAspectScale
        unitsConverter?.visibleBounds = self.interactiveContainer.convertContent(self.interactiveContainer.frame, from: self)

        contentItems.forEach {
            $0.value.item.unitsConverterUpdated()
        }

        coordinatesGridLegendView?.unitsConverter = unitsConverter
    }
}
