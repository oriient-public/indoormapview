//
//  Polygon.swift
//  IPSMapView
//
//  Created by Michael Krutoyarskiy on 12/04/2020.
//  Copyright © 2020 Michael Krutoyarskiy. All rights reserved.
//

import Foundation
import UIKit

/// A layer to represent polygons on the map
/// See `PolygonVisualConfig` for possible visual configurations
open class PolygonLayer: CAShapeLayer, ZoomSensitiveLayer {
    public let id: String
    public var isSelectable: Bool = false
    public var config: PolygonVisualConfig {
        didSet {
            applyConfig()
        }
    }
    public var vertices: [CGPoint] {
        didSet {
            resetPath()
        }
    }

    public var unitsConverter: UnitsConverter

    // MARK: Zoom management properties
    var currentContainerScale: CGFloat = 1
    public var hidesWhenOutsideOfAcceptableZoom: Bool = false {
        didSet {
            evaluateCurrentScale()
        }
    }
    public var zoomSensitiveLayer: CALayer { self }
    public var minZoom: CGFloat = 0
    public var maxZoom: CGFloat = .greatestFiniteMagnitude

    public init(id: String, vertices: [CGPoint], config: PolygonVisualConfig, unitsConverter: UnitsConverter) {
        self.id = id
        self.config = config
        self.unitsConverter = unitsConverter
        self.vertices = vertices
        super.init()

        self.masksToBounds = false
        self.frame = .zero

        self.lineCap = CAShapeLayerLineCap.round
        self.lineJoin = CAShapeLayerLineJoin.round

        applyConfig()
        resetPath()
        applyScale()
    }

    private func applyConfig() {
        self.fillColor = config.fillColor.cgColor
        self.strokeColor = config.strokeColor?.cgColor
        self.lineWidth = config.strokeWidth/currentContainerScale
    }

    internal func resetPath() {
        if vertices.count > 2 {
            let path = UIBezierPath()
            path.move(to: unitsConverter.convertCoordinateToPointOnScreen(vertices[0]))
            for i in 1..<vertices.count {
                path.addLine(to: unitsConverter.convertCoordinateToPointOnScreen(vertices[i]))
            }
            path.close()
            self.path = path.cgPath
        }
    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(layer: Any) {
       let layer = layer as! PolygonLayer
       self.id = layer.id
       self.config = layer.config
       self.unitsConverter = layer.unitsConverter
       self.vertices = layer.vertices
       super.init(layer: layer)
    }

    func applyScale() {
        if config.strokeWidth > 0 {
            self.lineWidth = config.strokeWidth/currentContainerScale
        }
        evaluateCurrentScale()
    }
}

@objc public class PolygonVisualConfig: NSObject {
    let fillColor: UIColor
    let strokeColor: UIColor?
    let strokeWidth: CGFloat

    public init(fillColor: UIColor, strokeColor: UIColor?, strokeWidth: CGFloat?) {
        self.fillColor = fillColor
        self.strokeColor = strokeColor
        self.strokeWidth = strokeWidth ?? 0
        super.init()
    }
}

extension PolygonLayer: MapObject {
    public func setContainerScale(_ scale: CGFloat, rotation: CGFloat) {
        if scale == self.currentContainerScale {
            return
        }
        self.currentContainerScale = scale
        applyScale()
    }

    public func isPointInside(_ point: CGPoint) -> Bool {
        if isHidden {
            return false
        }

        return self.path?.contains(point) ?? false
    }

    public func unitsConverterUpdated() {
        resetPath()
    }
}



