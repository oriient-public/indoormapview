//
//  Image.swift
//  IPSMapView
//
//  Created by Michael Krutoyarskiy on 12/04/2020.
//  Copyright © 2020 Michael Krutoyarskiy. All rights reserved.
//

import Foundation
import UIKit

/// Image provider used in case the UIImage object for the
/// `ImageLayer` can't be provided on initialization
/// for example if the image has to be downloaded first
@objc public protocol ImageProvider: AnyObject {
    func getImage(_ completion: @escaping (UIImage?) -> Void)
}

/// A layer to place images on the map
/// Can be initialized with an image or `ImageProvider` + optional placeholder
/// See `ImageVisualConfig` for the visual configurations
/// Note: `ImageVisualConfig.anchorPoint` defines which point of the image will correspond to the `coordinates` provided during initialization
open class ImageLayer: CALayer, ZoomSensitiveLayer {

    public let id: String
    public var isSelectable: Bool = false
    public var unitsConverter: UnitsConverter
    
    internal var currentContainerScale: CGFloat = 1
    private var currentContainerRotation: CGFloat = 0
    private let config: ImageVisualConfig
    private let imageProvider: ImageProvider?
    public var coordinates: CGPoint {
        didSet {
            unitsConverterUpdated()
        }
    }
    public var zoomSensitiveLayer: CALayer { self }
    public var minZoom: CGFloat = 0
    public var maxZoom: CGFloat = .greatestFiniteMagnitude

    public var hidesWhenOutsideOfAcceptableZoom: Bool = false {
        didSet {
            evaluateCurrentScale()
        }
    }

    public init(
        id: String,
        coordinates: CGPoint,
        image: UIImage?,
        config: ImageVisualConfig,
        unitsConverter: UnitsConverter
    ) {
        self.id = id
        self.unitsConverter = unitsConverter
        self.config = config
        self.imageProvider = nil
        self.coordinates = coordinates
        super.init()
        commonInit()
        self.contents = image?.cgImage
    }

    public init(
        id: String,
        coordinates: CGPoint,
        placeholderImage: UIImage?,
        imageProvider: ImageProvider,
        config: ImageVisualConfig,
        unitsConverter: UnitsConverter
    ) {
        self.id = id
        self.unitsConverter = unitsConverter
        self.config = config
        self.imageProvider = imageProvider
        self.coordinates = coordinates
        super.init()
        commonInit()
        self.contents = placeholderImage?.cgImage
        imageProvider.getImage { [weak self] image in
            if let image = image {
                self?.contents = image.cgImage
                self?.setNeedsLayout()
            }
        }
    }

    private func commonInit() {
        self.contentsGravity = config.contentsGravity
        self.anchorPoint = config.anchorPoint
        self.masksToBounds = true
        self.frame = CGRect(x: 0, y: 0, width: config.size.width, height: config.size.height)
        self.position = unitsConverter.convertCoordinateToPointOnScreen(coordinates)
        self.contentsScale = UIScreen.main.scale
        applyScaleAndRotation()
    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(layer: Any) {
       let layer = layer as! ImageLayer
       self.id = layer.id
       self.config = layer.config
       self.unitsConverter = layer.unitsConverter
       self.imageProvider = layer.imageProvider
        self.coordinates = layer.coordinates
       super.init(layer: layer)
       self.contents = layer.contents
    }

    private func applyScaleAndRotation() {
        var transfrom = CATransform3DIdentity

        evaluateCurrentScale()

        if !config.shouldRotateWithContent {
            transfrom = CATransform3DMakeRotation(CGFloat(2 * Double.pi) - self.currentContainerRotation, 0, 0, 1)
        }

        if !config.shouldScaleWithContent {
            let scale = 1/currentContainerScale
            transfrom = CATransform3DScale(transfrom, scale, scale, 1)
        }

        self.transform = transfrom
    }
}

/// Visual configurations for the image layer
@objc public class ImageVisualConfig: NSObject {

    /// If true the image will be scaled the same way the user scales the map,
    /// if false the image will always be the same size on the screen
    let shouldScaleWithContent: Bool

    /// If true the image will be rotated the same way user scales the map,
    /// if false the image will always be vertical on the screen
    let shouldRotateWithContent: Bool

    /// defines which point of the image will correspond to the `coordinates` provided during initialization
    let anchorPoint: CGPoint

    /// Additional area for `isPointInside`
    let clickableAreaExtension: CGSize

    /// See `CALayerContentsGravity` for the explanation
    let contentsGravity: CALayerContentsGravity

    /// Required size of the image in points
    let size: CGSize

    public init(
        size: CGSize,
        shouldScaleWithContent: Bool,
        shouldRotateWithContent: Bool,
        anchorPoint: CGPoint = CGPoint(x: 0.5, y: 0.5),
        clickableAreaExtension: CGSize = .zero,
        contentsGravity: CALayerContentsGravity = .resizeAspect
    ) {
        self.size = size
        self.shouldScaleWithContent = shouldScaleWithContent
        self.shouldRotateWithContent = shouldRotateWithContent
        self.anchorPoint = anchorPoint
        self.clickableAreaExtension = clickableAreaExtension
        self.contentsGravity = contentsGravity
        super.init()
    }
}

extension ImageLayer: MapObject {
    public func unitsConverterUpdated() {
        self.position = unitsConverter.convertCoordinateToPointOnScreen(coordinates)
    }

    public func setContainerScale(_ scale: CGFloat, rotation: CGFloat) {
        self.currentContainerScale = scale
        self.currentContainerRotation = rotation
        applyScaleAndRotation()
    }

    public func isPointInside(_ point: CGPoint) -> Bool {
        if isHidden {
            return false
        }
        return self.frame.insetBy(
            dx: -config.clickableAreaExtension.width,
            dy: -config.clickableAreaExtension.height
        ).contains(point)
    }
}
