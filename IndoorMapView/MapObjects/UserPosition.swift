//
//  UserPosition.swift
//  IPSMapView
//
//  Created by Michael Krutoyarskiy on 01/04/2020.
//  Copyright © 2020 Michael Krutoyarskiy. All rights reserved.
//

import Foundation
import UIKit

open class UserPositionLayer: CALayer {
    // MARK: - Properties
    private enum AccuracyUnit {
        case meters(value: CGFloat)
        case points(value: CGFloat)
    }

    private var positionLayer: CAShapeLayer!
    private var headingLayer: CAShapeLayer!
    private var accuracyLayer: CAShapeLayer!

    public static var positionAnimationDuration: Double = 0.2
    public static var headingAnimationDuration: Double = 0.2
    public static var accuracyAnimationDuration: Double = 0.5

    public static var positionAnimationFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
    public static var headingAnimationFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
    public static var accuracyAnimationFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)

    private let unitsConverter: UnitsConverter
    private var currentContainerScale: CGFloat = 1
    private var currentHeadingAngle: CGFloat = 0
    private var currentAccuracyValue: AccuracyUnit = .meters(value: 0)
    private var currentCoordinates: CGPoint?
    private var positionVisualConfig: PositionVisualConfig
    public let id: String

    // MARK: - Public
    @objc public func setAccuracyHidden(_ hidden: Bool, animated: Bool) {
        if animated && self.accuracyLayer.isHidden != hidden {
            self.accuracyLayer.addFadeAnimation()
        }
        self.accuracyLayer.isHidden = hidden
    }

    @objc public func setHeadingHidden(_ hidden: Bool, animated: Bool) {
        if animated && self.headingLayer.isHidden != hidden{
            self.headingLayer.addFadeAnimation()
        }
        self.headingLayer.isHidden = hidden
    }

    @objc public func setPositionHidden(_ hidden: Bool, animated: Bool) {
        if animated && self.positionLayer.isHidden != hidden{
            self.positionLayer.addFadeAnimation()
        }

        /// hiding without opacity change may not affect the stroke
        self.positionLayer.opacity = hidden ? 0 : 1
        self.positionLayer.isHidden = hidden
    }

    @objc public func setUserPositionColor(_ color: UIColor, animated: Bool) {
        positionLayer.fillColor = color.cgColor
        if animated {
             self.positionLayer.addFadeAnimation()
        }
    }

    @objc public func setUserPositionStrokeColor(_ color: UIColor, animated: Bool) {
        positionLayer.strokeColor = color.cgColor
        if animated {
             self.positionLayer.addFadeAnimation()
        }
    }

    @objc public func setPositionConfig(_ config: PositionVisualConfig, animated: Bool) {
        applyPositionConfig(config, to: positionLayer)
        self.positionVisualConfig = config
        if animated {
             self.positionLayer.addFadeAnimation()
        }
    }

    @objc public func setHeadingConfig(_ config: HeadingVisualConfig, animated: Bool) {
        applyHeadingConfig(config, to: headingLayer)
        if animated {
             self.headingLayer.addFadeAnimation()
        }
    }

    @objc public func setAccuracyConfig(_ config: AccuracyVisualConfig, animated: Bool) {
        applyAccuracyConfig(config, to: accuracyLayer)
        if animated {
             self.accuracyLayer.addFadeAnimation()
        }
    }

    @objc public func setAccuracyRadiusMeters(_ value: CGFloat, animated: Bool) {
        setAccuracyRadius(AccuracyUnit.meters(value: value), animated: animated)
    }

    @objc public func setAccuracyRadiusPoints(_ value: CGFloat, animated: Bool) {
        setAccuracyRadius(AccuracyUnit.points(value: value), animated: animated)
    }

    // radians angle in a building's coordinate system
    @objc public func setHeadingAngle(_ angle: CGFloat, animated: Bool) {
        self.currentHeadingAngle = angle
        self.applyCurrentHeading(animated: animated)
    }

    @objc public func move(to coordinates: CGPoint, animated: Bool) {
        self.currentCoordinates = coordinates
        let newPosition = self.unitsConverter.convertCoordinateToPointOnScreen(coordinates)
        if animated {
            CALayer.wrapInCAAnimationTransaction({
                self.position = newPosition
            }, duration: Self.positionAnimationDuration, mediaFunction: Self.positionAnimationFunction)
        } else {
            CALayer.disableImplicitAnimations {
                self.position = newPosition
            }
        }
    }

    // MARK: - Transforms
    private func adjustPositionTransform() {
        let scale = 1.0/currentContainerScale
        positionLayer.transform = CATransform3DMakeScale(scale, scale, scale)
    }

    private func applyCurrentHeading(animated: Bool) {
        let scale = 1/currentContainerScale
        var transform = CATransform3DMakeScale(scale, scale, scale)
        transform = CATransform3DRotate(transform, unitsConverter.convertHeadingAngleToUserRotation(currentHeadingAngle), 0, 0, 1)

        if animated {
            CALayer.wrapInCAAnimationTransaction({
                self.headingLayer.transform = transform
            }, duration: Self.headingAnimationDuration, mediaFunction: Self.headingAnimationFunction)
        }else {
            CALayer.disableImplicitAnimations {
                self.headingLayer.transform = transform
            }
        }
    }

    private func applyCurrentAccuracy(animated: Bool) {
        var radius: CGFloat = 0
        switch currentAccuracyValue {
        case .points(let value):
            radius = value/currentContainerScale
        case .meters(let value):
            radius = unitsConverter.convertMetersToPoints(value)
        }

        accuracyLayer.removeAnimation(forKey: "scale")

        if animated {
            CALayer.wrapInCAAnimationTransaction({
                self.accuracyLayer.transform = CATransform3DMakeScale(radius, radius, radius)
            }, duration: Self.accuracyAnimationDuration, mediaFunction: Self.accuracyAnimationFunction)
        } else {
            CALayer.disableImplicitAnimations {
                self.accuracyLayer.transform = CATransform3DMakeScale(radius, radius, radius)
            }
        }
    }

    private func setAccuracyRadius(_ value: AccuracyUnit, animated: Bool) {
        self.currentAccuracyValue = value
        self.applyCurrentAccuracy(animated: animated)
    }

    // MARK: - Initialization
    public init(
        id: String = "me.oriient.userPosition",
        headingConfig: HeadingVisualConfig,
        accuracyConfig: AccuracyVisualConfig,
        positionConfig: PositionVisualConfig,
        unitsConverter: UnitsConverter) {
        self.unitsConverter = unitsConverter
        self.positionVisualConfig = positionConfig
        self.id = id
        super.init()
        self.masksToBounds = false
        self.frame = .zero
        self.headingLayer = setupHeadingLayer(headingConfig)
        self.accuracyLayer = setupAccuracyLayer(accuracyConfig)
        self.positionLayer = setupPositionLayer(positionConfig)
    }

    override init(layer: Any) {
        let layer = layer as! UserPositionLayer

        self.positionLayer = CAShapeLayer(layer: layer.positionLayer!)
        self.headingLayer = CAShapeLayer(layer: layer.headingLayer!)
        self.accuracyLayer = CAShapeLayer(layer: layer.accuracyLayer!)
        self.unitsConverter = layer.unitsConverter
        self.positionVisualConfig = layer.positionVisualConfig
        self.id = layer.id

        super.init(layer: layer)
    }

    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // Should not be called directily, used to customize the layer in subclasses
    open func setupHeadingLayer(_ config: HeadingVisualConfig) -> CAShapeLayer {
        let headingLayer = CAShapeLayer()
        self.addSublayer(headingLayer)
        headingLayer.zPosition = 2
        headingLayer.backgroundColor = nil
        headingLayer.isHidden = true
        applyHeadingConfig(config, to: headingLayer)
        return headingLayer
    }

    // Should not be called directily, used to customize the layer in subclasses
    open func applyHeadingConfig(_ config: HeadingVisualConfig, to headingLayer: CAShapeLayer) {
        headingLayer.fillColor = config.color.cgColor
        let startValue = -config.openningAngle / 2
        let endValue = config.openningAngle / 2
        let headingPath = UIBezierPath(arcCenter: CGPoint(x: 0, y: 0), radius: config.radius, startAngle: startValue, endAngle: endValue, clockwise: true)
        headingPath.addLine(to: CGPoint(x: 0, y: 0))
        headingPath.close()
        headingLayer.path = headingPath.cgPath
    }

    // Should not be called directily, used to customize the layer in subclasses
    open func setupPositionLayer(_ config: PositionVisualConfig) -> CAShapeLayer {
       let positionLayer = CAShapeLayer()
       self.addSublayer(positionLayer)
       positionLayer.zPosition = 3
       applyPositionConfig(config, to: positionLayer)
       return positionLayer
    }

    // Should not be called directily, used to customize the layer in subclasses
    open func applyPositionConfig(_ config: PositionVisualConfig, to positionLayer: CAShapeLayer) {
        positionLayer.path = UIBezierPath(arcCenter: CGPoint(x: 0, y: 0), radius: config.radius, startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true).cgPath
        positionLayer.fillColor = config.fillColor.cgColor
        positionLayer.strokeColor = config.strokeColor.cgColor
        positionLayer.lineWidth = config.strokeWidth
    }

    // Should not be called directily, used to customize the layer in subclasses
    open func setupAccuracyLayer(_ config: AccuracyVisualConfig) -> CAShapeLayer {
        let accuracyLayer = CAShapeLayer()
        self.addSublayer(accuracyLayer)
        accuracyLayer.zPosition = 1
        accuracyLayer.path = UIBezierPath(arcCenter: CGPoint(x: 0, y: 0), radius: 1.0, startAngle: CGFloat(0), endAngle: CGFloat(Double.pi * 2), clockwise: true).cgPath
        accuracyLayer.isHidden = true
        applyAccuracyConfig(config, to: accuracyLayer)
        return accuracyLayer
    }

    // Should not be called directily, used to customize the layer in subclasses
    open func applyAccuracyConfig(_ config: AccuracyVisualConfig, to accuracyLayer: CAShapeLayer) {
        accuracyLayer.fillColor = config.color.cgColor
    }
}

// MARK: - As IPSMapObject
extension UserPositionLayer: MapObject {
    public func unitsConverterUpdated() {
        if let coordinates = self.currentCoordinates {
            self.move(to: coordinates, animated: false)
        }

        self.applyCurrentAccuracy(animated: false)
        self.applyCurrentHeading(animated: false)
    }

    open var isSelectable: Bool { false }

    @objc open func isPointInside(_ point: CGPoint) -> Bool {
        if isHidden {
            return false
        }

        return self.position.distance(to: point) < 1.5*positionVisualConfig.radius
    }

    @objc public func setContainerScale(_ scale: CGFloat, rotation: CGFloat) {
        self.currentContainerScale = scale
        self.applyCurrentHeading(animated: false)
        self.adjustPositionTransform()
        self.applyCurrentAccuracy(animated: false)
    }
}

// MARK: - Visual configurations
@objc public class AccuracyVisualConfig: NSObject {
    public let color: UIColor

    public init(color: UIColor) {
        self.color = color
        super.init()
    }
}

@objc public class PositionVisualConfig: NSObject {
    public let fillColor: UIColor
    public let strokeColor: UIColor
    public let radius: CGFloat
    public let strokeWidth: CGFloat
    public let clickableRadius: CGFloat

    public init(fillColor: UIColor, strokeColor: UIColor, radius: CGFloat, strokeWidth: CGFloat, clickableRadius: CGFloat? = nil) {
        self.fillColor = fillColor
        self.strokeColor = strokeColor
        self.radius = radius
        self.strokeWidth = strokeWidth
        self.clickableRadius = clickableRadius ?? 1.5*radius
        super.init()
    }
}

@objc public class HeadingVisualConfig: NSObject {
    public let color: UIColor
    public let openningAngle: CGFloat
    public let radius: CGFloat

    public init(color: UIColor, openningAngle: CGFloat, radius: CGFloat) {
        self.color = color
        self.openningAngle = openningAngle
        self.radius = radius
        super.init()
    }
}
