//
//  ZoomSensitiveLayer.swift
//  IPSMapView
//
//  Created by Michael Krutoyarskiy on 31/05/2020.
//  Copyright © 2020 Michael Krutoyarskiy. All rights reserved.
//

import Foundation
import UIKit

/// Helper to show/hide the object depending on the zoom level
public protocol ZoomSensativeMapObject: AnyObject {
    var hidesWhenOutsideOfAcceptableZoom: Bool { get set }
    var unitsConverter: UnitsConverter { get }

    func setAcceptableZoomScale(min: CGFloat, max: CGFloat)

    func resetAcceptableZoomScale()
}

/// Provides default impelementations of `ZoomSensativeMapObject` for a layer
/// Declare your layer to be `ZoomSensitiveLayer`, implement the properties and call `evaluateCurrentScale` in the right place
public protocol ZoomSensitiveLayer: ZoomSensativeMapObject {
    var zoomSensitiveLayer: CALayer { get }
    var minZoom: CGFloat { get set }
    var maxZoom: CGFloat { get set }
}

extension ZoomSensitiveLayer {
    public func setAcceptableZoomScale(min: CGFloat, max: CGFloat) {
        self.minZoom = min
        self.maxZoom = max
        evaluateCurrentScale()
    }

    public func resetAcceptableZoomScale() {
        self.minZoom = 0
        self.maxZoom = .greatestFiniteMagnitude
        evaluateCurrentScale()
    }

    public func evaluateCurrentScale() {
        if !hidesWhenOutsideOfAcceptableZoom {
            self.zoomSensitiveLayer.setHidden(false, animated: false)
            return
        }
        
        let visibleAreaInMeters = unitsConverter.convertZoomScaleToFocusMeters(unitsConverter.containerScale)

        if visibleAreaInMeters >= minZoom {
           self.zoomSensitiveLayer.setHidden(true, animated: false)
           return
        }

        if visibleAreaInMeters <= maxZoom {
           self.zoomSensitiveLayer.setHidden(true, animated: false)
           return
        }

        self.zoomSensitiveLayer.setHidden(false, animated: false)
    }
}
