//
//  FloorTransition.swift
//  IPSMapView
//
//  Created by Michael Krutoyarskiy on 19/04/2020.
//  Copyright © 2020 Michael Krutoyarskiy. All rights reserved.
//

import Foundation
import UIKit

/// Direction of a floor transition
@objc public enum FloorTransitionDirection: Int {
    case none
    case up
    case down
}

/// Default images for the floor transition types and direction
@objc public class FloorTransitionImage: NSObject {
    private static var bundle: Bundle { IndoorMapViewCodeResources.resourceBundle }

    public static var arrowUp: UIImage? { UIImage(named: "arrow_up", in: bundle, compatibleWith: nil) }

    public static var arrowDown: UIImage? { UIImage(named: "arrow_down", in: bundle, compatibleWith: nil) }

    public static var elevator: UIImage? { UIImage(named: "elevator", in: bundle, compatibleWith: nil) }

    public static var escalator: UIImage? { UIImage(named: "escalator", in: bundle, compatibleWith: nil) }

    public static var ramp: UIImage? { UIImage(named: "ramp", in: bundle, compatibleWith: nil) }

    public static var stairs: UIImage? { UIImage(named: "stairs", in: bundle, compatibleWith: nil) }
}


/// A layer to place transitions between floors on the map
/// You can use `elevator`, `escalator`, `ramp`, `stairs` images from `FloorTransitionImage` as an iconImage.
/// Use `direction` and `floorText` to explain to which floor the user should go while navigation.
/// See `FloorTransitionVisualConfig` for visual configurations
open class FloorTransitionLayer: CALayer {
    public let id: String
    public var isSelectable: Bool = true
    private let unitsConverter: UnitsConverter
    private var currentContainerScale: CGFloat = 1
    private var currentContainerRotation: CGFloat = 0
    private let config: FloorTransitionVisualConfig

    private var iconBackgroundLayer: CALayer?
    private var iconImageLayer: CALayer?
    private var textLayer: CATextLayer?
    private var directionImageLayer: CALayer?
    private var badgeLayer: CALayer?
    private var platformLayer: CALayer?
    private let coordinates: CGPoint


    public init(id: String,
         iconImage: UIImage?,
         floorText: String?,
         direction: FloorTransitionDirection,
         coordinates: CGPoint,
         config: FloorTransitionVisualConfig,
         unitsConverter: UnitsConverter
         ) {
        self.id = id
        self.config = config
        self.unitsConverter = unitsConverter
        self.coordinates = coordinates
        super.init()

        self.masksToBounds = false
        self.frame = .zero

        self.addContent(
            iconImage: iconImage,
            floorText: floorText,
            direction: direction
        )
        self.position = unitsConverter.convertCoordinateToPointOnScreen(coordinates)
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(layer: Any) {
        let layer = layer as! FloorTransitionLayer
        if let platformLayer = layer.platformLayer,
            let bg = layer.iconBackgroundLayer,
            let img = layer.iconImageLayer,
            let text = layer.textLayer,
            let badgeLayer = layer.badgeLayer,
            let direction = layer.directionImageLayer  {
                self.iconBackgroundLayer = CALayer(layer: bg)
                self.iconImageLayer = CALayer(layer: img)
                self.textLayer = CATextLayer(layer: text)
                self.directionImageLayer = CALayer(layer: direction)
                self.platformLayer = CALayer(layer: platformLayer)
                self.badgeLayer = CALayer(layer: badgeLayer)
        }
        self.id = layer.id
        self.config = layer.config
        self.unitsConverter = layer.unitsConverter
        self.coordinates = layer.coordinates
        super.init(layer: layer)
    }

    func applyScaleAndRotation() {
        let scale = 1/currentContainerScale
        platformLayer?.transform = CATransform3DScale(CATransform3DMakeRotation(CGFloat(2 * Double.pi) - currentContainerRotation, 0, 0, 1), scale, scale, 1)
    }

    private func addContent(
        iconImage: UIImage?,
        floorText: String?,
        direction: FloorTransitionDirection
    ) {

        let platformLayer = CALayer()
        self.addSublayer(platformLayer)
        self.platformLayer = platformLayer

        let backgroundLayer = CAShapeLayer()
        let iconLayer = CALayer()

        backgroundLayer.addSublayer(iconLayer)
        platformLayer.addSublayer(backgroundLayer)

        self.iconBackgroundLayer = backgroundLayer
        self.iconImageLayer = iconLayer

        let bgSize = CGSize(width: 40, height: 40)
        let iconSize = CGSize(width: 28, height: 28)
        let badgeHeight: CGFloat = 32
        let badgeWidth: CGFloat = 32
        let arrowSize = CGSize(width: 17, height: 8)
        let textHeight: CGFloat = 16

        backgroundLayer.frame = CGRect(x: -bgSize.width/2, y: -bgSize.height/2, width: bgSize.width, height: bgSize.height)
        backgroundLayer.path = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: bgSize.width, height: bgSize.height)).cgPath
        backgroundLayer.fillColor = config.backgroundColor.cgColor
        backgroundLayer.lineWidth = 0.5
        backgroundLayer.strokeColor = config.backgroundStrokeColor.cgColor

        iconLayer.contentsGravity = .resizeAspect
        iconLayer.frame = CGRect(x: (bgSize.width - iconSize.width)/2, y: (bgSize.height - iconSize.height)/2, width: iconSize.width, height: iconSize.height)
        iconLayer.contents = iconImage?.cgImage

        switch direction {
        case .up, .down:
            guard let floorText = floorText else {
                break
            }

            let badgeLayer = CAShapeLayer()
            let textLayer = CATextLayer()
            let arrowLayer = CALayer()

            self.badgeLayer = badgeLayer
            self.textLayer = textLayer
            self.directionImageLayer = arrowLayer

            platformLayer.insertSublayer(badgeLayer, below: backgroundLayer)
            badgeLayer.addSublayer(textLayer)
            badgeLayer.addSublayer(arrowLayer)

            badgeLayer.frame = CGRect(x: -1, y: -badgeHeight/2, width: bgSize.width/2 + badgeWidth, height: badgeHeight)
            badgeLayer.path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: bgSize.width/2 + badgeWidth, height: badgeHeight), cornerRadius: 5).cgPath
            badgeLayer.fillColor = config.badgeColor.cgColor

            let paragraph = NSMutableParagraphStyle()
            paragraph.alignment = .center

            let strokeTextAttributes = [
                NSAttributedString.Key.strokeWidth: 0,
                NSAttributedString.Key.foregroundColor : config.textColor,
                NSAttributedString.Key.font : config.textFont,
                NSAttributedString.Key.paragraphStyle: paragraph
            ] as [NSAttributedString.Key : Any]

            textLayer.string = NSAttributedString(string: floorText, attributes: strokeTextAttributes)
            textLayer.alignmentMode = CATextLayerAlignmentMode(rawValue: "center")
            textLayer.contentsScale = UIScreen.main.scale
            textLayer.alignmentMode = CATextLayerAlignmentMode.center

            arrowLayer.contentsGravity = .resizeAspect

            if direction == .up {
                textLayer.frame = CGRect(x: bgSize.width/2, y: 10.5, width: badgeWidth, height: textHeight)
                arrowLayer.contents = config.arrowUpImage?.cgImage
                arrowLayer.frame = CGRect(x: textLayer.frame.origin.x + badgeWidth/2 - arrowSize.width/2, y: 4, width: arrowSize.width, height: arrowSize.height)
            } else {
                textLayer.frame = CGRect(x: bgSize.width/2, y: 5.5, width: badgeWidth, height: textHeight)
                arrowLayer.contents = config.arrowDownImage?.cgImage
                arrowLayer.frame = CGRect(x: textLayer.frame.origin.x + badgeWidth/2 - arrowSize.width/2, y: badgeHeight - 4-arrowSize.height, width: arrowSize.width, height: arrowSize.height)
            }
        case .none:
            break
        }
    }
}

/// Visual configurations of a floor transition layer
/// By default, `FloorTransitionImage.arrowUp` and `FloorTransitionImage.arrowDown` for the arrows.
@objc public class FloorTransitionVisualConfig: NSObject {
    let arrowUpImage: UIImage?
    let arrowDownImage: UIImage?
    let textColor: UIColor
    let textFont: UIFont
    let backgroundColor: UIColor
    let backgroundStrokeColor: UIColor
    let badgeColor: UIColor

    public init(arrowUpImage: UIImage? = FloorTransitionImage.arrowUp,
                  arrowDownImage: UIImage? = FloorTransitionImage.arrowDown,
                  textColor: UIColor = .white,
                  textFont: UIFont = UIFont.systemFont(ofSize: 12, weight: .medium),
                  backgroundColor: UIColor,
                  backgroundStrokeColor: UIColor,
                  badgeColor: UIColor) {
        self.arrowUpImage = arrowUpImage
        self.arrowDownImage = arrowDownImage
        self.textColor = textColor
        self.textFont = textFont
        self.backgroundColor = backgroundColor
        self.backgroundStrokeColor = backgroundStrokeColor
        self.badgeColor = badgeColor
    }
}

extension FloorTransitionLayer: MapObject {
    public func unitsConverterUpdated() {
        self.position = unitsConverter.convertCoordinateToPointOnScreen(coordinates)
    }

    public func setContainerScale(_ scale: CGFloat, rotation: CGFloat) {
        self.currentContainerRotation = rotation
        self.currentContainerScale = scale
        applyScaleAndRotation()
    }

    public func isPointInside(_ point: CGPoint) -> Bool {
        if self.isHidden {
            return false
        }

        guard let platform = self.platformLayer else {
            return false
        }

        let point = self.convert(point, from: self.superlayer)

        let transform = CATransform3DGetAffineTransform(platform.transform)

        if let badgeLayer = badgeLayer, badgeLayer.frame.applying(transform).contains(point) {
            return true
        }

        if let iconBackgroundLayer = iconBackgroundLayer, iconBackgroundLayer.frame.applying(transform).contains(point) {
            return true
        }

        return false
    }
}

