//
//  Circle.swift
//  IPSMapView
//
//  Created by Michael Krutoyarskiy on 12/04/2020.
//  Copyright © 2020 Michael Krutoyarskiy. All rights reserved.
//

import Foundation
import UIKit

/// A layer to represent circular objects on the map
/// Accepts radius both in `meters` and screen points
/// See `CircleVisualConfig` for possible visual configurations
open class CircleLayer: CAShapeLayer, ZoomSensitiveLayer {
    public let id: String
    public var isSelectable: Bool = false
    public var config: CircleVisualConfig {
        didSet {
            applyConfig()
        }
    }
    public var unitsConverter: UnitsConverter
    
    private let center: CGPoint
    private let radius: RadiusUnits

    private enum RadiusUnits {
        case meters(value: CGFloat)
        case points(value: CGFloat)
    }

    // MARK: Zoom management properties
    var currentContainerScale: CGFloat = 1
    public var hidesWhenOutsideOfAcceptableZoom: Bool = false {
        didSet {
            evaluateCurrentScale()
        }
    }
    public var zoomSensitiveLayer: CALayer { self }
    public var minZoom: CGFloat = 0
    public var maxZoom: CGFloat = .greatestFiniteMagnitude

    public init(id: String, center: CGPoint, radiusMeters: CGFloat, config: CircleVisualConfig, unitsConverter: UnitsConverter) {
        self.id = id
        self.config = config
        self.unitsConverter = unitsConverter
        self.center = center
        self.radius = RadiusUnits.meters(value: radiusMeters)
        super.init()

        commonInit()
    }

    public init(id: String, center: CGPoint, radiusPoints: CGFloat, config: CircleVisualConfig, unitsConverter: UnitsConverter) {
        self.id = id
        self.config = config
        self.unitsConverter = unitsConverter
        self.center = center
        self.radius = RadiusUnits.points(value: radiusPoints)
        super.init()

        commonInit()
    }

    private func commonInit() {
        self.masksToBounds = false
        self.frame = .zero
        self.applyConfig()
        self.lineCap = CAShapeLayerLineCap.round
        self.lineJoin = CAShapeLayerLineJoin.round

        resetPath()
        applyScale()
    }

    private func applyConfig() {
        self.fillColor = config.fillColor.cgColor
        self.strokeColor = config.strokeColor?.cgColor
        self.lineWidth = config.strokeWidth/currentContainerScale
    }

    override init(layer: Any) {
       let layer = layer as! CircleLayer
       self.id = layer.id
       self.config = layer.config
       self.unitsConverter = layer.unitsConverter
       self.radius = layer.radius
       self.center = layer.center
       super.init(layer: layer)
    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func resetPath() {
        var radius: CGFloat = 0

        switch self.radius {
        case .meters(let value):
            radius = unitsConverter.convertMetersToPoints(value)
        case .points(let value):
            radius = value/currentContainerScale
        }

        self.path =  UIBezierPath(
            arcCenter: unitsConverter.convertCoordinateToPointOnScreen(center),
            radius: radius,
            startAngle: CGFloat(0),
            endAngle: CGFloat(Double.pi * 2),
            clockwise: true
        ).cgPath
    }

    private func applyScale() {
        if config.strokeWidth > 0 {
            self.lineWidth = config.strokeWidth/currentContainerScale
        }

        evaluateCurrentScale()

        switch self.radius {
        case .meters:
            return
        case .points:
            resetPath()
        }
    }
}

/// Visual configurations of a circular shape
@objc public class CircleVisualConfig: NSObject {
    let fillColor: UIColor
    let strokeColor: UIColor?
    let strokeWidth: CGFloat

    public init(fillColor: UIColor, strokeColor: UIColor?, strokeWidth: CGFloat?) {
        self.fillColor = fillColor
        self.strokeColor = strokeColor
        self.strokeWidth = strokeWidth ?? 0
        super.init()
    }
}

extension CircleLayer: MapObject {
    public func setContainerScale(_ scale: CGFloat, rotation: CGFloat) {
        self.currentContainerScale = scale
        applyScale()
    }

    public func isPointInside(_ point: CGPoint) -> Bool {
        if isHidden {
            return false
        }

        return self.path?.contains(point) ?? false
    }

    public func unitsConverterUpdated() {
        resetPath()
    }
}

