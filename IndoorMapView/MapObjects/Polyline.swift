//
//  Polyline.swift
//  IPSMapView
//
//  Created by Michael Krutoyarskiy on 05/04/2020.
//  Copyright © 2020 Michael Krutoyarskiy. All rights reserved.
//

import Foundation
import UIKit
import InteractiveContainerView

/// A layer to place a set polylines on the map.
/// Each element in the `path` is a disconnected line.
/// See `PolylineVisualConfig` for visual configurations.
open class PolylineLayer: CALayer {
    public let id: String
    private var fillLayer = CAShapeLayer()
    private var strokeLayer = CAShapeLayer()

    private let unitsConverter: UnitsConverter
    private var currentContainerScale: CGFloat = 1
    public var isSelectable: Bool = false

    public var path: [[CGPoint]] = [] {
        didSet {
            applyPath()
        }
    }

    public var visualConfig: PolylineVisualConfig {
        didSet {
            applyConfig()
        }
    }

    public init(id: String, config: PolylineVisualConfig, unitsConverter: UnitsConverter) {
        self.id = id
        self.visualConfig = config
        self.unitsConverter = unitsConverter
        super.init()

        self.masksToBounds = false
        self.frame = .zero

        strokeLayer.fillColor = UIColor.clear.cgColor
        strokeLayer.lineCap = config.lineCap
        strokeLayer.lineJoin = config.lineJoint
        addSublayer(strokeLayer)

        fillLayer.fillColor = UIColor.clear.cgColor
        fillLayer.lineCap = config.lineCap
        fillLayer.lineJoin = config.lineJoint
        addSublayer(fillLayer)

        applyConfig()
    }

    private func applyConfig() {
        strokeLayer.strokeColor = visualConfig.strokeColor.cgColor
        fillLayer.strokeColor = visualConfig.fillColor.cgColor
        applyStyle()
    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(layer: Any) {
       let layer = layer as! PolylineLayer
       self.id = layer.id
       self.visualConfig = layer.visualConfig
       self.unitsConverter = layer.unitsConverter

       self.strokeLayer = CAShapeLayer(layer: layer.strokeLayer)
       self.fillLayer = CAShapeLayer(layer: layer.fillLayer)

       super.init(layer: layer)
    }

    private func applyStyle() {
        if visualConfig.strokeColor != .clear && visualConfig.strokeWidth.value > 0 {
            switch visualConfig.lineWidth {
            case .meters(let value):
                strokeLayer.lineWidth = unitsConverter.convertMetersToPoints(value + 2*visualConfig.strokeWidth.value)
                fillLayer.lineWidth = unitsConverter.convertMetersToPoints(value)
            case .points(let value):
                strokeLayer.lineWidth = (value + 2*visualConfig.strokeWidth.value)/currentContainerScale
                fillLayer.lineWidth = value/currentContainerScale
            }
        } else {
            strokeLayer.lineWidth = 0
            switch visualConfig.lineWidth {
            case .meters(let value):
                fillLayer.lineWidth = unitsConverter.convertMetersToPoints(value)
            case .points(let value):
                fillLayer.lineWidth = value/currentContainerScale
            }
        }
    }

    private func applyPath() {
        let path = self.path
        let drawPath = buildBezierPath(path)
        let cgPath = drawPath.cgPath

        fillLayer.path = cgPath

        if visualConfig.strokeColor != .clear && visualConfig.strokeWidth.value > 0 {
            strokeLayer.path = cgPath
        } else {
            strokeLayer.path = nil
        }
    }

    private func buildBezierPath(_ path: [[CGPoint]]) -> UIBezierPath {
        let result = UIBezierPath()

        if path.isEmpty {
            return result
        }

        for coordinates in path {
            if coordinates.isEmpty || coordinates.count < 2 {
                continue
            }

            result.move(to: unitsConverter.convertCoordinateToPointOnScreen(coordinates[0]))

            for i in 1..<coordinates.count {
                result.addLine(to: unitsConverter.convertCoordinateToPointOnScreen(coordinates[i]))
            }
        }

        return result
    }
}

/// Visual configurations for the polyline layer
/// `PolylineVisualConfig.WidthUnits` allow you define the line width and stroke size in meters or in screen points.
@objc public class PolylineVisualConfig: NSObject {
    let fillColor: UIColor
    let strokeColor: UIColor
    let lineWidth: WidthUnits
    let strokeWidth: WidthUnits
    let lineCap: CAShapeLayerLineCap
    let lineJoint: CAShapeLayerLineJoin

    enum WidthUnits {
        case meters(value: CGFloat)
        case points(value: CGFloat)

        var value: CGFloat {
            switch self {
            case .meters(let value):
                return value
            case .points(let value):
                return value
            }
        }
    }

    public init(
        fillColor: UIColor,
        strokeColor: UIColor,
        lineWidth: CGFloat,
        strokeWidth: CGFloat,
        lineCap: CAShapeLayerLineCap = .round,
        lineJoint: CAShapeLayerLineJoin = .round
    ) {
        self.fillColor = fillColor
        self.strokeColor = strokeColor
        self.lineWidth = .points(value: lineWidth)
        self.strokeWidth = .points(value: strokeWidth)
        self.lineCap = lineCap
        self.lineJoint = lineJoint
        super.init()
    }

    public init(
        fillColor: UIColor,
        strokeColor: UIColor,
        lineWidthMeters: CGFloat,
        strokeWidthMeters: CGFloat,
        lineCap: CAShapeLayerLineCap = .round,
        lineJoint: CAShapeLayerLineJoin = .round
    ) {
        self.fillColor = fillColor
        self.strokeColor = strokeColor
        self.lineWidth = .meters(value: lineWidthMeters)
        self.strokeWidth = .meters(value: strokeWidthMeters)
        self.lineCap = lineCap
        self.lineJoint = lineJoint
        super.init()
    }
}

extension PolylineLayer: MapObject {
    public func setContainerScale(_ scale: CGFloat, rotation: CGFloat) {
        if currentContainerScale == scale {
            return
        }
        self.currentContainerScale = scale

        switch visualConfig.lineWidth {
        case .points:
            applyStyle()
        default:
            break
        }
    }

    public func isPointInside(_ point: CGPoint) -> Bool {
        if isHidden {
            return false
        }
        let point = self.convert(point, from: self.superlayer)
        let width = max(self.fillLayer.lineWidth, self.strokeLayer.lineWidth)
        return fillLayer.path?.copy(strokingWithWidth: width, lineCap: .round, lineJoin: .round, miterLimit: CGFloat(Float.pi)).contains(point) ?? false
    }

    public func unitsConverterUpdated() {
        applyPath()
        applyConfig()
    }
}
