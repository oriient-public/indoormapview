//
//  IPSMapObject.swift
//  IPSMapView
//
//  Created by Michael Krutoyarskiy on 30/03/2020.
//  Copyright © 2020 Michael Krutoyarskiy. All rights reserved.
//

import Foundation
import QuartzCore

public typealias ContentItem = CALayer & MapObject

public typealias MapObjectID = String

/// The protocol required to add CALayer to the IndoorMapView
/// Each map object should have ID for indexing, should manage selection by user and handle relevant map transformations.
@objc public protocol MapObject {
    /// Unique ID of this object
    var id: MapObjectID { get }

    /// New scale and rotation are applied to the map
    @objc func setContainerScale(_ scale: CGFloat, rotation: CGFloat)

    /// The units converter parameters changed => all current coordinates are invalidated
    @objc func unitsConverterUpdated()

    /// Should the object be available for `IndoorMap.items(at:)` methods
    var isSelectable: Bool { get }

    /// true if provided point is inside this object `bounds` (whatever it means for a specific type of the object)
    @objc func isPointInside(_ point: CGPoint) -> Bool
}


