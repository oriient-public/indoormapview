//
//  Text.swift
//  IPSMapView
//
//  Created by Michael Krutoyarskiy on 19/04/2020.
//  Copyright © 2020 Michael Krutoyarskiy. All rights reserved.
//

import Foundation
import UIKit

/// A layer to place text information on the map
/// Accepts `NSAttributedString` which allows the font, colors and other styles customizations.
/// See `TextVisualConfig` for more visual configurations
open class TextLayer: CATextLayer, ZoomSensitiveLayer {
    public let id: String
    public var isSelectable: Bool = false
    public var unitsConverter: UnitsConverter
    
    private var currentContainerRotation: CGFloat = 0
    private let config: TextVisualConfig
    private let coordinates: CGPoint

    // MARK: Zoom management properties
    var currentContainerScale: CGFloat = 1
    public var hidesWhenOutsideOfAcceptableZoom: Bool = false {
        didSet {
            evaluateCurrentScale()
        }
    }
    public var zoomSensitiveLayer: CALayer { self }
    public var minZoom: CGFloat = 0
    public var maxZoom: CGFloat = .greatestFiniteMagnitude

    public init(
        id: String,
        coordinates: CGPoint,
        text: NSAttributedString,
        config: TextVisualConfig,
        unitsConverter: UnitsConverter
    ) {
        self.id = id
        self.unitsConverter = unitsConverter
        self.config = config
        self.coordinates = coordinates
        super.init()
        self.contentsScale = UIScreen.main.scale
        self.anchorPoint = config.anchorPoint
        self.masksToBounds = false
        self.string = text
        let size = self.preferredFrameSize()
        self.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        self.position = unitsConverter.convertCoordinateToPointOnScreen(coordinates)
        self.alignmentMode = .center
        self.contentsGravity = .center
        applyScaleAndRotation()
    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(layer: Any) {
       let layer = layer as! TextLayer
       self.id = layer.id
       self.config = layer.config
       self.unitsConverter = layer.unitsConverter
       self.coordinates = layer.coordinates
       super.init(layer: layer)
       self.contents = layer.contents
    }

    private func applyScaleAndRotation() {
        var transfrom = CATransform3DIdentity

        if !config.shouldRotateWithContent {
            transfrom = CATransform3DMakeRotation(CGFloat(2 * Double.pi) - self.currentContainerRotation, 0, 0, 1)
        }

        if !config.shouldScaleWithContent {
            let scale = 1/currentContainerScale
            transfrom = CATransform3DScale(transfrom, scale, scale, 1)
        } else {
            self.contentsScale = UIScreen.main.scale*currentContainerScale
        }

        if !config.shouldRotateWithContent || !config.shouldScaleWithContent {
            self.transform = transfrom
        }

        evaluateCurrentScale()
    }

}

extension TextLayer: MapObject {
    public func unitsConverterUpdated() {
        self.position = unitsConverter.convertCoordinateToPointOnScreen(coordinates)
    }

    public func setContainerScale(_ scale: CGFloat, rotation: CGFloat) {
        self.currentContainerScale = scale
        self.currentContainerRotation = rotation
        applyScaleAndRotation()
    }

    public func isPointInside(_ point: CGPoint) -> Bool {
        if isHidden {
            return false
        }
        return self.frame.insetBy(
            dx: -config.clickableAreaExtension.width,
            dy: -config.clickableAreaExtension.height
        ).contains(point)
    }
}

/// Visual configurations for the text layer
@objc public class TextVisualConfig: NSObject {
    /// If true the text will be scaled the same way the user scales the map,
    /// if false the text will always be the same size on the screen
    let shouldScaleWithContent: Bool

    /// If true the text will be rotated the same way user scales the map,
    /// if false the text will always be vertical on the screen
    let shouldRotateWithContent: Bool

    // Additional area for `isPointInside`
    let clickableAreaExtension: CGSize

    /// defines which point of the text will correspond to the `coordinates` provided during initialization
    let anchorPoint: CGPoint

    public init(shouldScaleWithContent: Bool,
                shouldRotateWithContent: Bool,
                clickableAreaExtension: CGSize = .zero,
                anchorPoint: CGPoint = CGPoint(x: 0.5, y: 0.5)) {
        self.shouldScaleWithContent = shouldScaleWithContent
        self.shouldRotateWithContent = shouldRotateWithContent
        self.clickableAreaExtension = clickableAreaExtension
        self.anchorPoint = anchorPoint
    }
}
