//
//  PolygonWithVertices.swift
//  IPSMapView
//
//  Created by Baruch Nurilov on 07/10/2020.
//  Copyright © 2020 Michael Krutoyarskiy. All rights reserved.
//

import Foundation
import UIKit

/// A `PolyhonLayer` with a square for each of the vertices
/// See `VertexVisualConfig` for the vertices visual configurations.
open class PolygonLayerWithVertices: PolygonLayer {
    public override var vertices: [CGPoint] {
        didSet {
            resetPath()
        }
    }

    public var verticesConfig: VertexVisualConfig {
        didSet {
            resetPath()
        }
    }

    public var showVertices: Bool = true {
        didSet {
            if showVertices != oldValue {
                resetPath()
            }
        }
    }

    private var verticesSublayers: [CALayer] = []
    
    public init(
        id: String,
        vertices: [CGPoint],
        config: PolygonVisualConfig,
        verticesConfig: VertexVisualConfig,
        unitsConverter: UnitsConverter
    ) {
        self.verticesConfig = verticesConfig
        super.init(id: id, vertices: vertices, config: config, unitsConverter: unitsConverter)
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(layer: Any) {
        let layer = layer as! PolygonLayerWithVertices
        self.verticesConfig = layer.verticesConfig
        super.init(layer: layer)
    }

    private func createVertextLayer() -> CALayer {
        let layer = CALayer()
        layer.disableImplicitPositionAnimation()
        layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        layer.backgroundColor = verticesConfig.fillColor.cgColor
        layer.borderColor = verticesConfig.strokeColor?.cgColor
        layer.borderWidth = verticesConfig.strokeWidth/currentContainerScale
        return layer
    }

    override func applyScale() {
        super.applyScale()

        verticesSublayers.forEach {
            $0.borderWidth = verticesConfig.strokeWidth/currentContainerScale
            let position = $0.position
            $0.frame = CGRect(x: 0, y: 0, width: verticesConfig.size/currentContainerScale, height: verticesConfig.size/currentContainerScale)
            $0.position = position
        }
    }

    override func resetPath() {
        super.resetPath()

        if !showVertices {
            verticesSublayers.forEach {
                $0.removeFromSuperlayer()
            }

            verticesSublayers.removeAll()
        } else {
            for index in 0..<vertices.count {

                var vertexLayer: CALayer!

                if index < verticesSublayers.count {
                    vertexLayer = verticesSublayers[index]
                } else {
                    vertexLayer = createVertextLayer()
                    verticesSublayers.append(vertexLayer)
                    self.addSublayer(vertexLayer)
                }

                vertexLayer.frame = CGRect(x: 0, y: 0, width: verticesConfig.size/currentContainerScale, height: verticesConfig.size/currentContainerScale)
                vertexLayer.position = self.unitsConverter.convertCoordinateToPointOnScreen(vertices[index])
            }

            for index in vertices.count..<verticesSublayers.count {
                verticesSublayers[index].removeFromSuperlayer()
            }

            verticesSublayers.removeLast(verticesSublayers.count - vertices.count)
        }
    }

}

@objc public class VertexVisualConfig: NSObject {
    public let fillColor: UIColor
    public let strokeColor: UIColor?
    public let strokeWidth: CGFloat
    public let size: CGFloat

    public init(fillColor: UIColor, strokeColor: UIColor?, strokeWidth: CGFloat?, size: CGFloat) {
        self.fillColor = fillColor
        self.strokeColor = strokeColor
        self.strokeWidth = strokeWidth ?? 0
        self.size = size
        super.init()
    }
}
