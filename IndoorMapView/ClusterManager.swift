//
//  ClusterManager.swift
//  IndoorMapView
//
//  Created by Michael Krutoyarskiy on 13/11/2022.
//

import Foundation
import Combine
import UIKit

/// Function that creates ContentItem for a single cluster
public typealias SingleClusterItemProvider<ItemType> = (ItemType, UnitsConverter) -> ContentItem
/// Function that creates ContentItem for a cluster consisting of two or more items
public typealias ClusterItemProvider<ItemType> = ([ItemType], UnitsConverter) -> ContentItem

/// Generic class that manages clustering algorithm and adding/removing items from the map
///
/// - Parameters:
///     - ClusterItemType: Item type that will be clustered
///     - ClusteringAlgorithmType: Clustering algorithm
public class ClusterManager<ClusterItemType, ClusteringAlgorithmType> where
    ClusteringAlgorithmType: ClusteringAlgorithm,
    ClusterItemType: ClusteringCompatible,
    ClusteringAlgorithmType.ItemType == ClusterItemType {

    private var itemIdsOnMap: [ClusterItemType: MapObjectID] = [:]
    private var items: AnySequence<ClusterItemType> = AnySequence([])
    private var currentClusters: [ClusterItemType: [ClusterItemType]] = [:]
    private let clusteringAlgorithm: ClusteringAlgorithmType
    private let mapView: IndoorMapView
    private let singleItemProvider: SingleClusterItemProvider<ClusterItemType>
    private let clusterItemProvider: ClusterItemProvider<ClusterItemType>
    private let unitsConverter: UnitsConverter

    private var lastRecalculationTime: Date?
    private let minRecalculationTimeInterval: Double
    private var lastScaleUsedForRecalculation: CGFloat?
    private let minScaleChangeForRecalculation: CGFloat
    private let itemsLevel: UInt
        
    private var cancellable: [AnyCancellable] = []

    public init(
        itemsLevel: UInt,
        minScaleChangeForRecalculation: CGFloat = 0,
        minRecalculationTimeInterval: Double = 0.1,
        clusteringAlgorithm: ClusteringAlgorithmType,
        mapView: IndoorMapView,
        unitsConverter: UnitsConverter,
        singleItemProvider: @escaping SingleClusterItemProvider<ClusterItemType>,
        clusterItemProvider: @escaping ClusterItemProvider<ClusterItemType>
    ) {
        self.itemsLevel = itemsLevel
        self.clusteringAlgorithm = clusteringAlgorithm
        self.mapView = mapView
        self.singleItemProvider = singleItemProvider
        self.clusterItemProvider = clusterItemProvider
        self.minRecalculationTimeInterval = minRecalculationTimeInterval
        self.minScaleChangeForRecalculation = minScaleChangeForRecalculation
        self.unitsConverter = unitsConverter
        
        subscribeOnMapEvents()
    }
        
    ///Removes all clusters on the map on deinit
    deinit {
        removeClusters(Array(items))
    }
        
    // MARK: - Public
    // MARK: Items management
        
    ///Sets items to be clustered
    public func set<S>(items: S) where S : Sequence, ClusterItemType == S.Element {
        self.items = AnySequence(items)
        self.evaluateState()
    }

    // MARK: - Private
        
    private func subscribeOnMapEvents() {
        mapView.onMapTransformation
        .throttle(for: .seconds(minRecalculationTimeInterval), scheduler: DispatchQueue.main, latest: true)
        .filter { [weak self] _ in
            guard
                let self = self,
                let lastScaleUsedForRecalculation = self.lastScaleUsedForRecalculation
            else { return true }
            
            return abs(self.unitsConverter.containerScale - lastScaleUsedForRecalculation) >= self.minScaleChangeForRecalculation
        }
        .sink { [weak self] _ in
            self?.evaluateState()
        }.store(in: &cancellable)
    }
        
    private func evaluateState() {
        lastRecalculationTime = Date()
        lastScaleUsedForRecalculation = self.unitsConverter.containerScale
        
        var clustersToAdd: [[ClusterItemType]] = [[]]
        var clustersToDelete: [ClusterItemType] = []
        
        let clustersDict = createClustersDict(
            clusteringAlgorithm.cluster(items: self.items)
        )
        
        findNewClusters(
            clustersDict,
            clustersToAdd: &clustersToAdd,
            clustersToDelete: &clustersToDelete
        )
        
        findDisappearedClusters(
            currentClusters: currentClusters,
            newClusters: clustersDict,
            clustersToDelete: &clustersToDelete
        )

        removeClusters(clustersToDelete)
        addClustersToMap(clustersToAdd)
        
        currentClusters = clustersDict
    }
        
    /// Functions that represents clusters in a dict in order to filter it on clusters to add and clusters to remove
    /// Creates dictionary where key - fist item of a cluster, value - items in cluster
    private func createClustersDict(_ clusters: [[ClusterItemType]]) -> [ClusterItemType : [ClusterItemType]] {
        var clustersDict: [ClusterItemType: [ClusterItemType]] = [:]
        
        clusters.forEach { items in
            guard let first = items.first else { return }
            clustersDict[first] = items
        }
        
        return clustersDict
    }
        
    /// Functions that find new clusters to add in map
    private func findNewClusters(
        _ clustersDict: [ClusterItemType: [ClusterItemType]],
        clustersToAdd: inout [[ClusterItemType]],
        clustersToDelete: inout [ClusterItemType]
    ) {
        //Loop that will find all the new clusters
        clustersDict.forEach { key, newItems in
            if let oldItems = currentClusters[key] {
                // If we have a cluster with the same main item in presented clusters we should remove all the items in it and add new ones
                if newItems != oldItems {
                    oldItems.forEach { id in
                        clustersToDelete.append(id)
                    }
                    
                    clustersToAdd.append(newItems)
                }
                
            } else {
                // Add cluster if we can't find cluster with the same main item in presented clusters
                clustersToAdd.append(newItems)
            }
            
        }
    }
    
    /// Functions that find clusters to remove in map
    private func findDisappearedClusters(
        currentClusters: [ClusterItemType : [ClusterItemType]],
        newClusters: [ClusterItemType : [ClusterItemType]],
        clustersToDelete: inout [ClusterItemType]
    ) {
        // Find clusters that doesn't exist in new cluster
        currentClusters.forEach { key, oldItems in
            if newClusters[key] == nil {
                oldItems.forEach { id in
                    clustersToDelete.append(id)
                }
            }
        }
    }
        
    /// Function to remove clusters from the map
    private func removeClusters(_ clusters: [ClusterItemType]) {
        if !clusters.isEmpty {
            clusters.forEach { item in
                guard let id = itemIdsOnMap[item] else { return }
                
                mapView.removeContentItem(with: id)
            }
        }
    }
    
    /// Function to add clusters to the map
    private func addClustersToMap(_ clusters: [[ClusterItemType]]) {
        clusters.forEach { cluster in
            guard !cluster.isEmpty else { return }
            
            let firstItem = cluster[0]
            
            if cluster.count == 1 {
                let singleItem = singleItemProvider(firstItem, self.unitsConverter)
                itemIdsOnMap[firstItem] = singleItem.id
                
                self.mapView.addContentItem(
                    singleItem,
                    at: self.itemsLevel
                )
            } else {
                let clusterItem = clusterItemProvider(cluster, self.unitsConverter)
                itemIdsOnMap[firstItem] = clusterItem.id
                
                self.mapView.addContentItem(
                    clusterItem,
                    at: self.itemsLevel
                )
            }
        }
    }
        
}
