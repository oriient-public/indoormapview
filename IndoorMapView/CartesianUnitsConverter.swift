//
//  CartesianUnitsConverter.swift
//  IPSMapView
//
//  Created by Michael Krutoyarskiy on 27/10/2020.
//  Copyright © 2020 Michael Krutoyarskiy. All rights reserved.
//

import Foundation
import UIKit

/// The implementation of the UnitsConverter to convert between
/// UIKit Coordinate system
/// (Units are points of the screen, origin is at Top-Left, X - oriented to the right, Y - oriented downwards, 0 rotation corresponds to the Y axis)
/// and Cartesian metric coordinate system
/// (Units are meters, origin is at Bottom-Left, X - oriented to the right, Y - oriented upwards, 0 rotation corresponds to the X axis)
@objc public class CartesianUnitsConverter: NSObject, UnitsConverter {
    public var containerContentScale: CGFloat = 1 {
        didSet {
            if containerContentScale == 0 {
                mapImagePixelToMeter = 1
            }
        }
    }

    public var containerContentOffset: CGPoint = .zero

    public var mapImagePixelToMeter: CGFloat = 1 {
        didSet {
            if mapImagePixelToMeter == 0 {
                mapImagePixelToMeter = 1
            }
        }
    }
    public var containerScale: CGFloat = 1

    public var containerRotation: CGFloat = 0

    public var mapImageToBuildingOffset: CGPoint = .zero

    public var mapSizeInMeters: CGSize = .zero

    public var visibleBounds: CGRect = .zero

    public var screenScale: CGFloat = UIScreen.main.scale

    public func convertHeadingAngleToUserRotation(_ angle: CGFloat) -> CGFloat { -angle }

    public func convertUserRotationToHeadingAngle(_ angle: CGFloat) -> CGFloat { -angle }

    public func convertHeadingAngleToMapRotation(_ angle: CGFloat) -> CGFloat { angle - CGFloat(Double.pi/2) }

    public func convertMapRotationToHeadingAngle(_ angle: CGFloat) -> CGFloat { angle + CGFloat(Double.pi/2) }

    public func convertMetersToPoints(_ meters: CGFloat) -> CGFloat { meters*mapImagePixelToMeter*containerContentScale/screenScale }

    public func convertPointsToMeters(_ points: CGFloat) -> CGFloat { points*screenScale/(mapImagePixelToMeter*containerContentScale) }

    public func convertCoordinateToPointOnScreen(_ coordinates: CGPoint) -> CGPoint {
        convertPointOnImageToPointOnContent(convertCoordinatesToPointOnImage(coordinates))
    }

    public func convertPointOnScreenToCoordinate(_ point: CGPoint) -> CGPoint {
        convertPointOnImageToCoordinates(convertPointOnContentToPointOnImage(point))
    }

    public func convertFocusMetersToZoomScale(_ meters: CGFloat) -> CGFloat {
        (mapSizeInMeters.width + 2*convertPointsToMeters(containerContentOffset.x)) / meters
    }

    public func convertZoomScaleToFocusMeters(_ zoomScale: CGFloat) -> CGFloat {
        (mapSizeInMeters.width + 2*convertPointsToMeters(containerContentOffset.x)) / zoomScale
    }

    // Converts from point relative to image to point relative to content layer
    // the image on the content layer is scaled because of the screen resolution
    // and has offset because it's ratio is not the same as the screen ratio
    private func convertPointOnImageToPointOnContent(_ point: CGPoint) -> CGPoint {
        // screen resolution applied
        var point = point.screenPoints

        // screen ratio - scale + offset
        point = point.applying(CGAffineTransform(scaleX: containerContentScale, y: containerContentScale))
        point = point.applying(CGAffineTransform(translationX: containerContentOffset.x, y: containerContentOffset.y))

        return point
    }

    /// Converts from point relative to content layer to point relative to the image
    /// the image on the content layer is scaled because of the screen resolution
    /// and has offset because it's ratio is not the same as the screen ratio
    private func convertPointOnContentToPointOnImage(_ point: CGPoint) -> CGPoint {
        // screen ratio - scale + offset
        var point = point.applying(CGAffineTransform(translationX: -containerContentOffset.x, y: -containerContentOffset.y))
        point = point.applying(CGAffineTransform(scaleX: 1/containerContentScale, y: 1/containerContentScale))

        // screen resolution applied
        return CGPoint(x: point.x * screenScale, y: point.y * screenScale)
    }

    /// Converts coordinates in meters relative to a building to pixels relative to the image
    /// building's coordinates system starts from bottom-left, image's from top-left
    public func convertCoordinatesToPointOnImage(_ coordinates: CGPoint) -> CGPoint {
        return CGPoint(
            x: (coordinates.x - mapImageToBuildingOffset.x)*mapImagePixelToMeter,
            y: (mapSizeInMeters.height - coordinates.y + mapImageToBuildingOffset.y)*mapImagePixelToMeter
        )
    }

    /// Converts pixels relative to the image to coordinates in meters relative to a building
    /// building's coordinates system starts from bottom-left, image's from top-left
    public func convertPointOnImageToCoordinates(_ point: CGPoint) -> CGPoint {
        return CGPoint(
            x: point.x/mapImagePixelToMeter + mapImageToBuildingOffset.x,
            y: -point.y/mapImagePixelToMeter + mapImageToBuildingOffset.y + mapSizeInMeters.height
        )
    }
}
